# VERSION               0.0.1
# BUILD-USING:        gulp dist && docker build -t ui-webapp .
# PUSH-USING:         docker tag ui-webapp surf/ui-webapp  && docker push surf/ui-webapp

FROM      surf/base-node
MAINTAINER Dan Kinsley <dan@surfbeta.com>

RUN apt-get install -y git

ADD server.js /server.js
ADD package.json /package.json
ADD /dist /dist
ADD /build /build

RUN npm install --production

EXPOSE 5000

# Launch titan when launching the container
CMD ["npm", "start"]
