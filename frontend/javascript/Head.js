var React = require('react');

var Head = React.createClass({
    render: function() {
        var meta = this.props.meta.map(function(tag){
            return <meta key={tag.property} property={tag.property} content={tag.content} />
        });
        console.log("RENDERING HEAD",this.props);
        return (
            <head>
                <title>{this.props.title}</title>
                <link rel="icon" href="favicon.ico"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta name="apple-mobile-web-app-capable" content="yes"/>
                <meta name="mobile-web-app-capable" content="yes"/>
                {meta}
                <link href="/stylesheets/app.min.css" rel="stylesheet"/>
            </head>
        );
    }
});

module.exports = Head;