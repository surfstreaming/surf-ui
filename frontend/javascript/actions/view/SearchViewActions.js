var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes'),
    SearchAPI = require('../../utils/api/SearchAPI')
    ;

module.exports = {
    search: function(request){
        AppDispatcher.handleViewAction({
            type: actionTypes.SEARCH_REQUEST,
            request: request
        });
        SearchAPI.search(request);
    }
};