var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes'),
    AppStore = require('../../stores/AppStore'),
    UserStore = require('../../stores/UserStore'),
    UserAPI = require('../../utils/api/UserAPI'),
    UserServerActions = require('../../actions/server/UserServerActions')
    ;

module.exports = {
    requestUserByUsername: function(username,components){
        AppDispatcher.handleViewAction({
            type: actionTypes.USER_REQUEST,
            username: username
        });

        UserAPI.getUserByUsername(username,components).then(UserServerActions.receiveUser);
    },
    requestUser: function(userId,components){
        AppDispatcher.handleViewAction({
            type: actionTypes.USER_REQUEST,
            userId: userId
        });

        UserAPI.getUser(userId,components).then(UserServerActions.receiveUser);
    },
    requestCurrentUser: function(components){
        AppDispatcher.handleViewAction({
            type: actionTypes.CURRENT_USER_REQUEST
        });

        UserAPI.getCurrentUser(components).then(UserServerActions.currentUserChange,UserServerActions.currentUserError);
    },
    addFriendAction: function(userId,friendId){
        AppDispatcher.handleViewAction({
            type: actionTypes.FRIEND_ADD_REQUEST,
            userId: userId,
            friendId: friendId
        });

        UserAPI.addFriend(userId,friendId).then(UserServerActions.addFriendshipResponse.bind(this,userId,friendId));
    },
    cancelFriendRequest: function(userId,friendshipId){
        AppDispatcher.handleViewAction({
            type: actionTypes.FRIEND_CANCEL_REQUEST,
            userId: userId,
            friendshipId: friendshipId
        });

        UserAPI.removeFriend(userId,friendshipId).then(UserServerActions.cancelFriendshipResponse.bind(this,userId,friendshipId));
    },
    rejectFriendRequest: function(userId,friendId,friendshipId){
        AppDispatcher.handleViewAction({
            type: actionTypes.FRIEND_REMOVE_REQUEST,
            userId: userId,
            friendId: friendId,
            friendshipId: friendshipId
        });

        UserAPI.removeFriend(userId,friendshipId).then(UserServerActions.removeFriendshipResponse.bind(this,userId,friendshipId));
    },
    removeFriendAction: function(userId,friendId,friendshipId){
        AppDispatcher.handleViewAction({
            type: actionTypes.FRIEND_REMOVE_REQUEST,
            userId: userId,
            friendId: friendId,
            friendshipId: friendshipId
        });

        UserAPI.removeFriend(userId,friendshipId).then(UserServerActions.removeFriendshipResponse.bind(this,userId,friendId,friendshipId));
    },
    confirmFriendAction: function(userId,friendId,friendshipId){
        AppDispatcher.handleViewAction({
            type: actionTypes.FRIEND_CONFIRM_REQUEST,
            userId: userId,
            friendId: friendId,
            friendshipId: friendshipId
        });

        UserAPI.confirmFriend(userId,friendshipId).then(UserServerActions.confirmFriendshipResponse.bind(this,userId,friendId,friendshipId));
    },
    updateUser: function(userId,user){
        AppDispatcher.handleViewAction({
            type: actionTypes.USER_UPDATE_REQUEST,
            userId: userId,
            user: user
        });

        UserAPI.update(userId,user).then(UserServerActions.updateResponse.bind(this,userId,user),UserServerActions.updateError.bind(this,userId,user));
    },
    updatePassword: function(current,password,confirm){
        AppDispatcher.handleViewAction({
            type: actionTypes.USER_CHANGE_PASSWORD_REQUEST
        });

        UserAPI.changePassword(current,password,confirm).then(UserServerActions.changePasswordResponse,UserServerActions.changePasswordError);
    }
};