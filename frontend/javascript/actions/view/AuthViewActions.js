var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes'),
    AuthAPI = require('../../utils/api/AuthAPI'),
    RouterStore = require('../../stores/RouterStore'),
    AuthServerActions = require('../server/AuthServerActions')
    ;

module.exports = {
    getInitialAuth: function(){
        AuthAPI.getInitialAuth();
    },
    login: function(request){
        AppDispatcher.handleViewAction({
            type: actionTypes.LOGIN_REQUEST,
            request: request
        });

        AuthAPI.login(request).then(function(response){
            console.log("TIME TO FIRE LOGIN SUCCESS");
            AuthServerActions.loginSuccess(response);
            console.log("TIME TO TRANSITION",RouterStore.get());
            RouterStore.get().transitionTo("home",null,{login: true});
        },AuthServerActions.loginError);
    },
    logout: function(){
        AppDispatcher.handleViewAction({
            type: actionTypes.LOGOUT_REQUEST
        });
        AuthAPI.logout();
    },
    usernameAvailable: function(username){
        AppDispatcher.handleViewAction({
            type: actionTypes.AUTH_USERNAME_AVAILABLE_REQUEST,
            username: username
        });
        AuthAPI.usernameAvailable(username);
    },
    emailAvailable: function(email){
        AppDispatcher.handleViewAction({
            type: actionTypes.AUTH_EMAIL_AVAILABLE_REQUEST,
            email: email
        });
        AuthAPI.emailAvailable(email);
    },
    quickRegister: function(email){
        AppDispatcher.handleViewAction({
            type: actionTypes.AUTH_QUICK_REGISTER_REQUEST,
            email: email
        });
        AuthAPI.quickRegister(email);
    },
    register: function(request){
        AppDispatcher.handleViewAction({
            type: actionTypes.AUTH_REGISTER_REQUEST,
            request: request
        });
        AuthAPI.signup(request);
    },
    validate: function(token){
        AppDispatcher.handleViewAction({
            type: actionTypes.AUTH_TOKEN_VALIDATE_REQUEST,
            token: token
        });
        AuthAPI.validate(token);
    }
};