var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes'),
    AppStore = require('../../stores/AppStore'),
    ContentAPI = require('../../utils/api/ContentAPI'),
    ContentServerActions = require('../../actions/server/ContentServerActions'),
    QueueServerActions = require('../../actions/server/QueueServerActions'),
    QueueAPI = require('../../utils/api/QueueAPI')
;

module.exports = {
    requestFeaturedContent: function(){
        AppDispatcher.handleViewAction({
            type: actionTypes.FEATURED_CONTENT_REQUEST
        });
        ContentAPI.getFeaturedContent().then(ContentServerActions.featuredContentChange);
    },
    requestFavoriteQueues: function(userId,components){
         AppDispatcher.handleViewAction({
            type: actionTypes.FAVORITE_QUEUES_REQUEST
         });
        QueueAPI.getFavorites(userId,components)
            .then(QueueServerActions.favoriteQueueChange);
    },
    requestFeaturedQueues: function(components){
        AppDispatcher.handleViewAction({
            type: actionTypes.FEATURED_QUEUES_REQUEST
        });
        QueueAPI.getFeatured(components)
            .then(QueueServerActions.featuredQueueChange);
    }
};