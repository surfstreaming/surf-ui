var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes'),
    normalize = require('normalizr').normalize,
    QueueAPI = require('../../utils/api/QueueAPI'),
    AuthStore = require('../../stores/AuthStore'),
    RouterStore = require('../../stores/RouterStore'),
    QueueServerActions = require('../../actions/server/QueueServerActions'),
    Schema = require('../../utils/api/Schema')
    ;

module.exports = {
    requestQueues: function(userId,components){
        AppDispatcher.handleViewAction({
            type: actionTypes.QUEUES_REQUEST,
            user: userId
        });

        QueueAPI.getQueues(userId,components)
            .then(QueueServerActions.receiveQueues.bind(this,userId));
    },
    requestQueue: function(userId,queueId,components){
        AppDispatcher.handleViewAction({
            type: actionTypes.QUEUE_REQUEST,
            user: userId,
            queue: queueId
        });
        QueueAPI.getQueue(userId,queueId,components)
            .then(QueueServerActions.receiveQueue.bind(this,userId))
    },
    addToFavorites: function(userId,queueId){
        console.log("GOT QUEUE_FAVORITES_REMOVE_REQUEST");
        AppDispatcher.handleViewAction({
            type: actionTypes.QUEUE_FAVORITES_ADD_REQUEST,
            user: userId,
            queue: queueId
        });
        QueueAPI.addFavorite(userId,queueId)
    },
    removeFromFavorites: function(userId,queueId,favoriteId){
        console.log("GOT QUEUE_FAVORITES_REMOVE_REQUEST",userId,queueId,favoriteId);
        AppDispatcher.handleViewAction({
            type: actionTypes.QUEUE_FAVORITES_REMOVE_REQUEST,
            user: userId,
            queueId: queueId,
            favoriteId: favoriteId
        });
        QueueAPI.deleteFavorite(userId,queueId,favoriteId)
    },
    updateQueue: function(username,userId,queue){
        AppDispatcher.handleViewAction({
            type: actionTypes.QUEUE_UPDATE_REQUEST,
            user: userId,
            queue: queue
        });
        QueueAPI.updateQueue(userId,queue).then(function(response){
            QueueServerActions.receiveUpdatedQueue(response);
            RouterStore.get().transitionTo("users.user.queues.queue.detail",{username: username, queueid: response.id})
        });
    },
    createQueue: function(queue){
        console.log("CREATE QUEUE",queue);
        var auth = AuthStore.getAuth();
        AppDispatcher.handleViewAction({
            type: actionTypes.QUEUE_NEW_REQUEST,
            user: auth.id,
            queue: queue
        });
        QueueAPI.createQueue(queue).then(function(response){
            console.log("REDIRECT",{username: auth.username, queueid: response.result});
            RouterStore.get().replaceWith("users.user.queues.queue.detail",{username: auth.username, queueid: response.result})
        });
    },
    deleteQueue: function(userId,queueId){
        console.log("DELETE QUEUE",userId,queueId);
        var merge = {
            id: queueId,
            deleted: "pending"
        };
        var normalized = normalize(merge,Schema.queues);

        AppDispatcher.handleViewAction({
            type: actionTypes.QUEUE_DELETE_REQUEST,
            normalized: normalized
        });
        QueueAPI.deleteQueue(userId,queueId);
    },
    manageQueueItems: function(requests){
        console.log("manageQueueItems",requests);
        if(Array.isArray(requests) && requests.length > 0){
            AppDispatcher.handleViewAction({
                type: actionTypes.QUEUE_ITEM_MANAGE_REQUEST,
                requests: requests
            });
            QueueAPI.manageQueueItems(requests);
        }

    }
};