var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes'),
    ContentAPI = require('../../utils/api/ContentAPI')
    ;

module.exports = {
    requestContent: function(contentId,components){
        AppDispatcher.handleViewAction({
            type: actionTypes.CONTENT_REQUEST,
            contentId: contentId
        });
        ContentAPI.getContent(contentId,components)
    },
    requestSeries: function(seriesId, components){
        AppDispatcher.handleViewAction({
            type: actionTypes.TVSERIES_REQUEST,
            seriesId: seriesId
        });
        ContentAPI.getTVSeries(seriesId,components)
    },
    requestSeason: function(seriesId, seasonId, components){
        AppDispatcher.handleViewAction({
            type: actionTypes.TVSEASON_REQUEST,
            seasonId: seasonId
        });
        ContentAPI.getTVSeason(seriesId, seasonId,components)
    },
    requestEpisode: function(seriesId, seasonId, episodeId, components){
        AppDispatcher.handleViewAction({
            type: actionTypes.TVEPISODE_REQUEST,
            episodeId: episodeId
        });
        ContentAPI.getTVEpisode(seriesId, seasonId, episodeId,components)
    }
};