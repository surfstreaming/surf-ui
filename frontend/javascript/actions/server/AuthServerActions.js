var AppDispatcher = require('../../dispatchers/AppDispatcher');
var ActionTypes = require('../../constants/ActionTypes');

module.exports = {
    receiveInitialAuth: function(auth){
        AppDispatcher.handleServerAction({
            type: ActionTypes.INITIAL_AUTH,
            auth: auth
        });
    },
    loginSuccess: function(auth) {
        AppDispatcher.handleServerAction({
            type: ActionTypes.LOGIN_SUCCESS,
            auth: auth
        });
    },
    loginError: function(error){
        AppDispatcher.handleServerAction({
            type: ActionTypes.LOGIN_ERROR,
            error: error
        });
    },
    logoutSuccess: function(){
        AppDispatcher.handleServerAction({
            type: ActionTypes.LOGOUT_SUCCESS
        });
    },
    logoutError: function(error){
        AppDispatcher.handleServerAction({
            type: ActionTypes.LOGOUT_ERROR,
            error: error
        });
    },
    invalidToken: function(error,authStr){
        AppDispatcher.handleServerAction({
            type: ActionTypes.INVALID_TOKEN,
            error: error,
            authStr: authStr
        });
    },
    usernameAvailable: function(username){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_USERNAME_AVAILABLE_CONFIRM,
            username: username
        });
    },
    usernameUnavailable: function(username){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_USERNAME_AVAILABLE_REJECT,
            username: username
        });
    },
    emailAvailable: function(email){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_EMAIL_AVAILABLE_CONFIRM,
            email: email
        });
    },
    emailUnavailable: function(email){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_EMAIL_AVAILABLE_REJECT,
            email: email
        });
    },
    quickRegisterSuccess: function(email,response){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_QUICK_REGISTER_SUCCESS,
            email: email,
            response: response
        });
    },
    quickRegisterError: function(email,response){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_QUICK_REGISTER_ERROR,
            email: email,
            response: response
        });
    },
    registerSuccess: function(request,response){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_REGISTER_SUCCESS,
            request: request,
            response: response
        });
    },
    registerError: function(request,response){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_REGISTER_ERROR,
            request: request,
            response: response
        });
    },
    validateSuccess: function(token,response){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_TOKEN_VALIDATE_SUCCESS,
            token: token,
            response: response
        });
    },
    validateError: function(token,response){
        AppDispatcher.handleServerAction({
            type: ActionTypes.AUTH_TOKEN_VALIDATE_ERROR,
            token: token,
            response: response
        });
    }
};