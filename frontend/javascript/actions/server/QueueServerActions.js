var AppDispatcher = require('../../dispatchers/AppDispatcher');
var actionTypes = require('../../constants/ActionTypes');

module.exports = {
    favoriteQueueChange: function(normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.FAVORITE_QUEUES_RESPONSE,
            normalized: normalized
        });
    },
    featuredQueueChange: function(normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.FEATURED_QUEUES_RESPONSE,
            normalized: normalized
        });
    },
    receiveQueues: function(userId,normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUES_RESPONSE,
            userId: userId,
            normalized: normalized
        });
    },
    receiveQueue: function(userId,normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUE_RESPONSE,
            userId: userId,
            normalized: normalized
        });
    },
    receiveFavoriteAdd: function(userId,queueId,response){
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUE_FAVORITES_ADD_RESPONSE,
            userId: userId,
            queueId: queueId,
            response: response
        });
    },
    receiveFavoriteDelete: function(userId,queueId,response){
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUE_FAVORITES_REMOVE_RESPONSE,
            userId: userId,
            queueId: queueId,
            response: response
        });
    },
    receiveUpdatedQueue: function(response){
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUE_UPDATE_RESPONSE,
            response: response
        });
    },
    receiveNewQueue: function(userId,normalized){
        console.log("!! receiveNewQueue",normalized);
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUE_NEW_RESPONSE,
            normalized: normalized,
            userId: userId
        });
        return normalized;
    },
    receiveDeleteQueue: function(userId,normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUE_DELETE_RESPONSE,
            normalized: normalized,
            userId: userId
        });
    },
    manageQueueItemResults: function(requests,responses){
        AppDispatcher.handleServerAction({
            type: actionTypes.QUEUE_ITEM_MANAGE_RESPONSE,
            requests: requests,
            responses: responses
        });
    }

};