var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes')
;

module.exports = {
    currentUserChange: function(normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.CURRENT_USER_CHANGE,
            normalized: normalized
        });
    },
    currentUserError: function(err){
        console.log("CURRENT USER ERROR",err);
        AppDispatcher.handleServerAction({
            type: actionTypes.CURRENT_USER_ERROR,
            error: err
        });
    },
    receiveUser: function(normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.USER_RESPONSE,
            normalized: normalized
        });
    },
    addFriendshipResponse: function(userId,friendId,response){
        AppDispatcher.handleServerAction({
            type: actionTypes.FRIEND_ADD_RESPONSE,
            userId: userId,
            friendId: friendId,
            response: response
        });
    },
    confirmFriendshipResponse: function(userId,friendId,friendshipId,response){
        AppDispatcher.handleServerAction({
            type: actionTypes.FRIEND_CONFIRM_RESPONSE,
            userId: userId,
            friendId: friendId,
            friendshipId: friendshipId,
            response: response
        });
    },
    cancelFriendshipResponse: function(userId,friendshipId,response){
        AppDispatcher.handleServerAction({
            type: actionTypes.FRIEND_CANCEL_RESPONSE,
            userId: userId,
            friendshipId: friendshipId,
            response: response
        });
    },
    removeFriendshipResponse: function(userId,friendId,friendshipId,response){
        AppDispatcher.handleServerAction({
            type: actionTypes.FRIEND_REMOVE_RESPONSE,
            userId: userId,
            friendId: friendId,
            friendshipId: friendshipId,
            response: response
        });
    },
    updateResponse: function(userId,user,normalized){
        AppDispatcher.handleServerAction({
            type: actionTypes.USER_UPDATE_SUCCESS,
            userId: userId,
            user: user,
            normalized: normalized
        });
    },
    updateError: function(userId,user,response){
        AppDispatcher.handleServerAction({
            type: actionTypes.USER_UPDATE_ERROR,
            userId: userId,
            user: user,
            response: response
        });
    },
    changePasswordResponse: function(){
        AppDispatcher.handleServerAction({
            type: actionTypes.USER_CHANGE_PASSWORD_SUCCESS
        });
    },
    changePasswordError: function(response){
        AppDispatcher.handleServerAction({
            type: actionTypes.USER_CHANGE_PASSWORD_ERROR,
            response: response
        });
    }
};