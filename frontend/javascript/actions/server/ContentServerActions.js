var AppDispatcher = require('../../dispatchers/AppDispatcher');
var actionTypes = require('../../constants/ActionTypes');

module.exports = {
    featuredContentChange: function(normalized){
        console.log("FEATURED CONTENT CHANGE",normalized);
        AppDispatcher.handleServerAction({
            type: actionTypes.FEATURED_CONTENT_RESPONSE,
            normalized: normalized
        });
        console.log("FEATURED CONTENT CHANGE2",normalized);
    },
    receiveContent: function(normalized){
        console.log("RECEIVED:",normalized);
        AppDispatcher.handleServerAction({
            type: actionTypes.CONTENT_RESPONSE,
            normalized: normalized
        });
    },
    tvSeriesResponse: function(normalized){
        console.log("RECEIVED:",normalized);
        AppDispatcher.handleServerAction({
            type: actionTypes.TVSERIES_RESPONSE,
            normalized: normalized
        });
    },
    tvSeriesError: function(error){
        AppDispatcher.handleServerAction({
            type: actionTypes.TVSERIES_ERROR,
            error: error
        });
    },
    tvSeasonResponse: function(normalized){
        console.log("RECEIVED:",normalized);
        AppDispatcher.handleServerAction({
            type: actionTypes.TVSEASON_RESPONSE,
            normalized: normalized
        });
    },
    tvSeasonError: function(error){
        AppDispatcher.handleServerAction({
            type: actionTypes.TVSEASON_ERROR,
            error: error
        });
    },
    tvEpisodeResponse: function(normalized){
        console.log("RECEIVED:",normalized);
        AppDispatcher.handleServerAction({
            type: actionTypes.TVEPISODE_RESPONSE,
            normalized: normalized
        });
    },
    tvEpisodeError: function(error){
        AppDispatcher.handleServerAction({
            type: actionTypes.TVEPISODE_ERROR,
            error: error
        });
    }
};