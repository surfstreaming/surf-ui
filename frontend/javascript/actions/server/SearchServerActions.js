var AppDispatcher = require('../../dispatchers/AppDispatcher'),
    actionTypes = require('../../constants/ActionTypes')
;

module.exports = {
    searchResponse: function(response){
        AppDispatcher.handleServerAction({
            type: actionTypes.SEARCH_RESPONSE,
            response: response
        });
    }
};