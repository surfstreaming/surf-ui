var AppDispatcher = require('../dispatchers/AppDispatcher'),
    PayloadSources = require('../constants/PayloadSources'),
    ActionTypes = require('../constants/ActionTypes'),
    StoreUtils = require('../utils/StoreUtils'),
    AuthStore = require('./AuthStore')
    ;

var _users = { };
var _usernames = { };
var _currentUser = -1;

var UserStore = StoreUtils.createStore({
    getCurrentUser: function(){
        return _users[_currentUser];
    },
    getUsers: function(){
        return _users;
    },
    getUser: function(id){
        return _users[id];
    },
    getUserByUsername: function(username){
        var id = _usernames[username];
        return isNaN(id) ? id : this.getUser(id);
    },
    getIdByUsername: function(username){
        console.log("REQUEST:",username,_usernames);
        return _usernames[username];
    }
});

function handleLogout(action){
    console.log("(UserStore) LOGOUT",action);
    _users = {};
    _usernames = {};
}

function handleLogin(action){
    _currentUser = action.auth.id;
}

function userRequest(action){
    if(action.username !== undefined){
        if(_usernames[action.username] === undefined){
            _usernames[action.username] = "loading";
        }
    } else {
        if(_users[action.userId] === undefined){
            _users[action.userId] = {loading: true};
        } else {
            _users[action.userId].loading = true;
        }
    }
}

function userResponse(action){
    var users = action.normalized.entities.users;
    //StoreUtils.mergeIntoBag(_users,users);
    Object.keys(users).forEach(function(key){
        users[key].loading = false;
        _usernames[users[key].object.username] = users[key].id;
    });
}

function queuesResponse(action){
    var userId = action.userId;
    _users[userId].queues = action.normalized.result;
}

function favoriteQueueAdd(action){
    var user = _users[_currentUser];
    if(Array.isArray(user.favorite_queues)){
        user.favorite_queues.push(action.queueId);
    } else {

    }
}
function favoriteQueueRemove(action){
    var user = _users[_currentUser];
    if(Array.isArray(user.favorite_queues)){
        var idx = user.favorite_queues.indexOf(action.queueId);
        if(idx !== -1){
            user.favorite_queues.splice(idx,1);
        }
    }
}

function friendAddResponse(action){
    var user = _users[action.friendId];
    if(user !== undefined){
        user.relationship = {
            id: action.response.edge.id,
            status: "Pending Request"
        };
    }
}

function friendConfirmResponse(action){
    var user = _users[action.friendId];
    if(user !== undefined){
        user.relationship = {
            id: action.response.id,
            status: "Friends"
        };
    }
}

function friendRemoveResponse(action){
    console.log("friendRemoveResponse",action);
    var user = _users[action.friendId];
    if(user !== undefined){
        user.relationship = {id: undefined, status: "No Relationship"}
    }
    console.log("OK",_users[action.friendId]);
}

function queueNewResponse(action){
    console.log("(UserStore) queueNewResponse",action);
    var queueId = action.normalized.result;
    var user = UserStore.getUser(action.userId);
    if(Array.isArray(user.queues)){
        user.queues.push(queueId);
    }
}

function queueDeleteResponse(action){
    console.log("(UserStore) queueDeleteResponse",action);
    var queueId = action.normalized.result;
    var user = UserStore.getUser(action.userId);
    if(user !== undefined && Array.isArray(user.queues)){
        var idx = user.queues.indexOf(queueId);
        if(idx > -1){
            user.queues.splice(idx,1);
        }
    }
}

UserStore.dispatchToken = AppDispatcher.register(function (payload) {
    //var entities = loadEntities(payload.action);
    var entities = StoreUtils.loadEntities(payload.action,_users,"users","UserStore");
    var actions = StoreUtils.process("UserStore",[
        // current user
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.INITIAL_AUTH, payload: payload, handler: handleLogin },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.LOGIN_SUCCESS, payload: payload, handler: handleLogin },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.LOGOUT_SUCCESS, payload: payload, handler: handleLogout },
        //{ source: PayloadSources.SERVER_ACTION, action: ActionTypes.CURRENT_USER_CHANGE, payload: payload, handler: currentUserChange },
        { source: PayloadSources.VIEW_ACTION, action: ActionTypes.USER_REQUEST, payload: payload, handler: userRequest },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.USER_RESPONSE, payload: payload, handler: userResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.QUEUES_RESPONSE, payload: payload, handler: queuesResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.QUEUE_FAVORITES_ADD_RESPONSE, payload: payload, handler: favoriteQueueAdd },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.QUEUE_FAVORITES_REMOVE_RESPONSE, payload: payload, handler: favoriteQueueRemove },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FRIEND_ADD_RESPONSE, payload: payload, handler: friendAddResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FRIEND_CONFIRM_RESPONSE, payload: payload, handler: friendConfirmResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FRIEND_REMOVE_RESPONSE, payload: payload, handler: friendRemoveResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.QUEUE_NEW_RESPONSE, payload: payload, handler: queueNewResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.QUEUE_DELETE_RESPONSE, payload: payload, handler: queueDeleteResponse }
    ]);

    if(entities || actions){
        UserStore.emitChange();
    }
});


module.exports = UserStore;