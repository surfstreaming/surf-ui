var _router  = null;

module.exports.set = function(router) {
    _router = router;
};

module.exports.get = function() {
    return _router;
};