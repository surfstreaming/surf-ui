var AppDispatcher = require('../dispatchers/AppDispatcher'),
    PayloadSources = require('../constants/PayloadSources'),
    ActionTypes = require('../constants/ActionTypes'),
    StoreUtils = require('../utils/StoreUtils'),
    createStore = StoreUtils.createStore,
    API = require('../utils/api/API')
    ;

// STORES
var ContentStore = require('./ContentStore'),
    QueueStore = require('./QueueStore'),
    UserStore = require('./UserStore')
    ;

var _state = {
};

var AuthStore = createStore({
    getState: function(){
        return _state;
    },
    getAuth: function(){
        return _state.auth;
    },
    getAuthId: function(){
        return _state.auth ? _state.auth.id : undefined;
    },
    getUserObject: function(){
        return UserStore.getUser(this.getAuthId());
    },
    isAnonymous: function(){
        return _state.anonymous
    }
});

function initialAuth(action){
    _state.auth = action.auth;

    if(action.auth === "anonymous"){
        _state.authenticated = false;
        _state.anonymous = true;
    } else {
        _state.authenticated = true;
        _state.anonymous = false;
        API.token = action.auth.token;
    }
}

function loginRequest(){
    _state.logout = undefined;
    _state.login = "pending";
}
function loginSuccess(action){
    _state.login = "successful";
    _state.authenticated = true;
    _state.auth = action.auth;
    API.token = action.auth.token;
    if(typeof localStorage !== 'undefined'){
        var authJson = JSON.stringify(action.auth);
        localStorage["surf-auth"] = authJson;
        document.cookie="token="+authJson;
    }

}
function loginError(action){
    _state.login = action.error;

}
function logoutRequest(){
    console.log("LOGGING OUT start");
    _state.logout = "Pending";
}
function logoutSuccess(){
    console.log("LOGGING OUT");
    API.token = undefined;
    _state = {
        logout: "successful"
    };
    if(typeof localStorage !== 'undefined'){
        console.log("REMOVING SURF-AUTH");
        localStorage.removeItem("surf-auth");
    }
}
function invalidToken(action){
    var err = action.error;

    _state.auth = undefined;
    if(typeof localStorage !== 'undefined'){
        console.log("REMOVING INVALID SURF-AUTH",action);
        localStorage.removeItem("surf-auth");
    }
}

function currentUser(action){
    console.log("currentUser",action);
    _state.currentUser = action.normalized.entities.users[action.normalized.result];
}

AuthStore.dispatchToken = AppDispatcher.register(function (payload) {
    var server = PayloadSources.SERVER_ACTION;
    var view = PayloadSources.VIEW_ACTION;

    var change = StoreUtils.process("AuthStore",[
        // initial auth
        { source: server, action: ActionTypes.INITIAL_AUTH, payload: payload, handler: initialAuth },
        // invalid token
        { source: server, action: ActionTypes.INVALID_TOKEN, payload: payload, handler: invalidToken },
        // login
        { source: view, action: ActionTypes.LOGIN_REQUEST, payload: payload, handler: loginRequest },
        { source: server, action: ActionTypes.LOGIN_SUCCESS, payload: payload, handler: loginSuccess },
        { source: server, action: ActionTypes.LOGIN_ERROR, payload: payload, handler: loginError },
        // logout
        { source: view, action: ActionTypes.LOGOUT_REQUEST, payload: payload, handler: logoutRequest },
        { source: server, action: ActionTypes.LOGOUT_SUCCESS, payload: payload, handler: logoutSuccess },
        // current user
        { source: server, action: ActionTypes.CURRENT_USER_CHANGE, payload: payload, handler: currentUser }
    ]);

    if(change){
        AuthStore.emitChange(payload);
    }
});


module.exports = AuthStore;