var React = require('react'),
    AppDispatcher = require('../dispatchers/AppDispatcher'),
    PayloadSources = require('../constants/PayloadSources'),
    ActionTypes = require('../constants/ActionTypes'),
    StoreUtils = require('../utils/StoreUtils')
    ;

var _content = { };
var _series = { };
var _seasons = { };
var _episodes = { };
var _featuredContent;

var ContentStore = StoreUtils.createStore({
    getAllContent: function(){
        return _content;
    },
    getItem: function(id){
        return _content[id];
    },
    getFeaturedContent: function(){
        if(_featuredContent === undefined) return;
        else return _featuredContent.map(this.getItem);
    },
    getSeries: function(id){
        return _series[id];
    },
    getSeason: function(id){
        return _seasons[id]
    },
    getEpisode: function(id){
        return _episodes[id]
    }
});

var entity = "content";
function loadEntities(action){
    if(typeof action !== 'undefined' && typeof action.normalized !== 'undefined' && typeof action.normalized.entities[entity] !== 'undefined'){
        StoreUtils.mergeIntoBag(_content,action.normalized.entities[entity],function(q){q.loading = false; return q;});
        return true;
    } else {
        return false;
    }
}

function reset(){
    console.log("(ContentStore) reset");
    _content = {};
    _featuredContent = undefined;
}

function contentRequest(action){
    var item = _content[action.contentId];
    if(item === undefined ){
        _content[action.contentId] = {loading: true};
    } else {
        _content[action.contentId].loading = true;
    }
}

function featuredContentRequest(){
    if(_featuredContent === undefined) _featuredContent = [];
    _featuredContent.loading = true;
}
function featuredContentResponse(action){
    _featuredContent = action.normalized.result;
    _featuredContent.loading = false;
}


function queueItemManageRequest(action){
    action.requests.forEach(function(request){
        if(request.addRemove === "add") addQueueItemRequest(request);
        else removeQueueItemRequest(request);
    });
}
function addQueueItemRequest(request){
    var newItem = {
        item: {id: "pending", _pending: "create"},
        queue: request.queueId
    };
    _content[request.contentId] = React.addons.update(_content[request.contentId],{
        private_queues: {$push: [newItem]}
    });
}
function removeQueueItemRequest(request){
    var idx = _content[request.contentId].private_queues.reduce(function(acc,qi, idx){
        if(qi.queue === request.queueId) return idx;
        else return undefined;
    },undefined);
    var newItem = {
        item: {id: request.itemId, _pending: "delete"},
        queue: request.queueId
    };
    _content[request.contentId] = React.addons.update(_content[request.contentId],{
        private_queues: {$splice: [[idx,1,newItem]]}
    });
}
function queueItemManageResponse(action){
    console.log("(ContentStore) queueItemManageResponse:",action);
    action.requests.forEach(function(request,idx){
        if(request.addRemove === "add") addQueueItemResponse(request,action.responses[idx]);
        else removeQueueItemResponse(request,action.responses[idx]);
    });
}
function addQueueItemResponse(request,response){
    var idx = _content[request.contentId].private_queues.reduce(function(acc,qi, idx){
        if(qi.queue === request.queueId) return idx;
        else return undefined;
    },undefined);
    var newItem = {
        item: response,
        queue: request.queueId
    };
    _content[request.contentId] = React.addons.update(_content[request.contentId],{
        private_queues: {$splice: [[idx,1,newItem]]}
    });
}
function removeQueueItemResponse(request,response){
    var idx = _content[request.contentId].private_queues.reduce(function(acc,qi, idx){
        if(qi.queue === request.queueId) return idx;
        else return undefined;
    },undefined);
    _content[request.contentId] = React.addons.update(_content[request.contentId],{
        private_queues: {$splice: [[idx,1]]}
    });
}
ContentStore.dispatchToken = AppDispatcher.register(function (payload) {
    var movies = StoreUtils.loadEntities(payload.action,_content,"content","ContentStore.movies");
    var series = StoreUtils.loadEntities(payload.action,_series,"series","ContentStore.series");
    var seasons = StoreUtils.loadEntities(payload.action,_seasons,"seasons","ContentStore.seasons");
    var episodes = StoreUtils.loadEntities(payload.action,_episodes,"episodes","ContentStore.episodes");

    var actions = StoreUtils.process("ContentStore",[
        // reset
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.LOGIN_SUCCESS, payload: payload, handler: reset },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.LOGOUT_SUCCESS, payload: payload, handler: reset },
        // featured content
        { source: PayloadSources.VIEW_ACTION, action: ActionTypes.FEATURED_CONTENT_REQUEST, payload: payload, handler: featuredContentRequest },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FEATURED_CONTENT_RESPONSE, payload: payload, handler: featuredContentResponse },
        // Content request
        { source: PayloadSources.VIEW_ACTION, action: ActionTypes.CONTENT_REQUEST, payload: payload, handler: contentRequest },
        // queue item manage
        { source: PayloadSources.VIEW_ACTION, action: ActionTypes.QUEUE_ITEM_MANAGE_REQUEST, payload: payload, handler: queueItemManageRequest },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.QUEUE_ITEM_MANAGE_RESPONSE, payload: payload, handler: queueItemManageResponse }
    ]);

    if(movies || series || seasons || episodes || actions){
        ContentStore.emitChange();
    }
});

module.exports = ContentStore;