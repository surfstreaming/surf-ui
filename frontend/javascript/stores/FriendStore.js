var AppDispatcher = require('../dispatchers/AppDispatcher'),
    PayloadSources = require('../constants/PayloadSources'),
    ActionTypes = require('../constants/ActionTypes'),
    StoreUtils = require('../utils/StoreUtils')
    ;

var _pending ;
var _requests ;
var _friends ;


var FriendStore = StoreUtils.createStore({
    getPendingRequests: function(){
        return _pending;
    },
    getFriendRequests: function(){
        return _requests;
    },
    getFriends: function(){
        return _friends;
    }
});

function handleLogout(){
    _pending = [];
    _requests = [];
    _friends = [];
}

function currentUser(action){
    console.log("(FriendStore) currentUser",action);
    _pending = action.normalized.entities.users[action.normalized.result].pending_friends;
    _requests = action.normalized.entities.users[action.normalized.result].friends_requesting;
    _friends = action.normalized.entities.users[action.normalized.result].friends;
}

function friendCancelRequest(action){
    var idx = _pending.map(function(x){ return x.link.id}).indexOf(action.friendshipId);
    if(idx >= 0){
        _pending[idx].deleted = "pending";
    }
}
function friendCancelResponse(action){
    var idx = _pending.map(function(x){ return x.link.id}).indexOf(action.friendshipId);
    if(idx >= 0){
        _pending.splice(idx,1);
    }
}

function friendAddResponse(action){
    var obj = {
        connection: action.friendId,
        link: action.response.edge
    };
    _pending.push(obj)
}

function friendConfirmResponse(action){
    console.log("(FriendStore) friendConfirmResponse",action,_pending,_friends);
    var idx = _requests.map(function(x){ return x.connection}).indexOf(action.friendId);
    if(idx >= 0){
        _requests.splice(idx,1);
    }
    _friends.push(action.friendId);
}

function friendRemoveResponse(action){
    var idx = _friends.indexOf(action.friendId);
    if(idx >= 0){
        _friends.splice(idx,1);
    }
}

FriendStore.dispatchToken = AppDispatcher.register(function (payload) {
    var entities = StoreUtils.loadEntities(payload.action,_friends,"friends");
    var actions = StoreUtils.process("UserStore",[
        // logout
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.LOGOUT_SUCCESS, payload: payload, handler: handleLogout },
        // current user
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.CURRENT_USER_CHANGE, payload: payload, handler: currentUser },
        // Cancel Friend Request
        { source: PayloadSources.VIEW_ACTION, action: ActionTypes.FRIEND_CANCEL_REQUEST, payload: payload, handler: friendCancelRequest },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FRIEND_CANCEL_RESPONSE, payload: payload, handler: friendCancelResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FRIEND_ADD_RESPONSE, payload: payload, handler: friendAddResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FRIEND_CONFIRM_RESPONSE, payload: payload, handler: friendConfirmResponse },
        { source: PayloadSources.SERVER_ACTION, action: ActionTypes.FRIEND_REMOVE_RESPONSE, payload: payload, handler: friendRemoveResponse }
    ]);

    if(entities || actions){
        FriendStore.emitChange();
    }
});


module.exports = FriendStore;