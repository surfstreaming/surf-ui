var React = require('react'),
    AppDispatcher = require('../dispatchers/AppDispatcher'),
    PayloadSources = require('../constants/PayloadSources'),
    ActionTypes = require('../constants/ActionTypes'),
    StoreUtils = require('../utils/StoreUtils')
    ;

var _results = [];
var _search = {};

var SearchStore = StoreUtils.createStore({
    getResults: function(){
        return _results;
    },
    getSearch: function(){
        return _search;
    }
});


var entity = "search";
function loadEntities(action){
    if(typeof action.normalized !== 'undefined' && typeof action.normalized.entities[entity] !== 'undefined'){
        console.log("(SearchStore) Loading Entities",action.normalized.entities[entity]);
        StoreUtils.mergeIntoBag(_search,action.normalized.entities[entity],function(q){q.loading = false; return q;});
        return true;
    } else {
        return false;
    }
}

function handleLogout(action){
    console.log("(SearchStore) LOGOUT",action);
    _results = [];
}
function searchRequest(action){
    console.log("(SearchStore) searchRequest",action);
    _search = action.request;
}
function searchResponse(action){
    console.log("(SearchStore) searchResponse",action);
    _results = action.response.results;
}

SearchStore.dispatchToken = AppDispatcher.register(function (payload) {
    var entities = loadEntities(payload.action);

    var server = PayloadSources.SERVER_ACTION;
    var view = PayloadSources.VIEW_ACTION;
    var actions = StoreUtils.process("SearchStore",[
        // current user
        { source: server, action: ActionTypes.LOGOUT_SUCCESS, payload: payload, handler: handleLogout },
        { source: view, action: ActionTypes.SEARCH_REQUEST, payload: payload, handler: searchRequest },
        { source: server, action: ActionTypes.SEARCH_RESPONSE, payload: payload, handler: searchResponse }


    ]);

    if(entities || actions){
        SearchStore.emitChange(payload);
    }
});


module.exports = SearchStore;