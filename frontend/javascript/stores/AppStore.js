var AppDispatcher = require('../dispatchers/AppDispatcher'),
    PayloadSources = require('../constants/PayloadSources'),
    ActionTypes = require('../constants/ActionTypes'),
    StoreUtils = require('../utils/StoreUtils'),
    createStore = StoreUtils.createStore
    ;

// STORES
var AuthStore = require('./AuthStore'),
    ContentStore = require('./ContentStore'),
    FriendStore = require('./FriendStore'),
    QueueStore = require('./QueueStore'),
    UserStore = require('./UserStore')
;

var _state = {
};

var AppStore = createStore({
    getState: function(){
        return _state;
    },
    getAuthId: function(){
        console.warn("Getting authId from AppStore - please retrieve from AuthStore");
        return AuthStore.getAuthId();
    },
    getAuth: function(){
        console.warn("Getting auth from AppStore - please retrieve from AuthStore");
        return AuthStore.getAuth();
    }
});

AppStore.dispatchToken = AppDispatcher.register(function (payload) {
    AppDispatcher.waitFor([
        AuthStore.dispatchToken,
        ContentStore.dispatchToken,
        FriendStore.dispatchToken,
        UserStore.dispatchToken,
        QueueStore.dispatchToken
    ]);

    //StoreUtils.process("AppStore",[ ]);

    console.log("(AppStore) EMIT",payload.source,payload.action);
    AppStore.emitChange(payload);

});


module.exports = AppStore;