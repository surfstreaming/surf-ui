var React = require('react'),
    AppDispatcher = require('../dispatchers/AppDispatcher'),
    PayloadSources = require('../constants/PayloadSources'),
    ActionTypes = require('../constants/ActionTypes'),
    StoreUtils = require('../utils/StoreUtils'),
    ContentStore = require('./ContentStore')
    ;

var _queues = { };
var _favorites ;
var _featured ;

var QueueStore = StoreUtils.createStore({
    getQueues: function(){
        return _queues;
    },
    getQueue: function(id){
        return _queues[id];
    },
    getFavorites: function(){
        if(_favorites !== undefined) return _favorites.map(this.getQueue);

    },
    getFeatured: function(){
        if(_featured !== undefined) return _featured.map(this.getQueue);
    }
});

function reset(){
    console.log("(QueueStore) reset");
    _queues = {};
    _favorites = undefined;
    _featured = undefined;
}

function queueRequest(action){
    var q = _queues[action.queue];
    if(q === undefined ){
        _queues[action.queue] = {loading: true};
    } else {
        _queues[action.queue].loading = true;
    }
}

function favoriteQueuesRequest(){

}

function favoriteQueuesResponse(action){
    console.log(action);
    _favorites = action.normalized.result;
}

function featuredQueuesResponse(action){
    console.log(action);
    _featured = action.normalized.result;
}

function favoriteQueueAdd(action){
    console.log("(QueueStore) favoriteQueueAdd",action);
    _queues[action.queueId].favorite = action.response;
    if(Array.isArray(_favorites)){
        _favorites.push(action.queueId);
    } else {
        _favorites = [action.queueId]
    }


    console.log("(QueueStore) favoriteQueueAdd complete ")
}
function favoriteQueueRemove(action){
    console.log("(QueueStore) favoriteQueueRemove",action);
    _queues[action.queueId].favorite = undefined;

    if(Array.isArray(_favorites)){
        var idx = _favorites.indexOf(action.queueId);
        if(idx !== -1){
            _favorites.splice(idx,1);
        }
    }

    console.log("(QueueStore) favoriteQueueRemove complete ")
}

function queueUpdateResponse(action){
    console.log("(QueueStore) queueUpdateResponse",action);
    _queues[action.response.id] = React.addons.update(_queues[action.response.id],{
        object: {$set: action.response.object},
        updated: {$set: new Date().getTime()}
    });
}

function queueItemManage(action){
    if(Array.isArray(action.requests)){
        action.requests.forEach(function(request,idx){
            var queue = _queues[request.queueId];
            delete queue.splash_images ;
            if(request.addRemove === "add"){
                if(Array.isArray(queue.items)){
                    queue.items.push({
                        content: request.contentId,
                        item: action.responses[idx]
                    });
                } else {
                    queue.items = [{
                        content: request.contentId,
                        item: action.responses[idx]
                    }];
                }
                if(Array.isArray(queue.first_ten)){
                    queue.first_ten.push({
                        content: request.contentId,
                        item: action.responses[idx]
                    });
                } else {
                    queue.first_ten = [{
                        content: request.contentId,
                        item: action.responses[idx]
                    }];
                }

            } else if(request.addRemove === "remove"){

                var remove = function(acc,cur,reduceIdx){
                    if(acc !== -1) return acc;
                    else if(request.contentId === cur.content){
                        return reduceIdx;
                    }
                    return -1;
                };
                if(Array.isArray(queue.items)){
                    var contentIdx = queue.items.reduce(remove,-1);
                    if(contentIdx !== -1){
                        queue.content.splice(contentIdx,1);
                    }
                }
                if(Array.isArray(queue.first_ten)){
                    var firstTenIdx = queue.first_ten.reduce(remove,-1);
                    if(firstTenIdx !== -1){
                        queue.first_ten.splice(firstTenIdx,1);
                    }
                }
            }
        });
    }
}
/*
var entity = "queues";
function loadEntities(action){
    if(typeof action.normalized !== 'undefined' && typeof action.normalized.entities[entity] !== 'undefined'){
        console.log("(QueueStore) Loading Entities",action.normalized.entities[entity]);
        StoreUtils.mergeIntoBag(_queues,action.normalized.entities[entity],function(q){q.loading = false; return q;});
        return true;
    } else {
        return false;
    }
}
*/
QueueStore.dispatchToken = AppDispatcher.register(function (payload) {
    //var entities = loadEntities(payload.action);
    var entities = StoreUtils.loadEntities(payload.action,_queues,"queues","QueueStore");

    var server = PayloadSources.SERVER_ACTION;
    var view = PayloadSources.VIEW_ACTION;
    var actions = StoreUtils.process("QueueStore",[
        // current user
        { source: server, action: ActionTypes.LOGOUT_SUCCESS, payload: payload, handler: reset },
        { source: server, action: ActionTypes.LOGIN_SUCCESS, payload: payload, handler: reset },
        // Queue request
        { source: view, action: ActionTypes.QUEUE_REQUEST, payload: payload, handler: queueRequest },
        // Favorite queues
        { source: view, action: ActionTypes.FAVORITE_QUEUES_REQUEST, payload: payload, handler: favoriteQueuesRequest },
        { source: server, action: ActionTypes.FAVORITE_QUEUES_RESPONSE, payload: payload, handler: favoriteQueuesResponse },
        // Featured queues
        { source: server, action: ActionTypes.FEATURED_QUEUES_RESPONSE, payload: payload, handler: featuredQueuesResponse },
        // queue favorite add/remove
        { source: server, action: ActionTypes.QUEUE_FAVORITES_ADD_RESPONSE, payload: payload, handler: favoriteQueueAdd },
        { source: server, action: ActionTypes.QUEUE_FAVORITES_REMOVE_RESPONSE, payload: payload, handler: favoriteQueueRemove },
        // UPDATE QUEUE
        { source: server, action: ActionTypes.QUEUE_UPDATE_RESPONSE, payload: payload, handler: queueUpdateResponse },
        // Queue item added
        { source: server, action: ActionTypes.QUEUE_ITEM_MANAGE_RESPONSE, payload: payload, handler: queueItemManage },


    ]);

    if(entities || actions){
        QueueStore.emitChange(payload);
    }
});


module.exports = QueueStore;