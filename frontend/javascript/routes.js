/** @jsx React.DOM */
var React = require('react'),
    Router = require('react-router'),
    Route = Router.Route,
    Routes = Router.Routes,
    DefaultRoute = Router.DefaultRoute;

// ROUTE COMPONENTS
var App = require('./App'),
    Abstract = require('./components/Abstract'),
    Home = require('./components/home/Home'),
    Friends = require('./components/users/Friends'),
    Login = require('./components/auth/Login'),
    Logout = require('./components/auth/Logout'),
    Movie = require('./components/content/movies/Movie'),
    QueueNew = require('./components/queues/QueueNew'),
    QueueList = require('./components/queues/QueueList'),
    QueuesAbstract = require('./components/users/queues/QueuesAbstract'),
    QueueAbstract = require('./components/users/queues//QueueAbstract'),
    QueueDetail = require('./components/users/queues/QueueDetail'),
    QueueEdit = require('./components/users/queues/QueueEdit'),
    UserAbstract = require('./components/users/user/UserAbstract'),
    Settings = require('./components/settings/Settings'),
    UserProfile = require('./components/users/user/UserProfile'),
    Search = require('./components/search/Search'),
    SignUp = require('./components/auth/SignUp'),
    TVSeries = require('./components/content/tv/TVSeries'),
    TVSeason = require('./components/content/tv/TVSeason'),
    TVEpisode = require('./components/content/tv/TVEpisode')
;

module.exports = (
    <Route path="/" handler={App}>
        <DefaultRoute name="home" handler={Home} pageTitle="Surf"/>

        <Route name="login" path="login" handler={Login} />
        <Route name="logout" path="logout" handler={Logout} />
        <Route name="signup" path="signup" handler={SignUp} />

        <Route name="friends" path="/friends" handler={Friends} />
        <Route name="users" path="users" handler={Abstract} >
            <Route name="users.user" path="/users/:username" handler={UserAbstract}>
                <DefaultRoute name="users.user.profile" handler={UserProfile}/>
                <Route name="users.user.queues" path="/users/:username/queues" handler={QueuesAbstract}>
                    <Route name="users.user.queues.queue" path="/users/:username/queues/:queueid" handler={QueueAbstract}>
                        <Route name="users.user.queues.queue.detail" path="/users/:username/queues/:queueid" handler={QueueDetail}/>
                        <Route name="users.user.queues.queue.edit" path="/users/:username/queues/:queueid/edit" handler={QueueEdit}/>
                    </Route>
                </Route>
            </Route>
        </Route>
        <Route name="settings" path="settings" handler={Settings.Base} >
            <DefaultRoute name="settings.basics" handler={Settings.Basics}/>
            <Route name="settings.password" path="/settings/password" handler={Settings.Password}/>
            <Route name="settings.avatar" path="/settings/avatar" handler={Settings.Avatar}/>
        </Route>
        <Route name="queues" path="queues" handler={QueueList}/>
        <Route name="queues.new" path="queues/new" pageTitle="New Queue" handler={QueueNew}/>
        <Route name="search" path="search" handler={Search}/>
        <Route name="movies" path="movies" handler={Abstract}>
            <Route name="movies.detail" path="/movies/:contentid" handler={Movie}/>
        </Route>
        <Route name="tv" path="tv" handler={Abstract}>
            <Route name="tv.series" path="/tv/:seriesid" handler={TVSeries}/>
            <Route name="tv.season" path="/tv/:seriesid/season/:seasonid" handler={TVSeason}/>
            <Route name="tv.episode" path="/tv/:seriesid/season/:seasonid/episode/:episodeid" handler={TVEpisode}/>
        </Route>
    </Route>
);
