var React = require('react'),
    Router = require('react-router'),
    mui = require("material-ui")
    ;
// Flux
var UserViewActions = require('../../actions/view/UserViewActions'),
    StoreDataMixin = require('../../mixins/StoreDataMixin'),
    AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore')
    ;

module.exports = React.createClass({
    mixins: [StoreDataMixin],
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            events: {
                USER_UPDATE_SUCCESS: this._updateSuccess,
                USER_UPDATE_ERROR: this._updateError
            }
        }
    },
    getInitialState: function(){
        return {
            userProps: this.props.user.object
        }
    },
    _updateSuccess: function(){
        this.setState({
            toast: "Successfully updated details.",
            showToast: true
        });
        setTimeout(this.setState.bind(this,{showToast: false}),4*1000);
    },
    _updateError: function(){

    },
    _save: function(){
        var user = AuthStore.getUserObject();
        var updated = this._merge(user);
        console.log("UPDATED VALUE IS ",updated);
        UserViewActions.updateUser(AuthStore.getAuthId(),updated)
    },
    _merge: function(user){
        var objs = this.refs;

        var newSettings = Object.keys(objs).reduce(function(acc,key){
            console.log("ACC:",acc,"KEY:",key);
            acc[key] = objs[key].getValue();
            return acc;
        },{});

        return React.addons.update(user.object,{
            $merge: newSettings
        });
    },
    render: function(){
        return (
            <mui.Paper >
                <mui.Toast message={this.state.toast} open={this.state.showToast}/>
                <div className="card layout vertical">
                    <div className="card-header">
                        <h2>Basics</h2>
                    </div>
                    <div className="card-body">
                        <mui.Input ref="firstName" type="text" name="current" placeholder="First Name" defaultValue={this.state.userProps.firstName} />
                        <mui.Input ref="lastName" type="text" name="newPassword" placeholder="Last Name" defaultValue={this.state.userProps.lastName} />
                        <mui.Input ref="email" type="text" name="confirm" placeholder="Email Address" defaultValue={this.state.userProps.email}  />
                    </div>

                    <div className="self-end">
                        <mui.FlatButton className="button-confirm" onClick={this._save} label="Save" />
                    </div>
                </div>
            </mui.Paper>
        )
    }
});