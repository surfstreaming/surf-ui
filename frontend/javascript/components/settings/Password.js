var React = require('react'),
    Router = require('react-router'),
    mui = require("material-ui")
    ;
// Flux
var UserViewActions = require('../../actions/view/UserViewActions'),
    StoreDataMixin = require('../../mixins/StoreDataMixin'),
    AppStore = require('../../stores/AppStore')
;

module.exports = React.createClass({
    mixins: [StoreDataMixin],
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            events: {
                USER_CHANGE_PASSWORD_SUCCESS: this._passwordSuccess,
                USER_CHANGE_PASSWORD_ERROR: this._passwordError
            }
        }
    },
    getDefaultState: function(){
        return {showPassword: false, ready: false, showToast: false}
    },
    _toggleShowPassword: function(){
        this.setState({showPassword: !this.state.showPassword})
    },
    _passwordSuccess: function(){
        this.setState({
            toast: "Successfully changed password.",
            showToast: true
        });
        setTimeout(this.setState.bind(this,{showToast: false}),4*1000);
    },
    _passwordError: function(){
        console.log("PASSWORD CHANGE ERROR");
    },
    _save: function(){
        UserViewActions.updatePassword(
            this.refs.current.getValue(),
            this.refs.newPassword.getValue(),
            this.refs.confirm.getValue()
        );
        this.setState({
            ready: false
        })
    },
    _checkPassword: function(e,password){
        if(password.length < 6){
            this.setState({passwordError: "Passwords must be at least 6 characters!"});
        } else{
            this.setState({passwordError: undefined})
        }
        this._confirmPasswords(password,this.refs.confirm.getValue());
    },
    _confirmChange: function(e,confirmPassword){
        this._confirmPasswords(this.refs.newPassword.getValue(),confirmPassword)
    },
    _confirmPasswords: function(password,confirm){
        if(confirm !== undefined && password !== confirm){
            this.setState({confirmError:"Passwords do not match!", ready: false});
        } else if(password === confirm) {
            this.setState({confirmError: undefined, ready: true});
        }
    },
    render: function(){
        return (
            <mui.Paper >
                <mui.Toast message={this.state.toast} open={this.state.showToast}/>
                <div className="card layout vertical">
                    <div className="card-header">
                        <h2>Change Password</h2>
                    </div>
                    <div className="card-body">
                        <mui.Input ref="current"
                            type={this.state.showPassword ? "text" : "password"}
                            name="current" placeholder="Current Password"
                        />
                        <mui.Input ref="newPassword"
                            type={this.state.showPassword ? "text" : "password"}
                            name="newPassword"
                            placeholder="New Password"
                            onChange={this._checkPassword}
                            error={this.state.passwordError}
                        />
                        <mui.Input ref="confirm"
                            type={this.state.showPassword ? "text" : "password"}
                            name="confirm"
                            placeholder="Confirm Password"
                            onChange={this._confirmChange}
                            error={this.state.confirmError}
                        />
                        <div className="horizontal layout self-center">
                            <span className="paddedText">Show Passwords</span>
                            <mui.Toggle onToggle={this._toggleShowPassword}/>
                        </div>
                    </div>

                    <div className="self-end">
                        <mui.FlatButton className="button-confirm"
                            onClick={this._save}
                            disabled={!this.state.ready}
                            label="Change Password"
                        />
                    </div>
                </div>
            </mui.Paper>
        )
    }
});