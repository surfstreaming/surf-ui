var React = require('react'),
    Router = require('react-router'),
    Link = Router.Link,
    mui = require('material-ui')
;
// flux
var AuthStore = require('../../stores/AuthStore'),
    UserStore = require('../../stores/UserStore'),
    UserViewActions = require('../../actions/view/UserViewActions')
    ;

// components
var LeftNav = require('../nav/LeftNav'),
    PageContainer = require('../common/PageContainer')
;

module.exports = React.createClass({
    mixins: [Router.State ,Router.Navigation],
    getInitialState: function(){
        return {
            requestedComponents: ["relationship","friends","queues"]
        };
    },
    componentDidMount: function(){
        AuthStore.addChangeListener(this._stateChange);
        this._setUser();
    },
    componentWillUnmount: function(){
        AuthStore.removeChangeListener(this._stateChange);
    },
    _stateChange: function(){
        this._setUser();
    },
    _setUser: function(){
        var id = AuthStore.getAuthId();
        if(id === undefined){
            console.error("You must be logged in to access settings");
        } else {
            var user = AuthStore.getUserObject();
            this.setState({user: user});
        }
    },
    _itemClicked: function(e,idx,item){
        this.transitionTo('settings.'+ item.payload)
    },
    _toggleLeftNav: function(){
        this.refs.container.toggleLeftNav();
    },
    _toggleRightNav: function(){
        this.refs.container.toggleRightNav();
    },
    render: function(){
        var user = this.state.user;
        var username = user === undefined || user.loading ? "" : user.object.username;

        var menuItems = [
            { payload: 'basics', text: 'Basics', icon: 'home' },
            { payload: 'password', text: 'Password', icon: 'home' },
            { payload: 'avatar', text: 'Avatar', icon: 'home' }
        ];

        var nest = <Router.RouteHandler params={this.props.params} query={this.props.query} user={this.state.user} />;
        var leftNav = <LeftNav />;
        var leftButton = <mui.IconButton className="PageContainer-icon" icon="navigation-menu" onClick={this._toggleLeftNav} />;

        return (
            <PageContainer
                ref="container"
                title="Settings"
                type="normal"
                leftNav={leftNav}
                leftButton={leftButton}
                defaultBackground="/images/material3.jpg"
            >
                <div className="panel panel-large layout horizontal">
                    <div style={{paddingRight: 10}} className="layout vertical center">
                        <mui.Menu menuItems={menuItems} onItemClick={this._itemClicked} />
                        <div style={{marginTop: 5}}>
                            <Link to="users.user.profile" params={{username: username}}><mui.FlatButton className="button-confirm" label="View Profile"/></Link>
                        </div>

                    </div>
                    <div className="rightContent panel-medium">
                        { user !== undefined  ? nest : <span>Loading...</span> }
                    </div>
                </div>
            </PageContainer>
        )
    }
});