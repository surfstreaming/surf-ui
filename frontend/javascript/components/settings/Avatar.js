var React = require('react'),
    Router = require('react-router'),
    mui = require("material-ui")
    ;
// Flux
var UserViewActions = require('../../actions/view/UserViewActions'),
    StoreDataMixin = require('../../mixins/StoreDataMixin'),
    AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore')
    ;
// Components
var RadioButton = mui.RadioButton,
    Avatar = require('../common/Avatar'),
    ImgSelector = require('../common/ImgSelector'),
    RadioButtonGroup = require('../common/RadioButtonGroup')
;

module.exports = React.createClass({
    mixins: [StoreDataMixin],
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            events: {
                USER_UPDATE_SUCCESS: this._updateSuccess,
                USER_UPDATE_ERROR: this._updateError
            }
        }
    },
    getInitialState: function(){
        return {
            user: React.addons.update(this.props.user,{})
        }
    },
    _updateSuccess: function(){
        this.setState({
            toast: "Successfully updated avatar.",
            showToast: true
        });
        setTimeout(this.setState.bind(this,{showToast: false}),4*1000);
    },
    _updateError: function(){

    },
    _avatarTypeChange: function(type){
        var userObj = this.props.user.object;
        var newAvatarId = userObj.avatarType === type ? userObj.avatarId : undefined;

        this.setState({
            user: React.addons.update(this.state.user,{
                object: {
                    $merge: {
                        avatarType: type,
                        avatarId: newAvatarId
                    }
                }
            })
        })
    },
    _nameButtonChange: function(nameDisplay){
        var userObj = this.state.user.object;
        var newDisplay;
        if (nameDisplay === "firstLastname"){
            newDisplay = userObj.firstName + " " + userObj.lastName;
        } else if (nameDisplay === "fLastname"){
            newDisplay = userObj.firstName.substring(0,1) + ". " + userObj.lastName;
        } else if (nameDisplay === "firstname"){
            newDisplay = userObj.firstName;
        } else if (nameDisplay === "firstL"){
            newDisplay = userObj.firstName + " " + userObj.lastName.substring(0,1) + ".";
        } else if (nameDisplay === "username"){
            newDisplay = userObj.username;
        } else {
            newDisplay = "unknown";
        }

        this.setState({
            user: React.addons.update(this.state.user,{
                object: {
                    $merge: {
                        displayNameType: nameDisplay,
                        displayName: newDisplay
                    }
                }
            })
        });
    },
    defaultAvatars: [
        { type: "local", id: 1, src: "/images/avatars/default1.png", size: 70},
        { type: "local", id: 2, src: "/images/avatars/default2.png", size: 70},
        { type: "local", id: 3, src: "/images/avatars/default3.jpeg", size: 70},
        { type: "local", id: 4, src: "/images/avatars/default4.jpg", size: 70}
    ],
    _save: function(){
        console.log("UPDATED VALUE IS ",this.state.user.object);
        UserViewActions.updateUser(this.state.user.id,this.state.user.object)
    },
    _renderTypeDetails: function(){
        var type = this.state.user.object.avatarType;
        var label;
        var input = <mui.Input ref="avatarId" name="avatarId" type="text" defaultValue={this.state.user.object.avatarId} onChange={this._avatarIdChange} />;
        if(type === "defaults") {
            label  = "Choose Avatar";
            input = (
                <div style={{width: 200}}>
                    <ImgSelector  images={this.defaultAvatars} onSelect={this._defaultClicked} columns={2} rows={2} />
                </div>
            )
        }
        else if(type === "twitter") {
            label  = "Enter Twitter Handle";
        } else if(type === "facebook") {
            label = "Enter Facebook Username"
        } else if(type === "instagram") {
            label = "Enter Instagram Username"
        } else if(type === "gravatar") {
            label = "Enter Gravatar Email"
        }

        return (
            <div>
                <strong>{label}</strong>
                <div>
                {input}
                </div>
            </div>
        )
    },
    _defaultClicked: function(img){
        this.setState({
            user: React.addons.update(this.state.user,{
                object: {
                    $merge: {
                        avatarId: img.src
                    }
                }
            })
        });
    },
    _avatarIdChange: function(e,newVal){
        this.setState({
            user: React.addons.update(this.state.user,{
                object: {
                    $merge: {
                        avatarId: newVal
                    }
                }
            })
        });
    },
    render: function(){
        return (
            <mui.Paper >
                <mui.Toast message={this.state.toast} open={this.state.showToast}/>
                <div className="card">
                    <div className="card-header">
                        <h2>Avatar</h2>
                    </div>
                    <div className="card-body">
                        <div className="layout vertical">
                            <div>
                                <Avatar
                                    user={this.state.user}
                                    type="badge"
                                />
                            </div>
                            <div className="layout vertical">
                                <h4>Avatar Type</h4>
                                <div className="layout horizontal">
                                    <RadioButtonGroup
                                        defaultValue={this.state.user.object.avatarType}
                                        onChange={this._avatarTypeChange}
                                        name="avatarTypeButtons"
                                        options={[
                                            {value: "defaults", label: "Default"},
                                            {value: "twitter", label: "Twitter"},
                                            {value: "facebook", label: "Facebook"},
                                            {value: "instagram", label: "Instagram"},
                                            {value: "gravatar", label: "Gravatar"}
                                        ]}
                                    />
                                    <div className="flex"></div>
                                    <div className="layout vertical center">
                                        {this._renderTypeDetails()}
                                    </div>
                                </div>
                            </div>
                            <div className="layout vertical">
                                <h4>How would you like your name displayed to others&#63;</h4>
                                <RadioButtonGroup
                                    name="displayName"
                                    defaultValue={this.state.user.object.displayNameType}
                                    onChange={this._nameButtonChange}
                                    options={[
                                        {value: "firstLastname", label: "First Lastname"},
                                        {value: "fLastname", label: "F. Lastname"},
                                        {value: "firstL", label: "First L."},
                                        {value: "username", label: "Username"}
                                    ]}
                                />

                            </div>
                            <div className="self-end">
                                <mui.FlatButton className="button-confirm" onTouchTap={this._save} label="Save"/>
                            </div>
                        </div>
                    </div>
                </div>
            </mui.Paper>
        )
    }
});