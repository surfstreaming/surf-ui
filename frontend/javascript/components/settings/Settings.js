
module.exports = {
    Avatar: require('./Avatar'),
    Base: require('./Base'),
    Basics: require('./Basics'),
    Password: require('./Password')
};