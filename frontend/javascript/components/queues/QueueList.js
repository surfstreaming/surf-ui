// Node Modules
var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui');

// Flux
var AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore'),
    QueueStore = require('../../stores/QueueStore'),
    QueueViewActions = require('../../actions/view/QueueViewActions'),
    UserStore = require('../../stores/UserStore');

// Components
var Avatar = require('../common/Avatar'),
    QueueCardList = require('../common/QueueCardList'),
    LeftNav = require('../nav/LeftNav'),
    PageContainer = require('../common/PageContainer')
;

module.exports = React.createClass({
    mixins: [ Router.Navigation ],
    statics: {
        getTitle: function(){
            return "Queues";
        }
    },

    getInitialState: function() {
        var user = AuthStore.getUserObject();
        if(user !== undefined && Array.isArray(user.queues)){
           return {
               user: user.id,
               queues: user.queues
           }
        } else {
            return {
                user: AuthStore.getAuthId(),
                queues: "unset"
            };
        }

    },
    componentDidMount: function() {
        AppStore.addChangeListener(this._appStateChange);
        if(!Array.isArray(this.state.queues)){
            QueueViewActions.requestQueues(this.state.user);
        }
    },
    componentWillUnmount: function(){
        AppStore.removeChangeListener(this._appStateChange);
    },
    _appStateChange: function(){
        var user = UserStore.getCurrentUser();
        console.log("(QueueList) start state change",user);
        if(user !== undefined && Array.isArray(user.queues)){
            var queues = user.queues;
            this.setState({queues: queues, user: user});
            console.log("(QueueList) end state change");
        } else if(user !== undefined) {
            console.log("USER:",user);
            //QueueViewActions.requestQueues(user.id);
        }
    },
    _goToNewQueue: function(){
        this.transitionTo("queues.new");
    },
    _toggleLeftNav: function(){
        this.refs.container.toggleLeftNav();
    },
    render: function() {
        console.log("(QueueList) RENDER",this.state);
        var queues = <span>Processing...</span>;

        if(Array.isArray(this.state.queues)){
            var queueObjs = this.state.queues.map(function(qid){
                var queue = QueueStore.getQueue(qid);
                var user = UserStore.getUser(AuthStore.getAuthId()); // todo this should be queue.user

                return { queue: queue, user: user };
            });

            queues = <QueueCardList
                queues={queueObjs}
                wrap={false}
                renderIfEmpty={<span>This does not appear in any of your friend's queues.</span>}
            />;
        } else {
            queues = <span>Loading...</span>;
        }

        var fab = <mui.FloatingActionButton icon="content-add" onClick={this._goToNewQueue}/>;

        return (
            <PageContainer
                ref="container"
                title="Queues"
                type="normal"
                defaultBackground="/images/material3.jpg"
                fab={fab}
            >
                <div className="layout vertical center">
                    <div className="panel layout horizontal wrap">
                        {queues}
                    </div>
                </div>
            </PageContainer>
        );
    }
});


