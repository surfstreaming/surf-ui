// Node Modules
var React = require('react'),
    mui = require('material-ui');

// Flux
var AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore'),
    QueueStore = require('../../stores/QueueStore'),
    UserStore = require('../../stores/UserStore'),
    StoreUtils = require('../../utils/StoreUtils'),
    QueueViewActions = require('../../actions/view/QueueViewActions'),
    UserViewActions = require('../../actions/view/UserViewActions')
    ;

// Components
var Backdrop = require('../common/Backdrop');


module.exports = React.createClass({
    componentDidMount: function(){
        AppStore.addChangeListener(this._appStateChange);
        this._processState();
    },
    componentWillUnmount: function(){
        AppStore.removeChangeListener(this._appStateChange);
    },
    _appStateChange: function(){
        this._processState();
    },
    _processState: function(){
        if(StoreUtils.isInBag(UserStore.getUsers(),AuthStore.getAuthId(),["queues"])){
            var allQueues = UserStore.getUsers()[AuthStore.getAuthId()].queues;

            // reduce all of the queues that already contain this content item
            // start folding over all of the queues the user owns
            // return an array containing queues that do not contain the content
            var output = this.props.inQueues.reduce(function(acc,qid){
                var idx = acc.indexOf(qid.queue);
                if(idx >= 0){
                    return React.addons.update(acc,{
                        $splice: [[idx,1]]
                    })
                } else {
                    return acc;
                }

            },allQueues);
            this.setState({
                queuesWithContent: this.props.inQueues,
                queuesWithoutContent: output
            });
        } else {
            if(!UserStore.getUser(AuthStore.getAuthId()).loading){
                UserViewActions.requestUser(AuthStore.getAuthId(),["queues"]);
            }
        }
    },
    getInitialState: function(){
        return {
            queuesWithoutContent: [],
            queuesWithContent: [],
            readyToAdd: [],
            readyToRemove: []
        }
    },
    _itemChecked: function(id,queue,addRemove){
        if(addRemove === "add"){
            this.setState({
                readyToAdd: this._readyStuff(this.state.readyToAdd,id)
            });
        } else {
            this.setState({
                readyToRemove: this._readyStuff(this.state.readyToRemove,id)
            });
        }
    },
    _readyStuff: function(queueStateArray,queueId){
        var idx = queueStateArray.indexOf(queueId);
        if(idx > -1){
            return React.addons.update(queueStateArray,{
                $splice: [[idx,1]]
            });
        } else {
            return React.addons.update(queueStateArray,{
                $push: [queueId]
            });
        }
    },
    _clear: function(){
        this.setState(this.getInitialState());
        this.props.onClose();
    },
    _doCheck: function(id,queue,addRemove){
        var checkbox = this.refs["checkbox"+id];
        checkbox.check();
        this._itemChecked(id,queue,addRemove);
    },
    _onDialogSubmit: function(){
        var contentId = this.props.content.id;
        var add = this.state.readyToAdd.map(function(queueId){
            return {
                addRemove: "add",
                userId: AuthStore.getAuthId(),
                queueId: queueId,
                contentId: contentId
            }
        });

        var privateQueues = this.props.content.private_queues;
        var remove = this.state.readyToRemove.map(function(itemId){
            // TODO this is not ideal, should have proper links from queueItem to queue
            var queueId = privateQueues.reduce(function(acc,qi){
                if(acc) return acc;
                else if(qi.item.id === itemId){
                    return qi.queue;
                } else return undefined;
            },undefined);

            return {
                addRemove: "remove",
                userId: AuthStore.getAuthId(),
                queueId: queueId,
                contentId: contentId,
                itemId: itemId
            }
        });
        console.log("DIALOG SUBMITTED",remove,add);
        QueueViewActions.manageQueueItems(remove.concat(add));
        this.props.onClose();
    },
    render: function(){
        var ready = true;
        return ready ? this._renderDialog() : this._renderLoading();
    },
    _renderLoading: function(){
        return <span>Loading...</span>;
    },
    _renderDialog: function(){
        var addToQueues;
        if(this.state.queuesWithoutContent.length > 0){
            var notInQueues = this.state.queuesWithoutContent.map(function(qid){
                return this._renderQueueItem(qid,QueueStore.getQueue(qid),"add")
            },this);
            addToQueues = <div className="layout vertical center">
                <h4>Add To Queues</h4>
                <div className="full-width">
                    <mui.Paper>
                    {notInQueues}
                    </mui.Paper>
                </div>
            </div>;
        }

        var removeFromQueues;
        if(this.state.queuesWithContent.length > 0){
            var isInQueues = this.state.queuesWithContent.map(function(queueAndItem){
                return this._renderQueueItem(queueAndItem.item.id,QueueStore.getQueue(queueAndItem.queue),"remove")
            },this);

            removeFromQueues = <div className="layout vertical center">
                <h4>Remove From Queues</h4>
                <div className="full-width">
                    <mui.Paper>
                    {isInQueues}
                    </mui.Paper>
                </div>
            </div>;
        }

        var actionButtons = this._renderActionButtons();


        return <div>
            <div>{addToQueues}</div>
            <div>{removeFromQueues}</div>
            <div>{actionButtons}</div>
        </div>;
    },
    _renderQueueItem: function(id,queue,addRemove){

        var bgColor;
        if(addRemove === "add"){
            bgColor = this.state.readyToAdd.indexOf(id) > -1 ? "#dcedc8" : "white";
        } else {
            bgColor = this.state.readyToRemove.indexOf(id) > -1 ? "#f9bdbb" : "white";
        }
        var bgStyle = {
            transition: "background-color 1s",
            padding: 5,
            backgroundColor: bgColor
        };
        return <div key={id} style={bgStyle} className="layout horizontal center linky ">
            <mui.Checkbox className={addRemove === "remove" ? "checkbox-red" : ""} ref={"checkbox"+id} name="checkboxName" value="checkboxValue1" onClick={this._itemChecked.bind(this,id,queue,addRemove)} />
            <div onClick={this._doCheck.bind(this,id,queue,addRemove)} style={{paddingLeft: 5}} className="flex layout horizontal center">
                <div className="flex" style={{minWidth: 150}}>{queue.object.name}</div>
                <Backdrop noLink={true} src={queue.object.splash} size={92}/>
            </div>
        </div>
    },
    _renderActionButtons: function(){
        return (
            <div className="layout horizontal end-justified" style={{marginTop: 30, marginRight: 10}}>
                <mui.FlatButton label="Cancel" className="button-transparent" onClick={this._clear} />
                <mui.FlatButton label="Save" className="button-confirm" onClick={this._onDialogSubmit} />
            </div>
        )
    }
});