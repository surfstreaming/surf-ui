// Node
var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui');

// Flux
var AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore'),
    ContentStore = require('../../stores/ContentStore'),
    QueueStore = require('../../stores/QueueStore'),
    UserStore = require('../../stores/UserStore'),
    QueueViewActions = require('../../actions/view/QueueViewActions');

//Components
var Avatar = require('../common/Avatar'),
    ImgSelector = require('../common/ImgSelector'),
    PageContainer = require('../common/PageContainer'),
    TmdbImg = require('../common/TmdbImg');

module.exports = React.createClass({
    mixins: [ Router.Navigation, Router.State ],
    statics: {
        getTitle: function(){
            return "New Queue";
        }
    },
    getInitialState: function(){
        return {
            queue: {
                object: {}
            }
        }
    },
    componentWillReceiveProps: function(props){
        console.log("NEW PROPS",props,this.props);
    },
    render: function() {
        console.log("RENDER QUEUE NEW",this.props);
        return this._getQueue();
    },
    _create: function(){
        var queue = React.addons.update(this.state.queue,{
            object: {
                name: {$set: this.refs.queueName.getValue()},
                description: {$set: this.refs.description.getDOMNode().value},
                code: {$set: this._codify(this.refs.queueName.getValue())}
            }
        });
        console.log("CREATE",queue);
        if(this._isValid(queue)){
            QueueViewActions.createQueue(queue);
        } else {
            console.log("QUEUE NOT VALID");
        }
    },
    _isValid: function(queue){
        return !(
         queue.object.name === undefined
         || queue.object.access === undefined
        )

    },
    _codify: function(name){
        if(name === undefined) return undefined;
        return encodeURI(
            name
                .toLowerCase()
                .replace(/ /g,"_")
        );
    },
    _accessClicked: function(e,index,item){
        this.setState({
            queue: React.addons.update(this.state.queue,{
                object: {
                    access: {$set: item.payload }
                }
            })
        });
    },
    _accessToIdx: function(access){
        if(access === "private") return 0;
        else if(access === "friends") return 1;
        else if(access === "public") return 2;
    },
    _getQueue: function(){
        var queue = this.state.queue;

        var fab = <mui.FloatingActionButton icon="content-save" onClick={this._create} />;
        var leftButton = <mui.IconButton icon="navigation-arrow-back" onClick={this.goBack} />;
        var rightButton;
        if(AuthStore.getAuth() !== undefined && this.getParams().username === AuthStore.getAuth().username){
            rightButton = <mui.IconButton icon="navigation-close" onClick={this._goToDetail} />;
        }

        var accessItems = [
            { payload: 'private', text: 'Private',data: "Only you can access", icon: "action-lock" },
            { payload: 'friends', text: 'Friends',data: "Only you and your friends", icon: "action-lock-outline" },
            { payload: 'public', text: 'Public',data: "Anyone can access", icon: "action-lock-open" }
        ];
        var accessIdx = this._accessToIdx(queue.object.access);

        return (
            <PageContainer
                title="New Queue"
                type="normal"
                leftButton={leftButton}
                rightButton={rightButton}
                fab={fab}
            >
                <div className="layout horizontal center-justified wrap layoutContainer">
                    <mui.Paper className="panel panel-medium card">
                        <div className="panel">
                            <h5 className="pink">What is the name of the queue&#63;</h5>
                            <mui.Input ref="queueName" defaultValue={queue.object.name} type="text" name="queueName" placeholder="Queue Name"/>
                        </div>
                        <div className="panel">
                            <h5 className="pink">Who can access the queue&#63;</h5>
                            <mui.Menu zDepth={0} autoWidth={false} selectedIndex={accessIdx} menuItems={accessItems} onItemClick={this._accessClicked} />

                        </div>
                        <div className="panel">
                            <h5 className="pink">Queue description</h5>
                            <textarea ref="description" name="Text1" cols="40" rows="5" defaultValue={queue.object.description}></textarea>
                        </div>
                    </mui.Paper>
                </div>



            </PageContainer>
        )
    }
});


