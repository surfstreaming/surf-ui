/**
 * @jsx React.DOM
 */

var React = require('react'),
    Router = require('react-router'),
    Link = Router.Link,
    mui = require('material-ui'),
    Avatar = require('../common/Avatar')
;

var AuthStore = require('../../stores/AuthStore')
;

var menuItems = [
    { route: 'home', text: 'Home' },
    { route: 'queues', text: 'Queues' },
    { route: 'search', text: 'Search' },
    { route: 'friends', text: 'Friends' },
    { route: 'settings', text: 'Settings' }
];
var anonMenuItems = [
    { route: 'home', text: 'Home' },
    { route: 'login', text: 'Log In' }
];

var LeftNav = React.createClass({

    mixins: [Router.Navigation, Router.State],

    getInitialState: function() {
        return {
            selectedIndex: null,
            authId: AuthStore.getAuthId(),
            currentUser: AuthStore.getUserObject()
        };
    },
    componentDidMount: function() {
        AuthStore.addChangeListener(this._authChange);
    },
    componentWillUnmount: function(){
        AuthStore.removeChangeListener(this._authChange);
    },
    _authChange: function(){
        var user = AuthStore.getUserObject();
        var authId = AuthStore.getAuthId();
        this.setState({
            authId: authId,
            currentUser: user
        });

    },
    render: function() {
        var logo = (
            <div style={{borderBottom: "1px solid #ddd", height: "68px"}} className="layout horizontal center center-justified">
                <mui.IconButton icon="navigation-arrow-back" onClick={this.goBack}/>
                <div className="flex layout vertical center ">
                    <img src="/images/logo.png" className="logo-img" style={{padding: 5,height: 55, width: "auto"}}/>
                </div>
                <Link to="home" style={{color: "#333"}}>
                    <mui.IconButton icon="action-home"/>
                </Link>
            </div>
        );

        var header;
        var menuChoice;
        console.log("(LeftNav) render");
        if(this.state.currentUser !== undefined){
            header = <div style={{borderTop: "1px solid #ddd", width: "100%"}} className="SideNavAvatar layout horizontal center" >
                        <Avatar user={this.state.currentUser} size={30} onClick={this._goToProfile}/>
                        <div className="flex layout horizontal end-justified" style={{marginRight: 10}}>
                            <div className="flex">Logged in as <strong>{this.state.currentUser.object.username}</strong></div>
                            <Link to="logout" className="self-end">Logout</Link>
                        </div>

                    </div>;
            menuChoice = menuItems;
        } else {
            header = <div>
                <h4 style={{marginLeft: 10}}>Surf</h4>
                <hr/>
            </div>;
            menuChoice = anonMenuItems;
        }

        return (
            <div>
                {logo}
                <mui.Menu
                    menuItems={ menuChoice }
                    selectedIndex={this._getSelectedIndex()}
                    onItemClick={this._onLeftNavChange}
                    zDepth={0}
                    autoWidth={false}
                />
                {header}

            </div>
        );
    },

    toggle: function() {
        this.refs.leftNav.toggle();
    },

    _getSelectedIndex: function() {
        var currentItem;

        for (var i = menuItems.length - 1; i >= 0; i--) {
            currentItem = menuItems[i];
            if (currentItem.route && this.isActive(currentItem.route)) return i;
        }
    },

    _onLeftNavChange: function(e, key, payload) {
        this.transitionTo(payload.route,payload.params);
    },

    _goToProfile: function() {
        this.transitionTo('users.user.profile',{username: this.state.currentUser.object.username });
        this.refs.leftNav.close();
    }

});

module.exports = LeftNav;