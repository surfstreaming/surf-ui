var React = require('react'),
    AppImg = require('./AppImg'),
    mui = require('material-ui')
;

module.exports = React.createClass({
    getDefaultProps: function(){
        return {
            columns: 1,
            rows: 1,
            controls: "bottom",
            pageChange: function(){},
            onSelect: function(){}
        }
    },
    getInitialState: function(){
        return {
            pages: [[]],
            currentPage: 0,
            pageCount: 0
        }
    },
    componentWillMount: function(){
        this.buildPages(this.props,this.state);
    },
    componentDidMount: function(){
        this.props.pageChange(this.state.pages[0]);
    },
    componentWillReceiveProps: function(props){
        this.buildPages(props);
    },
    nextPage: function(){
        this.goToPage(this.state.currentPage+1);
    },
    prevPage: function(){
        this.goToPage(this.state.currentPage-1);
    },
    goToPage: function(page){
        if(page >= 0 && page < this.state.pages.length){
            this.setState({
                currentPage: page
            });
            this.props.pageChange(this.state.pages[page]);
        }
    },
    getNumberOfPages: function(images,rows,columns){
        var p = 0;
        if(images !== undefined){
            p = Math.ceil(images.length/(rows*columns));
        }
        return p;
    },
    getPageOfImages: function(images,page,rows,columns){
        var begin = rows*columns*(page-1);
        var end = rows*columns*(page);
        return images.slice(begin,end);
    },
    buildPages: function(nextProps){
        var images = nextProps.images,
            rows = nextProps.rows,
            columns = nextProps.columns;
        var pageCount = this.getNumberOfPages(images,rows,columns);

        var pages = [];
        for(var i = 0;i < pageCount; i++){
            pages[i] = this.getPageOfImages(images,i+1,rows,columns);
        }

        this.setState({
            pages: pages,
            pageCount: pageCount
        });
    },
    _imgClicked: function(e){
        this.setState({
            selected: e.id
        });
        this.props.onSelect(e);
    },
   render: function(){

       var pageOfImages;
       if(this.state.pageCount > 0){
           pageOfImages = this.state.pages[this.state.currentPage];
       } else {
           pageOfImages = [];
       }

       var controls = <div className="layout horizontal center center-justified">
                <mui.IconButton icon="navigation-chevron-left" disabled={this.state.currentPage === 0} onClick={this.prevPage} />
                <span> {this.state.currentPage+1} / {this.state.pageCount}</span>
                <mui.IconButton icon="navigation-chevron-right" disabled={this.state.currentPage === this.state.pageCount -1}  onClick={this.nextPage} />
            </div>
       ;

       var columns = this.props.columns;
       var images = pageOfImages.map(function(img){
           var imgStyle = {
               width: Math.floor((100/columns)-(columns*2))+"%",
               margin: "1%",
               cursor: "pointer"
           };

           var classes = {
                selected: this.state.selected === img.id
           };

           var cx = React.addons.classSet;
           return <div className={cx(classes)} style={imgStyle} key={img.id} ><AppImg type={img.type} src={img.src} size={img.size} onClick={this._imgClicked.bind(this,img)}/></div>
       },this);

       return (
           <div>
               { this.props.controls === 'top' || this.props.controls === 'both' ? controls : <span></span>}
               <div className="img-selector layout horizontal wrap">
                   {images}
               </div>
               { this.props.controls === 'bottom' || this.props.controls === 'both' ? controls : <span></span>}
           </div>

       );
   }
});