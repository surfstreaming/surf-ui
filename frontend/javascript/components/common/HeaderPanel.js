var React = require('react'),
    mui = require('material-ui'),
    Paper = mui.Paper
    ;


module.exports = React.createClass({
    // acceptable sizes: 92 185
    // splash sizes: 92, 184, 300, 780
    getDefaultProps: function() {
        return {
            defaultBackground: "/images/material1.png",
            title: "Header"
        };
    },
    render: function(){
        var backgroundUrl = this.props.background === undefined ? this.props.defaultBackground : this.props.background;
        console.log("HEADER:",backgroundUrl,this.props.background,this.props.defaultBackground);
        var headerStyle = {
            backgroundImage: "url(" + backgroundUrl + ")"
        };

        function scaleFontsize(str){
            var rtn = 2.6;
            if (str.length > 10 && str.length <= 15) rtn = 2.2;
            else if (str.length > 15 && str.length <= 20) rtn = 1.8;
            else if (str.length > 20) rtn = 1.4;

            return rtn+"em";
        }
        var fontSize = scaleFontsize(this.props.title);


        var fab;
        if(this.props.fab){
            fab = <div className="header-fab layout horizontal end-justified">
                {this.props.fab}
            </div>;
        } else {
            fab = <span></span>
        }

        return (
            <div className="header-panel layout horizontal center">
                <Paper className="card flex">
                    <div style={headerStyle} className="header-panel-container layout vertical start">
                        <div className="header-panel-toolbar layout horizontal">
                            <span className="self-start">{typeof this.props.leftButton === 'undefined' ? <span></span> : this.props.leftButton}</span>
                            <span className="flex"></span>
                            <span className="self-end">{typeof this.props.rightButton === 'undefined' ? <span></span> : this.props.rightButton}</span>
                        </div>
                        <span className="flex self-center">{this.props.centerpiece}</span>
                        <h2 className="header-panel-title" style={{fontSize: fontSize}}>{this.props.title}</h2>
                    </div>
                    {fab}
                    {this.props.children}
                </Paper>
            </div>
        )
    }
});