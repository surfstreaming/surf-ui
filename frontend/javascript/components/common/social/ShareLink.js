var React = require('react')
;


var doc = typeof document === 'undefined' ? {} : document;
!function(d,s,id){
    if(typeof document !== 'undefined'){
        var js,
            fjs=d.getElementsByTagName(s)[0],
            p=/^http:/.test(d.location)?'http':'https';

        if(!d.getElementById(id)){
            js=d.createElement(s);
            js.id=id;
            js.src=p+'://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js,fjs);
        }
    }

}(doc, 'script', 'twitter-wjs');

var ShareLink = React.createClass({
    getDefaultProps: function(){
        return {
            type: "twitter",
            url: "http://surfbeta.com",
            twitterVia: "surfstreaming",
            text: ""
        }
    },
    componentDidMount: function(){
        if(this.props.type === "twitter") {
            this.addScript("twitter-wjs","http://platform.twitter.com/widgets.js");
        } else if(this.props.type === "facebook") {
            this.addScript("facebook-jssdk","http://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0");
        }
    },
    addScript: function(id,script){

        var link =  this.refs.link.getDOMNode();
        if(typeof document !== 'undefined'){
            var js = document.createElement("script"),
                p=/^http:/.test(document.location)?'http':'https';

            js.src = script;
            js.id=id;
            link.parentNode.appendChild(js);
        }

    },
    render: function(){
        if(this.props.type === "twitter") {
            return this.renderTwitter();
        } else if(this.props.type === "facebook") {
            return this.renderFacebook();
        }
    },
    renderTwitter: function(){
        return (
            <div>
                <a href="https://twitter.com/share"
                    ref="link"
                    className="twitter-share-button"
                    data-url={this.props.url}
                    data-text={this.props.text}
                    data-size="large"
                    data-count="none"
                >
                    {this.props.label}
                </a>
            </div>
        )
    },
    renderFacebook: function(){
        return (
            <div>
                <div ref="link"
                    className="fb-share-button"
                    data-layout="icon_link"
                    data-href={this.props.url}
                ></div>
            </div>
        )

    }

});

module.exports = ShareLink;