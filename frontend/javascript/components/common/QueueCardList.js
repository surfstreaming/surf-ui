var React = require('react'),
    QueueCard = require('./QueueCard'),
    Collapsible = require('./Collapsible'),
    Avatar = require('./Avatar')
;


module.exports = React.createClass({
    getDefaultProps: function(){
        return {
            label: "Queues",
            renderIfEmpty: <span>No Queues</span>,
            showAvatar: true,
            wrap: true
        };
    },

    render: function(){
        var queues = this.props.queues;
        var missing = this.props.renderIfEmpty;
        var label = this.props.label;
        var showAvatar = this.props.showAvatar;
        var count = Array.isArray(queues) ? queues.length : 0;

        var content = missing;
        if(count > 0){

            var queuesDOM = queues.map(function(queueItem){
                return <QueueCard key={queueItem.queue.id} queue={queueItem.queue} user={queueItem.user} pending={queueItem.pending} showAvatar={showAvatar}  />;
            });
            content = <div className="layout horizontal start wrap">{queuesDOM}</div>;
        }

        if(this.props.wrap){
            return (
                <Collapsible title={label + "("+count+")"} show={ count < 5 }>
                {content}
                </Collapsible>
            )
        } else {
            return <div>{content}</div>
        }


    }
});