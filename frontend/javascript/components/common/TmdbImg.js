var React = require('react')
;


module.exports = React.createClass({
    statics: {
        getURL: function(src,size){

            if(src !== undefined && !isNaN(size) ){
                return "http://image.tmdb.org/t/p/w" + size +  src;
            } else if(src !== undefined){
                return "http://image.tmdb.org/t/p/original/"+src;
            }
        }
    },
    // acceptable sizes: 92 185
    // splash sizes: 92, 184, 300, 780
    getDefaultProps: function() {
        return {
            size: 92,
            default: "/images/missing.png"
        };
    },
    render: function(){
        if(this.props.src !== undefined){
            var url = "http://image.tmdb.org/t/p/w" + this.props.size +  this.props.src;
            return (
                <img className="AppImg" src={url} onClick={this.props.onClick} />
            )
        } else {
            return <img src={this.props.default} className="AppImg"  />
        }
    }
});