var React = require('react'),
    mui = require('material-ui'),
    AppImg = require('./AppImg'),
    Link = require('react-router').Link
;

module.exports = React.createClass({

    getDefaultProps: function(){
        return {
            to: "movies.detail",
            size: 300, //184
            noLink: false
        }
    },
    _sizeToClass: function(size){
        if(size === 92) return "backdrop-small";
        else return "backdrop"
    },
    render: function(){
        return this.props.noLink ? this.renderNoLink() : this.renderWithLink();
    },
    renderNoLink: function(){

        return (
            <div className={this._sizeToClass(this.props.size)}>
                <AppImg type="tmdb" src={this.props.src} className="backdrop" size={this.props.size} default="/images/defaultLandscape.jpg" />
            </div>
        )
    },
    renderWithLink: function(){
        return (
            <div className={this._sizeToClass(this.props.size)}>
                <Link to={this.props.to} params={this.props.params}>
                    <AppImg type="tmdb" src={this.props.src} className="backdrop" size={this.props.size} default="/images/defaultLandscape.jpg" />
                </Link>
            </div>
        )
    }
});