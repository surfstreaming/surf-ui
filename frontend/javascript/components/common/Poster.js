var React = require('react'),
    AppImg = require('./AppImg'),
    Link = require('react-router').Link
;

module.exports = React.createClass({

    getDefaultProps: function(){
        return {
            to: "movies.detail",
            size: "medium",
            noLink: false
        }
    },
    render: function(){
        var imgSize;
        var className;
        if(this.props.size === "medium"){
            imgSize = 92;
            className = "poster"
        } else if (this.props.size === "large"){
            imgSize = 184;
            className = "poster-large"
        }

        var img = <AppImg type="tmdb" src={this.props.src} size={imgSize} default="/images/defaultPoster.png" />;

        if(this.props.noLink){
            return (
                <div className={className}>
                    <div className="poster-img">{img}</div>
                    {this.props.children}
                </div>
            )
        } else {
            return (
                <div className={className}>
                    <Link to={this.props.to} params={{contentid: this.props.contentid}}>
                        {img}
                        {this.props.children}
                    </Link>
                </div>
            )
        }

    }
});