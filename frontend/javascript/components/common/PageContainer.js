var React = require('react'),
    mui = require('material-ui'),
    Router = require('react-router'),
    LeftNav = require('../nav/LeftNav')
;

var ScrollListenerMixin = require('../../mixins/ScrollListenerMixin');


var PageContainerNav = React.createClass({

    getInitialState: function(){

        return {
            open: false,
            toolbarScrollClass: this._getToolbarScrollClass(0)
        };
    },
    render: function(){
        return <span>foo</span>
    }
});

var PageContainer = React.createClass({
    mixins: [ScrollListenerMixin, Router.Navigation],
    getDefaultProps: function(){
        return {
            center: false,
            type: "header",
            toolbarClass: ""
        }
    },
    getInitialState: function(){

        return {
            expanded: true,
            rightNavOpen: false,
            leftNavOpen: false,
            navOpen: false,
            toolbarScrollClass: this._getToolbarScrollClass(0,550)
        };
    },
    propTypes: {
        title: React.PropTypes.string,
        text: React.PropTypes.string
    },
    onPageScroll: function(pos){
        var switchPos = 550;
        var toolbarScrollClass = this._getToolbarScrollClass(pos,switchPos);
        if(toolbarScrollClass !== this.state.toolbarScrollClass){
            this.setState({toolbarScrollClass: toolbarScrollClass})
        }
    },
    _getToolbarScrollClass: function(pos,switchPos){
        var toolbarScrollClass;
        if(this.props.type === "normal"){
            toolbarScrollClass = "main";
        } else {

            if(pos < switchPos){
                toolbarScrollClass = "top";
            } else {
                toolbarScrollClass = "main";
            }
        }

        return toolbarScrollClass;
    },
    goToAfterHeader: function(){
        var scrollPos = this.refs.content.getDOMNode().scrollTop;
        window.scrollTo(0,scrollPos);
    },
    _goToSearch: function(){
        this.transitionTo("search");
    },
    toggleLeftNav: function(){
        this.setState({leftNavOpen: !this.state.leftNavOpen, navOpen: (!this.state.leftNavOpen || this.state.rightNavOpen)});
    },
    toggleRightNav: function(){
        this.setState({rightNavOpen: !this.state.rightNavOpen, navOpen: (this.state.leftNavOpen || !this.state.rightNavOpen)});
    },
    _dismissNav: function(){
        this.setState({rightNavOpen: false, leftNavOpen: false, navOpen: false});
    },
    render: function(){

        var containerMainClass = "";
        if(this.state.leftNavOpen){
            containerMainClass = " show-nav-left"
        } else if(this.state.rightNavOpen){
            containerMainClass = " show-nav-right"
        }


        var leftNav = <LeftNav />;
        return (
            <div className="container">
                <div className={"container--leftnav" + (this.state.leftNavOpen ? " show-nav" : "")}>
                    {leftNav}
                </div>
                <div className={"container--main" + containerMainClass}>
                    <div className={"container--main--overlay"+ (!this.state.navOpen ? " container--main--overlay__hidden" : "")} onClick={this._dismissNav}></div>
                    <div className="container--main--header">
                        {this._renderToolbar()}
                    </div>
                    {this._renderSplash()}

                    {this.props.afterHeader}
                    <div ref="content" className={"container--main--content" + (this.props.contentClass !== undefined ? " "+this.props.contentClass : "")}>
                        {this.props.children}
                    </div>
                </div>
                <div className={"container--rightnav" + (this.state.rightNavOpen ? " show-nav" : "")}>
                    {this.props.rightNav}
                </div>
            </div>

        )
    },
    _renderToolbar: function(){
        var leftButton = this.props.leftButton || <mui.IconButton icon="navigation-menu" onClick={this.toggleLeftNav} />;
        var rightButton = this.props.rightButton || <span style={{width: 32}}/>;

        return (
            <div className={"container--main--header--toolbar container--main--header--toolbar__" + this.state.toolbarScrollClass + " " + this.props.toolbarClass }>
                <div className="PageContainer-icon">{leftButton}</div>
                <span className="container--main--header--toolbar--title">{this.props.title}</span>
                <span className="flex"></span>
                <div className="container--main--header--toolbar--fab toolbar-fab self-end">{this.props.fab}</div>
                <div className="PageContainer-icon">{rightButton}</div>
            </div>
        )
    },
    _renderSplash: function(){

        var headerBackground = this.props.background ? { backgroundImage: "url("+this.props.background+")" } : {} ;
        var colorOverlay = this.props.colorOverlay ? { backgroundColor: this.props.colorOverlay } : {} ;

        if(this.props.type === "normal"){
            return <div style={{paddingTop: 68 }}></div>;
        }

        return (
            <div style={headerBackground} className="container--main--splash" >
                <div style={colorOverlay} className="flex container--main--splash--body">
                    <div className={"flex layout vertical " + (this.props.center ? "center-justified" : "end-justified") } style={{display: this.props.type === "splash" ? undefined : "none" }}>
                        <strong className="container--main--splash--title">{this.props.title}</strong>
                        <div className="PageContainer-extras container--main--splash--extras">
                                {this.props.header}
                        </div>
                    </div>

                    <div className="container--main--splash--bottom">
                        <div className="layout horizontal center-justified center">
                            <mui.IconButton className="flex bigIcon" icon="navigation-expand-more" onClick={this.goToAfterHeader}/>
                            <div className="container--main--splash--body--fab">{this.props.fab}</div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
});

module.exports = PageContainer;