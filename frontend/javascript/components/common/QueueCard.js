var React = require('react'),
    Link = require('react-router').Link,
    mui = require('material-ui'),
    Backdrop = require('./Backdrop'),
    Avatar = require('./Avatar')
    ;


var QueueCard = React.createClass({
    // acceptable sizes: 92 185
    // splash sizes: 92, 184, 300, 780
    getDefaultProps: function() {
        return {
            showAvatar: true
        };
    },
    render: function(){
        var avatar = <div><Avatar user={this.props.user}/></div>;
        var classes = "layout vertical center queue-card " + this.props.pending + (this.props.queue.deleted ? " queue-card__deleted" : "");
        var queue = (
            <div className={classes}>
                <Backdrop to="users.user.queues.queue.detail" params={{username: this.props.user.object.username, queueid: this.props.queue.id}} src={this.props.queue.object.splash}/>
                <Link className="queue-card--link" to="users.user.queues.queue.detail" params={{username: this.props.user.object.username, queueid: this.props.queue.id}} >{this.props.queue.object.name}</Link>
            </div>
        );

        if(this.props.showAvatar){
            return (
                <div className="layout horizontal">
                    {queue}
                    {avatar}
                </div>
            )
        } else {
            return queue;
        }
    }
});

module.exports = QueueCard;