var React = require('react'),
    TmdbImg = require('./TmdbImg'),
    mui = require('material-ui'),
    Paper = mui.Paper
    ;


module.exports = React.createClass({
    // acceptable sizes: 92 185
    // splash sizes: 92, 184, 300, 780
    getDefaultProps: function() {
        return {
            size: 92,
            default: "/images/missing.png"
        };
    },
    render: function(){
        if(this.props.type === 'tmdb'){
            return <Paper><TmdbImg src={this.props.src} size={this.props.size} default={this.props.default} onClick={this.props.onClick} /></Paper>
        }
        if(this.props.type === 'local'){
            var style = {
                width: this.props.size
            };
            return <Paper><img src={this.props.src} className="AppImg" onClick={this.props.onClick} /></Paper>
        }
        else {
            return <span>error</span>
        }
    }
});