var React = require('react'),
    mui = require('material-ui')
    ;


module.exports = React.createClass({
    // acceptable sizes: 92 185
    // splash sizes: 92, 184, 300, 780
    getDefaultProps: function() {
        return {
            show: false,
            openIcon: "navigation-expand-more",
            closedIcon: "navigation-expand-less"
        };
    },
    getInitialState: function(){
        return {
            visible: this.props.show
        }
    },
    componentWillReceiveProps: function(props){
        this.setState({visible: props.show});
    },
    _toggle: function(){
       this.setState({visible: !this.state.visible});
    },
    render: function(){
        var icon = this.state.visible ? this.props.openIcon : this.props.closedIcon;
        var header = (
            <div className="collapsible-header layout horizontal center" onClick={this._toggle}>
                <mui.IconButton icon={icon}/><h4>{this.props.title}</h4>
            </div>
        );
        var cx = React.addons.classSet;
        var contentClasses = cx({
            'collapsible-content': true,
            'collapsible-content-hidden': !this.state.visible,
            'collapsible-content-visible': this.state.visible
        });

        return (
            <div>
                {header}
                <div className={contentClasses}><div>{this.props.children }</div></div>
            </div>
        )
    }
});