var React = require('react'),
    Link = require('react-router').Link,
    mui = require("material-ui"),
    Paper = mui.Paper
;


var Avatar = React.createClass({
    getDefaultProps: function() {
        return {
            zDepth: 1,
            size: 40,
            noLink: false
        };
    },
    propTypes: {
        size: React.PropTypes.number,
        type: React.PropTypes.string,
        user: React.PropTypes.object.isRequired
    },
    getAvatarURL: function(){
        if(this.props.user.object.avatarType != "defaults"){
            return "http://avatars.io/" + this.props.user.object.avatarType + "/" + this.props.user.object.avatarId +"?size=" + this.props.size;
        } else {
            return this.props.user.object.avatarId;
        }
    },
    isValidUser: function(){
        return this.props.user.id !== undefined;
    },
    render: function(){
        return this.props.user ? this._renderSome() : this._renderNone();
    },
    _renderNone: function(){
        return (<span>Loading avatar...</span>)
    },
    _renderSome: function(){

        var url = this.getAvatarURL();
        var paperStyle = {
            height: this.props.size + "px",
            width: this.props.size + "px",
            overflow: "hidden",
            marginRight: "5px"
        };
        var avatarStyle = {
            height: this.props.size + "px",
            width: this.props.size + "px",
            backgroundImage: "url("+url+")",
            backgroundSize: "100% 100%"
        };

        var avatar = (
            <div style={paperStyle}>
                <Paper className="overflow-hidden" zDepth={this.props.zDepth} circle={true} >
                    <div style={avatarStyle}></div>
                </Paper>
            </div>
        );

        var badge = (
            <div className="layout vertical">
                <strong>{this.props.user.object.displayName}</strong>
                <span>{this.props.user.object.username}</span>
            </div>
        );

        if(this.isValidUser()) {
            var output = avatar;
            if(this.props.type === "badge"){
                output = (
                    <div className="layout horizontal">
                        {avatar}
                        {badge}
                    </div>
                )
            }
            else if (this.props.type === "low-profile"){
                output = (
                    <div className="layout horizontal center">
                        {avatar}
                        <small>{this.props.user.object.displayName}</small>
                    </div>
                )
            }

            if(this.props.noLink){
                return output;
            } else {
                return <Link className="avatarLink" to="users.user.profile" params={{username: this.props.user.object.username}} >{output}</Link>
            }

        } else {
            return <span>Invalid user</span>
        }

    }
});

module.exports = Avatar;