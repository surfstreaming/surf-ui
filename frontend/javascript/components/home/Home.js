var React = require('react'),
    mui = require('material-ui'),
    Router = require('react-router')
;
// Flux
var AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore'),
    ContentStore = require('../../stores/ContentStore'),
    QueueStore = require('../../stores/QueueStore'),
    HomeViewActions = require('../../actions/view/HomeViewActions'),
    StoreDataMixin = require('../../mixins/StoreDataMixin')
;
// components
var SignupSplash = require('./SignupSplash'),
    AuthenticatedSplash = require('./AuthenticatedSplash'),
    PageContainer = require('../common/PageContainer'),
    Login = require('../auth/Login')
;


module.exports = React.createClass({
    mixins: [Router.Navigation],
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            requests: {
                content: {
                    retriever: ContentStore.getFeaturedContent,
                    action: HomeViewActions.requestFeaturedContent
                },
                favorites: {
                    id: AuthStore.getAuthId(),
                    retriever: QueueStore.getFavorites,
                    action: HomeViewActions.requestFavoriteQueues
                }
            },
            events: {}
        }
    },
    statics: {
        getTitle: function(){
            return "Surf";
        }
    },
    render: function(){
        console.log("RENDER HOME",this.state,AuthStore.getAuth());

        if(AuthStore.getAuthId() === undefined){
            return <SignupSplash  />;
        }
        else {
            return <AuthenticatedSplash />;
        }
    }
});