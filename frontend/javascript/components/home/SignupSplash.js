var React = require('react'),
    mui = require('material-ui'),
    Router = require('react-router')
    ;
// Flux
var AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore'),
    UserStore = require('../../stores/UserStore'),
    QueueStore = require('../../stores/QueueStore'),
    StoreDataMixin = require('../../mixins/StoreDataMixin'),
    MediaQueryMixin = require('../../mixins/MediaQueryMixin'),
    AuthViewActions = require('../../actions/view/AuthViewActions'),
    HomeViewActions = require('../../actions/view/HomeViewActions')
    ;
// components
var PageContainer = require('../common/PageContainer'),
    QueueCardList = require('../common/QueueCardList'),
    Login = require('../auth/Login')
    ;


var SignupSplash = React.createClass({
    mixins: [StoreDataMixin, MediaQueryMixin, Router.Navigation],
    getInitialState: function(){
        return {
            disableRegister: false
        }
    },
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            requests: {
                favorites: {
                    id: AuthStore.getAuthId(),
                    retriever: QueueStore.getFavorites,
                    action: HomeViewActions.requestFavoriteQueues
                }
            },
            events: {
                AUTH_QUICK_REGISTER_SUCCESS : this.registerSuccess,
                AUTH_QUICK_REGISTER_ERROR : this.registerError,
                AUTH_QUICK_REGISTER_REQUEST : this.registerRequest
            }
        }
    },
    statics: {
        getTitle: function(){
            return "Surf";
        }
    },
    _quickSignup: function(num){
        this._checkValidEmail(this.refs["email"+num].getValue());
    },
    _checkValidEmail: function(email){
        var emailRegex = /^.*@.*.[A-Za-z0-9]*$/;

        if(email.length < 3){
        }
        else if(emailRegex.exec(email)){
            this.setState({
                checkingEmail: email,
                emailDescription: "Checking if " + email + " is available...",
                emailError: undefined
            });
            AuthViewActions.quickRegister(email);
        } else {
            this.setState({
                emailError: "This does not look like a valid email address"
            });
        }
    },
    registerRequest: function(){
        this.setState({ disableRegister : true});
    },
    registerError: function(){
        this.setState({
            checkingEmail: undefined,
            emailDescription: undefined,
            emailError: "This email has already been registered!",
            disableRegister: false
        });
    },
    registerSuccess: function(event){
        var email = event.action.email;
        this.refs.email1.clearValue();
        this.refs.email2.clearValue();
        if(email === this.state.checkingEmail){
            this.setState({
                checkingEmail: undefined,
                emailDescription: "Email available for registration",
                emailError: undefined,
                error: undefined,
                registered: true,
                disableRegister: true
            });
        }
    },
    _toggleLogin: function(){
        this.refs.container.toggleRightNav();
    },
    _toggleLeft: function(){
        this.refs.container.toggleLeftNav();
    },
    render: function(){
        console.log("RENDER SIGNUP SPLASH",this.props);
        var sampleQueues;
        if(Array.isArray(this.state.favorites)){
            sampleQueues = this.state.favorites.map(function(q){
                var user = UserStore.getUser(q.user);
                return {user: user, queue: q};
            });
        } else {
            sampleQueues = "loading";
        }

        var isSmall = ["phones","phonesLandscape"].indexOf(this.state.media) > -1;
        var titleStyle = {color: "white", lineHeight: "125%"};
        titleStyle.fontSize = 46;

        var header = (
            <mui.Paper style={{textAlign: "center", backgroundImage: "url(/images/home/call_to_action_splash.jpg)"}}>
                <div  className="layout vertical center" style={{padding: "30px 0"}}>
                    <div className="panel panel-large layout vertical center center-justified" style={{padding: 10, color:"white", lineHeight: "125%", fontSize: "20px"}}>
                        <h1 style={titleStyle}>Stop Searching, Start <i>Surfing</i></h1>
                        <div>
                            Tired of hunting down where you can stream your favorite shows&#63;
                        </div>
                        <div><strong>We can help.</strong></div>
                        <div style={{margin: "0 0 20px 0"}}>Sign up for free to get in early: </div>
                    </div>
                    <div>
                        {this.renderSignup(1)}
                    </div>
                </div>
            </mui.Paper>
        );

        //var leftButton = <mui.IconButton className="PageContainer-icon" icon="navigation-menu" onClick={this.props.showMenu} />;
        var leftButton = <img src="/images/logo.png" style={{width: 40, height: 40}}/>;
        var rightButton = <mui.FlatButton className="button-confirm" label="Log In" onClick={this._toggleLogin} />;

        var loginNav = this.renderLogin();

        return (
            <PageContainer
                ref="container"
                center={true}
                title="Surf"
                type="normal"
                afterHeader={header}
                toolbarClass="toolbar-white"
                leftButton={leftButton}
                rightButton={rightButton}
                rightNav={loginNav}
                contentClass="container--main--content__nobleed"
            >
                <div className="full-width layout vertical center">
                    <div className="signup-info-panel">
                        <div className="panel panel-large layout horizontal wrap">
                            <div className="panel panel-medium layout vertical center">
                                <h3>Search Across Services</h3>
                                <div>
                                With all of the different services out there, it is hard to keep track of where everything is.
                                Surf let's you search across all the major providers to find where it can be streamed.
                                </div>
                            </div>
                            <div className="panel panel-small layout vertical center">
                                <div style={{width: "75%"}}><img src="/images/home/streaming_services.png" style={{width: "100%"}} /></div>
                            </div>

                        </div>
                    </div>
                    <div style={{borderTop: "1px solid #ccc", width: "75%"}}></div>
                    <div className="signup-info-panel">
                        <div className="panel panel-large layout horizontal wrap">
                            <div className="panel panel-small layout vertical center">
                                <div style={{width: "80%"}}><img src="/images/home/create.png" style={{width: "100%"}} /></div>
                            </div>
                            <div className="panel panel-medium layout vertical center">
                                <h3>What Will You Create&#63;</h3>
                                <div>
                                    Make queues to organize movies and shows however you like. Keep your Saturday Morning Cartoons and separate from your Crime Dramas -
                                    then share your creations with your friends on Facebook or Twitter.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="signup-info-panel" style={{backgroundColor: "white", border: "1px solid #ccc"}}>
                        <div className="panel panel-large layout vertical center" style={{paddingBottom: 15}}>
                            <h2>Featured Queues</h2>
                            <QueueCardList queues={sampleQueues} showAvatar={false} wrap={false} />
                        </div>
                    </div>
                </div>
                <div className="signup-info-panel">
                    <div className="panel panel-large layout vertical center">
                        <h2>Is It Really Free&#63;</h2>
                        <div>
                        Although Surf itself is free, you still need to pay for the underlying services to actually watch the content.
                        When you find an item you would like to watch, we will direct you to the provider's site where you can play it.
                        </div>
                        <p>

                        </p>
                    </div>
                </div>
                <mui.Paper zDepth={4} className="full-width" >
                    <div className="signup-info-panel layout vertical center full-width" style={{color: "white", backgroundImage: "url(/images/home/call_to_action_splash.jpg)"}}>
                        <h2 style={{color: "white"}}>I'm in. Sign me up!</h2>
                        <div style={{marginBottom: 20}}>Enter your email address to sign up for early access:</div>
                        <div></div>
                        {this.renderSignup(2)}
                    </div>

                </mui.Paper>
                <div className="signup-info-panel"></div>
                <div className="signup-info-panel" style={{backgroundColor: "#333", color: "white", border: "1px solid #ccc"}}>
                    {this.renderFooter()}
                </div>
            </PageContainer>
        )
    },
    renderFooter: function(){
        var items = [
        ];

        return <div className="footer">
            <mui.Menu zDepth={0} menuItems={items} autoWidth={false} />
        </div>


    },
    renderSignup: function(num){
        return (
            <div style={{marignTop: 15, marginBottom: 15}}>
                <div className="layout horizontal center center-justified">
                    <mui.Input
                        ref={"email"+num}
                        className="quick-signup-input"
                        type="text"
                        name="email"
                        description={this.state.emailDescription}
                        error={this.state.emailError}
                        placeholder="Email Address" />
                    <mui.RaisedButton className="button-pink" label="Sign Up" onClick={this._quickSignup.bind(this,num)} disabled={this.state.disableRegister} />
                </div>

                { this.state.registered ? this.renderSuccess() : undefined}
            </div>
        )
    },
    renderLogin: function(){
        var style={marginTop: 90, boxShadow: "0 2px 2px rgba(0,0,0,0.2)", borderTop:"1px solid #ccc"};
        return (
            <div style={style}>
                <Login />
            </div>
        )
    },
    renderSuccess: function(){
        var style = {
            backgroundColor: "rgba(255,64,129,0.5)",
            padding: 6,
            color: "white",
            marginTop: 10
        };
        return (
            <div style={style}>
            You're In! Check your email to complete the registration process!
            </div>
        )
    }

});

module.exports = SignupSplash;