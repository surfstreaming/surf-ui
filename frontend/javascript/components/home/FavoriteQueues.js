var React = require('react'),
    Avatar = require('../common/Avatar'),
    Poster = require('../common/Poster'),
    Link = require('react-router').Link,
    UserStore = require('../../stores/UserStore'),
    ContentStore = require('../../stores/ContentStore')
;

module.exports = React.createClass({
    render: function(){
        console.log("RENDERING FAVORITE QUEUES",this.props.queues);
        if(Array.isArray(this.props.queues) && this.props.queues.length > 0){
            function renderQueue(queue){
                var user =  UserStore.getUser(queue.user);
                var items = queue.first_ten || ( Array.isArray(queue.content) ? queue.content.splice(0,9) : [] );
                var content = items.length > 0 ? renderContent(items) : <span>No items in this queue</span> ;
                return (
                    <div key={queue.id}>
                        <div className="layout horizontal center wrap">
                            <h4 style={{marginRight: 5}}>
                                <Link to="users.user.queues.queue.detail" params={{queueid: queue.id, username: user.object.username}}>{queue.object.name}</Link>
                            </h4>
                            <Avatar user={user} type="low-profile" size={30}/>
                        </div>
                        <div className="layout horizontal wrap">
                            {content}
                        </div>
                    </div>
                )
            }
            function renderContent(items){
                return items.map(function(contentItem){
                    var content = ContentStore.getItem(contentItem.content);
                   return <Poster key={contentItem.item.id} contentid={content.id} src={content.object.posterPath} />
                });
            }
            var queues = this.props.queues.map(renderQueue);

            return (
                <div>
                    {queues}
                </div>
            )
        } else {
            return (
                <div>
                    No Favorite Queues!
                </div>
            )
        }

    }
});