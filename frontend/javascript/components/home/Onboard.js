var React = require('react'),
    mui = require('material-ui')
;
// Flux
var AuthStore = require('../../stores/AuthStore'),
    UserViewActions = require('../../actions/view/UserViewActions')
;


var Onboard = React.createClass({
    getInitialState: function(){
        return {
            items: [
                {key: 0, title: "Let's Get Started", render: this.renderFirst},
                {key: 1, title: "Add items to your queue", render: this.renderAddItem, check: this.hasQueueWithItems},
                {key: 2, title: "Find your friends", render: this.renderFindFriends, check: this.sentAFriendRequest},
                {key: 3, title: "Change your avatar", render: this.renderChangeAvatar, check: this.doesNotHaveDefaultAvatar},
                {key: 4, title: "Add a \"favorite\" queue", render: this.renderFavoriteQueue, check: this.hasFavoriteQueues},
                {key: 5, title: "You're Good to Go!", render: this.renderComplete}
            ],
            current: 0
        }
    },
    hasQueueWithItems: function(){
        var user = AuthStore.getUserObject();
        if(user !== undefined){
            if(user.queues !== undefined){
                return user.queues.reduce(function(acc,cur){
                    if(acc) return acc;
                    else return false;
                },false);
            }
        }
        return false;
    },
    hasFavoriteQueues: function(){
        return Array.isArray(this.props.favorites) && this.props.favorites.length > 0;
    },
    doesNotHaveDefaultAvatar: function(){
        var user = AuthStore.getUserObject();
        if(user !== undefined && user.object !== undefined){
            return user.object.avatarType !== "defaults";
        }
        return false;

    },
    sentAFriendRequest: function(){
        return false;
    },
    goToCard: function(key){
        this.setState({current: key});
    },
    _completeOnboarding: function(){
        console.log("COMPLETE");
        var user = AuthStore.getUserObject();
        var userObj = user.object;

        var newUserObj = React.addons.update(userObj,{
            $merge: {onboarded: true}
        });

        UserViewActions.updateUser(user.id,newUserObj)
    },
    render: function(){
        var item = this.state.items[this.state.current];

        return (
            <div className="layout vertical center">
                <div className="panel panel-medium">
                    {this.renderContainer(item)}
                </div>
            </div>
        )
    },
    renderContainer: function(item){
        var circles = this.state.items.map(function(item){
            var style = {
                fontSize: 12,
                margin: 10,
                cursor: "pointer"
            };
            var icon = this.state.current === item.key ? "toggle-radio-button-on" : "toggle-radio-button-off";
            return <mui.Icon icon={icon} key={item.key} style={style} onClick={this.goToCard.bind(this,item.key)}/>;
        },this);

        var backDisabled = this.state.current === 0;
        var forwardDisabled = this.state.current === this.state.items.length - 1;
        return (
            <div key={item.key} className="card card__rounded card__shadow">
                <div className="card--header">{item.title}</div>
                <div className="card--body">{item.render()}</div>
                <div className="card--footer layout horizontal">
                    <mui.IconButton icon="navigation-arrow-back" disabled={backDisabled} onClick={this.goToCard.bind(this,this.state.current-1)}/>

                    <div className="flex layout horizontal center center-justified">
                        {circles}
                    </div>
                    <mui.IconButton icon="navigation-arrow-forward" disabled={forwardDisabled} onClick={this.goToCard.bind(this,this.state.current+1)}/>
                </div>
            </div>
        );
    },

    renderFirst: function(){
        var itemList = this.state.items.slice(1,this.state.items.length-1).map(function(item){
            var style = {
                textDecoration: item.check() ? "line-through" : "none"
            };
            return <li key={item.key} style={style}>{item.title}</li>
        });
        return (
            <div>
                There are just a few things needed to get up and running:
                <ol style={{marginLeft: 30}}>
                    {itemList}
                </ol>
            </div>
        )
    },
    renderAddItem: function(){
        return (
            <div>
                <div>
                    We have created a queue to get you started, but there is nothing in it yet.
                    To view your queue, open the main menu <mui.Icon icon="navigation-menu"/> and select "Queues", then select "My Queue".
                    Add an item to your queue then come back "Home".
                </div>
                <br/>
                <div>Go ahead, we'll wait.</div>
            </div>
        )
    },
    renderFindFriends: function(){
        return (
            <div>
                <div>
                You can share the queues you have created with your friends and see what they recommend.
                Find each other by sharing your profile on Facebook or Twitter.
                </div>
                <div>
                Go to the "Settings" page from the main menu <mui.Icon icon="navigation-menu"/>, then select "View Profile".
                Open the sub menu <mui.Icon icon="navigation-more-vert"/> on the top-right of the screen and choose "Share Profile".
                </div>
            </div>
        )
    },
    renderChangeAvatar: function(){
        return (
            <div>
                <div>
                    Personalize your profile by changing your avatar.
                    You can use your profile image from Facebook, Twitter, Instagram, or Gravatar.
                </div>
                <div>
                    Open the main menu <mui.Icon icon="navigation-menu"/> and select "Settings", then select "Avatar".
                </div>
            </div>
        )
    },
    renderFavoriteQueue: function(){
        return (
            <div>
                <div>If you find a queue and don't want to forget about it, you can "Like" it, or mark as a "Favorite".</div>
                <div>On the queue page, click the <mui.Icon icon="action-favorite-outline"/> button to mark as a favorite, or click the sub-menu  <mui.Icon icon="navigation-more-vert"/> and choose "Like".</div>
            </div>
        )
    },
    renderComplete: function(){
        return (
            <div>
                <div>Click the button below to get rid of this screen:</div>
                <div className="layout vertical center"><mui.FlatButton label="Complete" onClick={this._completeOnboarding}/></div>
            </div>
        )
    }
});

module.exports = Onboard;