var React = require('react'),
    mui = require('material-ui'),
    Router = require('react-router')
    ;
// Flux
var AppStore = require('../../stores/AppStore'),
    AuthStore = require('../../stores/AuthStore'),
    QueueStore = require('../../stores/QueueStore'),
    StoreDataMixin = require('../../mixins/StoreDataMixin'),
    MediaQueryMixin = require('../../mixins/MediaQueryMixin'),
    HomeViewActions = require('../../actions/view/HomeViewActions')
    ;
// components
var PageContainer = require('../common/PageContainer'),
    FavoriteQueues = require('./FavoriteQueues'),
    Onboard = require('./Onboard')
    ;


var AuthenticatedSplash = React.createClass({
    mixins: [StoreDataMixin, MediaQueryMixin, Router.Navigation],
    getInitialState: function(){
        return {
            disableRegister: false
        }
    },
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            requests: {
                favorites: {
                    id: AuthStore.getAuthId(),
                    retriever: QueueStore.getFavorites,
                    action: HomeViewActions.requestFavoriteQueues
                }
            },
            events: {
            }
        }
    },
    statics: {
        getTitle: function(){
            return "Surf";
        }
    },
    _goToSearch: function(){
        this.transitionTo("search")
    },
    render: function(){
        console.log("RENDER AuthenticatedSplash",this.state,this.mediaQuery());
        var background = "http://image.tmdb.org/t/p/original/3JcoCln6tpRkEr5FfTncl0LYtUw.jpg";

        var favesExist = Array.isArray(this.state.favorites) && this.state.favorites.length === 0;

        var rightButton = <mui.IconButton icon="action-search" onClick={this._goToSearch} />;

        return (
            <PageContainer
                ref="container"
                type="normal"
                title="Home"
                center={true}
                background={background}
                rightButton={rightButton}
                colorOverlay="rgba(0,0,0,0.8)"
                contentClass="full"
            >
                {this.renderContent()}

            </PageContainer>
        )
    },
    renderContent: function(){
        var user = AuthStore.getUserObject();
        var showOnboard = (user !== undefined && user.object !== undefined && !user.object.onboarded);

        return (
            <div className="layout vertical layoutContainer">
                { showOnboard ? <Onboard favorites={this.state.favorites} /> : undefined }
                <h4>Favorite Queues</h4>
                <FavoriteQueues queues={this.state.favorites} />
                <h4>Featured Queues</h4>
                <FavoriteQueues queues={this.state.featured} />
            </div>
        )
    },
    renderCopy: function(){
        return (
            <div className="full-width layout vertical center">
                <div className="panel panel-large">
                    <div className="card card__base card__pink">
                    We would love to hear your thoughts about your experience so far.
                    Please send any feedback to <a href="mailto:contact@surfbeta.com">contact@surfbeta.com</a>.
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = AuthenticatedSplash;