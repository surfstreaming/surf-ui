var React = require('react'),
    Navigation = require('react-router').Navigation,
    mui = require('material-ui'),
    ImgSelector = require('../common/ImgSelector'),
    Paper = mui.Paper
    ;


module.exports = React.createClass({
    mixins: [Navigation],
    getInitialState: function(){
        return {
            selected: {
                id: 0,
                overview: "",
                title: ""
            }
        }
    },
    pageChange: function(e){
        if(typeof e[0] !== 'undefined'){
            this.setState({
                selected: e[0].data
            });
        }
    },
    _goToContent: function(){
        console.log(this.state);
        this.transitionTo("movies.detail",{contentid: this.state.selected.id})
    },
    render: function(){

        var content = this.props.featuredContent;
        if(content === undefined || content.length === 0){
            return <span>Loading...</span>
        } else if(!Array.isArray(content)) {
            return <span>ERROR</span>
        } else {
            var images = this.props.featuredContent.map(function(img){
                var data = {
                   id: img.id,
                    title: img.object.title,
                    overview: img.object.overview
                };
                return {type: "tmdb",id: img.id, src: img.object.backdropPath, size: 300, data: data }
            });

            return (
                <div className="layout horizontal center center-justified wrap">
                    <div className="panel panel-medium">
                        <div>
                            <ImgSelector images={images} size={300} columns={1} rows={1} pageChange={this.pageChange} onSelect={this._goToContent} />
                        </div>
                    </div>
                    <div className="panel panel-small">
                        <Paper className="card card-dark">
                            <h3 className="self-center">{this.state.selected.title}</h3>
                            <p>{this.state.selected.overview}</p>
                            <div className="layout horizontal end-justified">
                                <mui.RaisedButton label="Details" onClick={this._goToContent}/>
                            </div>
                        </Paper>
                    </div>

                </div>
            )
        }
    }
});