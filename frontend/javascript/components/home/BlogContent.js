var React = require('react'),
    Navigation = require('react-router').Navigation,
    mui = require('material-ui'),
    ImgSelector = require('../common/ImgSelector'),
    Paper = mui.Paper
    ;


module.exports = React.createClass({
    mixins: [Navigation],
    getInitialState: function(){
        return {
            selected: {
                id: 0,
                overview: "",
                title: ""
            }
        }
    },
    pageChange: function(e){
        if(typeof e[0] !== 'undefined'){
            this.setState({
                selected: e[0].data
            });
        }
    },
    _goToContent: function(){
        console.log(this.state);
        this.transitionTo("movies.detail",{contentid: this.state.selected.id})
    },
    render: function(){

        return (
            <div className="layout horizontal center center-justified wrap">
                <div style={{width: "50%"}}>
                    <h3>Say Hello To Surf</h3>
                    <mui.RaisedButton label="Read"/>
                </div>
                <div style={{width: "50%"}}>
                Surf lets you to create any number of "queues" (lists) to organize the movies and shows you love.
                You can then share those queues with the rest of the world (or just your friends).
                When you would like to watch an item in a queue, we will link to you a streaming service provider that can play it.
                </div>
            </div>
        )
    }
});