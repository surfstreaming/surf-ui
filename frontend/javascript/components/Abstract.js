/** @jsx React.DOM */
var React = require('react');
var Router = require('react-router');

module.exports = React.createClass({
    getInitialState: function() {
        return {  };
    },

    render: function() {
        return ( <Router.RouteHandler  params={this.props.params} query={this.props.query} />);
    }
});


