// Node
var React = require('react'),
    router = require('react-router'),
    mui = require('material-ui'),
    Navigation = require('react-router').Navigation
;
// Flux
var AppStore = require('../../stores/AppStore'),
    StoreDataMixin = require('../../mixins/StoreDataMixin'),
    AuthViewActions = require('../../actions/view/AuthViewActions'),
    AuthStore = require('../../stores/AuthStore')
;
// Components
var Input = mui.Input,
    Toggle = mui.Toggle,
    Paper = mui.Paper,
    Login = require('./Login'),
    PageContainer = require('../common/PageContainer')
;

module.exports = React.createClass({
    mixins: [Navigation,StoreDataMixin, router.State],
    statics: {
        getTitle: function(){
            return "Sign Up";
        }
    },
    getInitialState: function(){
        return {
            token: this.props.query.token,
            loading: true,
            showPassword: false,
            readyToSubmit: false,
            checkingUsername: false
        }
    },
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            requests: {
            },
            events: {
                AUTH_TOKEN_VALIDATE_SUCCESS: this.validateTokenSuccess,
                AUTH_TOKEN_VALIDATE_ERROR: this.validateTokenError,
                AUTH_USERNAME_AVAILABLE_CONFIRM : this.usernameAvailable,
                AUTH_USERNAME_AVAILABLE_REJECT : this.usernameUnavailable,
                AUTH_QUICK_REGISTER_REQUEST : this.registerRequest
            }
        }
    },
    componentDidMount: function(){
        var token = this.props.query.token;
        if(token !== undefined){
            AuthViewActions.validate(token);
        }
    },
    validateTokenSuccess: function(payload){
        console.log("TOKEN SUCCESS",payload.action.response.subject);
        this.setState({email: payload.action.response.subject, loading: false });
    },
    validateTokenError: function(payload){
        console.log("TOKEN ERROR",payload);
        this.setState({
            tokenError: payload.action.response
        })
    },
    usernameAvailable: function(event){
        var uname = event.action.username;
        if(uname === this.state.checkingUsername){
            this.setState({
                checkingUsername: undefined,
                usernameDescription: uname + " is available!",
                usernameError: undefined
            });
        }
    },
    usernameUnavailable: function(event){
        this.setState({
            checkingUsername: undefined,
            usernameDescription: undefined,
            usernameError: "Username "+ event.action.username + " is not available!"
        });
    },
    _toggleShowPassword: function(){
        this.setState({ showPassword: !this.state.showPassword });
    },
    _checkInput: function(){
        var form = this.refs;
        return Object.keys(this.refs).reduce(function(acc,key){
            console.log("CHECK:",form[key].state);
            var isReady = form[key].state.value && !form[key].state.error;
            return acc && isReady;
        },true);
    },
    _checkValidUsername: function(e,username){
        var usernameRegex = /^[A-Za-z0-9]*$/;

        if(username.length < 3){

        }
        else if(usernameRegex.exec(username)){
            this.setState({
                checkingUsername: username,
                usernameDescription: "Checking if " + username + " is available...",
                usernameError: undefined
            });
            AuthViewActions.usernameAvailable(username);
        } else {
            this.setState({usernameError: "Username must contain only letters and numbers"});
        }
    },
    _checkPassword: function(e,password){
        if(password.length < 6){
            this.setState({passwordError: "Passwords must be at least 6 characters!"});
        } else{
            this.setState({passwordError: undefined})
        }
    },
    _confirmPasswords: function(e,confirmPassword){
        if(confirmPassword !== this.refs.password.getValue()){
            this.setState({confirmError:"Passwords do not match!"});
        } else {
            this.setState({confirmError: undefined});
        }
    },
    _signup: function(){
        var ready = this._checkInput();
        if(ready){
            console.log("Sending registration request...");
            var request = {
                username: this.refs.username.getValue(),
                email: this.state.email,
                firstName: this.refs.firstname.getValue(),
                lastName: this.refs.lastname.getValue(),
                password: this.refs.password.getValue(),
                confirm: this.refs.confirm.getValue(),
                token: this.state.token
            };
            AuthViewActions.register(request);
        } else {
            this.setState(
                {bottomMessage: "Looks like you're not quite ready to sign up. Please make sure all fields are filled out and without errors."}
            )
        }

    },
    _goHome: function(){
        this.transitionTo("home");
    },
    render: function(){
        var leftButton = <mui.IconButton icon="navigation-arrow-back" onClick={this.goBack} />;
        return (
            <PageContainer
                type="normal"
                title="Sign Up"
                leftButton={leftButton}
                contentClass="container--main--content__center"
            >
                {this.getQuery().success ? this.renderLogin() : this.renderSignup() }
            </PageContainer>
        )
    },
    renderSignup: function(){
        return (
            <Paper className="panel panel-medium" zDepth={1}>
                <div className="card center layout vertical">
                    <div className="card-header">
                        <h2 className="pink">Join the Party!</h2>
                    </div>
                        { this.renderMessage()}
                        { this.state.tokenError ? this.renderTokenError() : undefined }
                        { this.state.token === undefined ? this.renderNoToken() : undefined }
                        { this.state.token && !this.state.tokenError ? this.renderForm() : undefined }
                    <div className="card-footer layout vertical center">
                        <span>{this.state.bottomMessage}</span>
                    </div>
                </div>
            </Paper>
        )
    },
    renderLogin: function(){
        return (
            <Paper className="panel panel-small" zDepth={1}>
                <div className="card center layout vertical">
                    <div className="card-header">
                        <h2 className="pink">You're In!</h2>
                    </div>
                    <div>Sign in below to get started:</div>
                    <Login />
                </div>
            </Paper>
        );
    },
    renderMessage: function(){
        if(this.getQuery().signup){
            return <div style={{padding: 15}}>
                <Login />
            </div>
        }
        else if(this.state.token === undefined) return undefined;

        return this.state.loading ?
            <strong>Validating your registration token, please wait...</strong>
            : <strong>Completing registration for { this.state.email}</strong>
    },
    renderNoToken: function(){
        return (
            <div style={{padding: 15}}>
                Oops, it looks like there was no registration token passed in from the URL.
                If you got here from the email we sent you then this is likely an error.
                Please send an email to <strong>contact@surfbeta.com</strong> or try registering your email again from the <a href="/">home page</a>.
            </div>
        )
    },
    renderTokenError: function(){
        var error = this.state.tokenError.body;
        if(error === null || error === undefined){
            error = "No Message"
        }
        return (
            <div style={{padding: 15}}>
            Oops, it looks like there was an error validating your registration token.
            The error we received was:
            <blockquote>
                {error}
            </blockquote>
            Try registering your email again from the <a href="/">home page</a>.
            </div>
        )
    },
    renderForm: function(){
        var toggleSpanStyle = {
            marginRight: "15px"
        };

        return <div className="card-body layout vertical center">
            <div className="layout horizontal width-100">
                <Input ref="username"
                    type="text"
                    name="username"
                    placeholder="Username"
                    onChange={this._checkValidUsername}
                    description={this.state.usernameDescription}
                    className="flex"
                    error={this.state.usernameError}
                    disabled={this.state.loading}
                />
            </div>
            <Input ref="firstname"
                type="text"
                name="firstname"
                placeholder="First Name"
                disabled={this.state.loading}
            />
            <Input ref="lastname"
                type="text"
                name="lastname"
                placeholder="Last Name"
                disabled={this.state.loading}
            />
            <Input ref="password"
                type={this.state.showPassword ? "text" : "password" }
                name="password"
                placeholder="Password"
                onChange={this._checkPassword}
                disabled={this.state.loading}
                error={this.state.passwordError}
            />
            <Input ref="confirm"
                type={this.state.showPassword ? "text" : "password" }
                name="username"
                placeholder="Confirm Password"
                onChange={this._confirmPasswords}
                disabled={this.state.loading}
                error={this.state.confirmError}
            />

            <div className="full-width layout horizontal center">
                <span style={toggleSpanStyle}>Show Password:</span>
                <Toggle onToggle={this._toggleShowPassword} />
                <span className="flex"></span>
                <mui.FloatingActionButton className="self-end" type="FAB" icon="navigation-arrow-forward" primary={true} onClick={this._signup} disabled={this.state.loading} />
            </div>
        </div>
    }
});