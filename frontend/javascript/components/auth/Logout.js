var React = require('react'),
    router = require('react-router'),
    AuthViewActions = require('../../actions/view/AuthViewActions'),
    AppStore = require('../../stores/AppStore')
;

var Logout = React.createClass({
    mixins: [router.Navigation],
    statics: {
        getTitle: function(){
            return "Logging out...";
        }
    },
    componentWillMount: function() {
        console.log("START LOG OUT");
        AuthViewActions.logout();
    },
    render: function(){
        return (
            <div className="center panel panel-medium">
                <h2>Logging Out</h2>
            </div>
        );
    }
});

module.exports = Logout;
