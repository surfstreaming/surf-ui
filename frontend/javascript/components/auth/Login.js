var React = require('react'),
    router = require('react-router'),
    mui = require("material-ui")
;
// Flux
var AuthViewActions = require('../../actions/view/AuthViewActions'),
    AppStore = require('../../stores/AppStore'),
    StoreDataMixin = require('../../mixins/StoreDataMixin')
;
// Components
var Link = require("react-router").Link,
    Paper = mui.Paper,
    Input = mui.Input,
    Toggle = mui.Toggle,
    PageContainer = require('../common/PageContainer')
;


var Login = React.createClass({
    mixins: [StoreDataMixin, router.Navigation, router.State],
    getDataNeeded: function(){
        return {
            listeners: [AppStore],
            requests: {},
            events: {
                LOGIN_REQUEST: this._loginRequest,
                LOGIN_ERROR: this._loginError,
                LOGIN_SUCCESS : this._loginSuccess
            }
        }
    },
    statics: {
        getTitle: function(){
            return "Log In";
        }
    },
    getInitialState: function(){
      return {
          showPassword: false,
          readyToSubmit: false
      }
    },
    _loginRequest: function(payload){
        console.log("Login request",payload);

    },
    _loginSuccess: function(payload){
        console.log("Login success",payload);

    },
    _loginError: function(payload){
        console.log("Login error",payload);
        if(payload.action.error.body.code === 401){
            this.setState({ error: "Incorrect username or password"});
        } else {
            this.setState({ error: "Error logging in"});
        }


    },
    _toggleShowPassword: function(){
      this.setState({ showPassword: !this.state.showPassword });
    },
    _goToSignup: function(){
        this.transitionTo("/signup");
    },
    _checkInput: function(){
        var res =  this.refs.username.getValue() !== undefined && this.refs.password.getValue() !== undefined;
        if(res !== this.state.readyToSubmit){
            this.setState({ readyToSubmit: res });
        }
    },
    _login: function(){
        console.log("Logging in: ",this.refs.username.getValue());
        AuthViewActions.login({
            username:  this.refs.username.getValue(),
            password: this.refs.password.getValue()
        });
    },
    _goHome: function(){
        this.transitionTo("home")
    },
    render: function(){

        var welcomeText;
        if(this.getQuery().signup){
            welcomeText = "You're In! Sign in below to get started:"
        }

        return (
                    <div className="card layout vertical center center-justified" >
                        <p>{welcomeText}</p>
                        <div className="card-body layout vertical center">
                            <Input ref="username"
                                type="text"
                                name="username"
                                placeholder="Username or Email"
                                onChange={this._checkInput}
                            />
                            <Input ref="password"
                                type={this.state.showPassword ? "text" : "password" }
                                name="password"
                                placeholder="Password"
                                onChange={this._checkInput}
                            />
                            <div>
                                <span className="errorMessage">{this.state.error}</span>
                            </div>
                            <div className="full-width layout horizontal center">
                                <span style={{marginRight: 15}}>Show Password:</span>
                                <Toggle onToggle={this._toggleShowPassword} />
                                <span className="flex"></span>
                                <mui.FloatingActionButton className="self-end" type="FAB" icon="navigation-arrow-forward" primary={true} onClick={this._login} />
                            </div>
                        </div>
                    </div>

        );
    }
});


module.exports = Login;