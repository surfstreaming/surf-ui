var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
    ;
// Flux
var AuthStore = require('../../stores/AuthStore'),
    UserStore = require('../../stores/UserStore'),
    FriendStore = require('../../stores/FriendStore'),
    StoreUtils = require('../../utils/StoreUtils'),
    UserViewActions = require('../../actions/view/UserViewActions'),
    StoreDataMixin = require('../../mixins/StoreDataMixin')
;
// Components
var Avatar = require('../common/Avatar'),
    PageContainer = require('../common/PageContainer'),
    LeftNav = require('../nav/LeftNav')
;

module.exports = React.createClass({
    mixins: [StoreDataMixin],
    getDataNeeded: function(){
        return {
            listeners: [FriendStore],
            transform: this._transform,
            requests: {
                pending: {
                    retriever: FriendStore.getPendingRequests
                },
                requests: {
                    retriever: FriendStore.getFriendRequests
                },
                friends: {
                    retriever: FriendStore.getFriends
                }
            }
        }
    },
    statics: {
        getTitle: function(){
            return "Friends";
        }
    },
    _transform: function(change){
        console.log("FRIEND STORE CHANGE",change);
        return change;
    },
    getInitialState: function(){
        return {}
    },
    _toggleLeftNav: function(){
        this.refs.container.toggleLeftNav();
    },
    _toggleRightNav: function(){
        this.refs.container.toggleRightNav();
    },
    _renderLoading: function(){
        return <span>Loading...</span>;
    },
    _renderFriends: function(){
        console.log("RENDER FRIENDS",this.state);
        return (
            <div className="layout horizontal center-justified wrap">
                <div className="leftContent">
                    <div>{this._renderFriendsRequesting()}</div>
                    <div>{this._renderPendingFriends()}</div>
                </div>
                <div className="rightContent">
                    <div>{this._renderExistingFriends()}</div>
                </div>
            </div>
        )
    },
    _renderExistingFriends: function(){
        if(Array.isArray(this.state.friends) && this.state.friends.length > 0){
            var friends = this.state.friends.map(function(x){
                var user = UserStore.getUser(x);
                return (
                    <div key={x} className="layout horizontal">
                        <Avatar user={user} type="badge"/>
                        <span className="flex"></span>
                    </div>
                )
            });

            return (
                <div>
                    <h5>Your Friends:</h5>
                    {friends}
                </div>
            )
        } else {
            return (
                <div>
                    <h5>Looks like you don't have any friends!</h5>
                </div>
            )
        }
    },
    _renderFriendsRequesting: function(){
        if(Array.isArray(this.state.requests) && this.state.requests.length > 0){
            var requests = this.state.requests.map(function(x){
                var user = UserStore.getUser(x.connection);
                var userId = AuthStore.getAuthId();
                var friendship = x.link.id;
                return (
                    <div key={x.link.id} className="layout horizontal">
                        <Avatar user={user} type="badge"/>
                        <span className="flex"></span>
                        <mui.FlatButton className="button-reject" label="Reject" onClick={UserViewActions.rejectFriendRequest.bind(this,userId,x.connection,friendship)}/>
                        <mui.FlatButton className="button-confirm" label="Confirm" onClick={UserViewActions.confirmFriendAction.bind(this,userId,x.connection,friendship)}/>
                    </div>
                )
            });

            return (
                <div>
                    <h5>Friend requests:</h5>
                    {requests}
                </div>
            )
        }

    },
    _renderPendingFriends: function(){
        if(Array.isArray(this.state.pending) && this.state.pending.length > 0){
            var requests = this.state.pending.map(function(x){
                var user = UserStore.getUser(x.connection);
                var userId = AuthStore.getAuthId();
                var friendship = x.link.id;
                return <div key={x.link.id} className="layout horizontal">
                    <Avatar user={user} type="badge"/>
                    <span className="flex"></span>
                    <mui.FlatButton className="button-reject" label="Cancel" onClick={UserViewActions.cancelFriendRequest.bind(this,userId,friendship)}/>
                </div>
            });

            return (
                <div>
                    <div>Requests waiting confirmation:</div>
                    <div className="layout vertical"> {requests} </div>
                </div>
            )
        }

    },
    render: function(){
        console.log("(Friends) rendering friends",this.state);
        var content;
        if(this.state.friends === undefined){
            content = this._renderLoading();
        } else {
            content = this._renderFriends();
        }

        var leftNav = <LeftNav />;
        var leftButton = <mui.IconButton className="PageContainer-icon" icon="navigation-menu" onClick={this._toggleLeftNav} />;
        var rightButton;

        return (
            <PageContainer
                ref="container"
                title="Friends"
                type="normal"
                leftNav={leftNav}
                leftButton={leftButton}
                rightButton={rightButton}
            >
                {content}
            </PageContainer>
        )

    }
});