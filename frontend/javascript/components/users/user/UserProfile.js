// Node
var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;
// Flux
var AppStore = require('../../../stores/AppStore'),
    AuthStore = require('../../../stores/AuthStore'),
    UserStore = require('../../../stores/UserStore'),
    QueueStore = require('../../../stores/QueueStore'),
    UserViewActions = require('../../../actions/view/UserViewActions'),
    StoreUtils = require('../../../utils/StoreUtils')
;
// Components
var Collapsible = require('../../common/Collapsible'),
    QueueCardList = require('../../common/QueueCardList'),
    Avatar = require('../../common/Avatar'),
    PageContainer = require('../../common/PageContainer'),
    ShareLink = require('../../common/social/ShareLink')
;


module.exports = React.createClass({
    mixins: [Router.State, Router.Navigation],
    statics: {
        getTitle: function(data){
            return "User Profile";
        }
    },
    _renderQueues: function(){
        var user = this.props.user;
        if(Array.isArray(this.props.user.queues)){
            var queues = this.props.user.queues.map(function(x){
                return {
                    queue: QueueStore.getQueue(x),
                    user: user
                };
            });
            return <QueueCardList
                queues={queues}
                renderIfEmpty={<span>No Queues Available.</span>}
                showAvatar={false}
            />
        }

    },
    _renderFriends: function(){
        var friends = this.props.user.friends;
        var count = Array.isArray(friends) ? friends.length : 0;
        var content = <div>
            <div>{this.props.user.object.displayName} has no friends!</div>
        </div>;
        if(count > 0){
            content = friends.map(function(friend){
               return <Avatar key={friend} user={UserStore.getUser(friend)} type="badge"/>
            });
        }

        return <Collapsible title={"Friends ("+count+")"} show={true}>{content}</Collapsible>
    },
    _renderHeaderButtons: function(){
        var relationship = this.props.user.relationship;
        var userId = AuthStore.getAuthId();
        var friendId = this.props.user.id;
        var friendshipId = relationship ? relationship.id : undefined;

        if(relationship !== undefined && relationship.status === "Current User"){
            return (
                <div className="layout vertical center">
                    <mui.FlatButton className="button-confirm" label="Settings" onClick={this._goToSettings}/>
                    <mui.FlatButton className="button-confirm" label="Friends" onClick={this._goToFriends}/>
                </div>
            )
        } else if(relationship !== undefined && relationship.status === "No Relationship"){
            return (
                <div className="layout vertical center">
                    <mui.FlatButton className="button-confirm" label="Add Friend" onClick={this._addFriendship.bind(this,userId,friendId)}/>
                </div>
            )
        } else if(relationship !== undefined && relationship.status === "Pending Request"){
            return (
                <div className="layout vertical center">
                    <span>Pending Request</span>
                    <mui.FlatButton className="button-confirm" label="Cancel Request" onClick={this._cancelRequest.bind(this,userId,friendshipId)}/>
                </div>
            )
        } else if(relationship !== undefined && relationship.status === "Friends"){
            return (
                <div className="layout vertical center">
                    <mui.FlatButton className="button-confirm" label="Unfriend" onClick={this._unfriend.bind(this,userId,friendId,friendshipId)}/>
                </div>
            )
        }
    },
    _renderRightNav: function(){
        var filterMenuItems = [
            { payload: '1', text: 'Share Profile'}
        ];

        if(this.props.user !== undefined && this.props.user.object !== undefined){
            //var url = this.makeHref("users.user.profile",{username: this.props.user.object.username});
            var url = document.location.href;
            console.log("url: ",url);
            //var url = "http://surfbeta.com"
            var menu = <mui.Menu menuItems={filterMenuItems} />;

            return (
                <div style={{marginTop: 90}}>
                    <div style={{marginLeft: 10}}>
                        <h4>Share Your Profile:</h4>
                        <div style={{marginLeft: 20}}>
                            <div>
                                <ShareLink type="twitter" text="Check out my profile on Surf" url={url} label="Share on Twitter"/>
                            </div>
                            <div>
                                <ShareLink type="facebook" url={url} label="Share on Twitter"/>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return <span>Loading...</span>;
        }

    },
    _goToSettings: function(){
        this.transitionTo("settings")
    },
    _goToFriends: function(){
        this.transitionTo("friends")
    },
    _toggleRightNav: function(){
        this.refs.container.toggleRightNav();
    },
    _addFriendship: function(userId,friendId){
        UserViewActions.addFriendAction(userId,friendId);
    },
    _cancelRequest: function(userId,friendshipId){
        UserViewActions.cancelFriendRequest(userId,friendshipId);
    },
    _unfriend: function(userId,friendId,friendshipId){
        UserViewActions.removeFriendAction(userId,friendId,friendshipId);
    },
    _confirmFriendship: function(userId,friendshipId){
        UserViewActions.confirmFriendAction(userId,friendshipId);
    },
    render: function(){
        var user = this.props.user;
        console.log("RENDER PROFILE",user);
        var rightButton = <mui.IconButton icon="navigation-more-vert" onClick={this._toggleRightNav} />;
        var rightNav = this._renderRightNav();
        if(user === undefined || user === "loading" || user.loading){
            return <span>Loading...</span>;
        }
        else {
            var avatar = <Avatar user={user} size={115}/>;
            return (
                <PageContainer
                    ref="container"
                    type="normal"
                    header={avatar}
                    title={user.object.displayName}
                    rightButton={rightButton}
                    rightNav={rightNav}
                >
                    <div className="layoutContainer layout horizontal wrap">
                        <div className="panel panel-small">
                             {this._renderHeaderButtons()}
                        </div>
                        <div className="layout vertical panel panel-large">
                        { this._renderQueues()}
                        { this._renderFriends()}
                        </div>
                    </div>

                </PageContainer>
            )
        }
    }
});