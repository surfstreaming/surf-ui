var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;
// API
var UserAPI = require('../../../utils/api/UserAPI');

// Flux
var StoreUtils = require('../../../utils/StoreUtils'),
    AppStore = require('../../../stores/AppStore'),
    UserStore = require('../../../stores/UserStore'),
    UserViewActions = require('../../../actions/view/UserViewActions')
    ;

module.exports = React.createClass({
    mixins: [ Router.Navigation, Router.State ],
    statics: {
        resolve: {
            user: function(params,query,data){
                return UserAPI.getUserByUsername(params.username,["relationship"]);
            }
        }
    },
    componentDidMount: function() {
        AppStore.addChangeListener(this._appStateChange);
    },
    componentWillUnmount: function(){
        AppStore.removeChangeListener(this._appStateChange);
    },
    componentWillReceiveProps: function(props){
        console.log("(UserAbstract) RECEIVE PROPS",props);
        this.requestIfNeeded(props.params.username);
        this.setState({
            user: UserStore.getUserByUsername(props.params.username)
        });
    },
    _appStateChange: function(payload){
        console.log("(UserAbstract) CHANGE",payload.action.type);
        var username = this.getParams().username;
        var user = UserStore.getUserByUsername(username);
        this.setState({user : user});
    },
    genericRequestIfNeeded: function(id,retriever,fieldsFunc,action){
        var object = retriever.call(this,id);
        var fields = fieldsFunc.call(this);
        if(! StoreUtils.hasFields(object,fields)){
            action.call(this,id,fields);
        }
    },
    requestIfNeeded: function(username){
        var id = username;
        var retriever = UserStore.getUserByUsername;
        var fieldsFunc = this.getPartsNeeded;
        var action = UserViewActions.requestUserByUsername;

        this.genericRequestIfNeeded(id,retriever,fieldsFunc,action);
    },
    getPartsNeeded: function(){
        if(this.isActive("users.user.profile")){
            return ["relationship","friends","queues"];
        } else {
            return ["relationship","queues"];
        }
    },
    getInitialState: function() {
        this.requestIfNeeded(this.getParams().username);
        return { user: UserStore.getUserByUsername(this.getParams().username)  };
    },
    shouldComponentUpdate: function(props,nextState){
       // return nextState.user !== this.state.user;
        return true;
    },
    render: function(){
        return (
            <Router.RouteHandler params={this.props.params} query={this.props.query} user={this.state.user} />
        )
    }
});


