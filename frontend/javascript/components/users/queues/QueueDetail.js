// Node
var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;
// Flux
var AppStore = require('../../../stores/AppStore'),
    AuthStore = require('../../../stores/AuthStore'),
    QueueStore = require('../../../stores/QueueStore'),
    UserStore = require('../../../stores/UserStore'),
    ContentStore = require('../../../stores/ContentStore'),
    QueueViewActions = require('../../../actions/view/QueueViewActions')
;
// components
var PageContainer = require('../../common/PageContainer'),
    TmdbImg = require('../../common/TmdbImg'),
    Avatar = require('../../common/Avatar'),
    Poster = require('../../common/Poster')
;

module.exports = React.createClass({
    mixins: [ Router.Navigation, Router.State ],
    statics: {
        getTitle: function(data){
            return "Queue Detail";
        }
    },
    _toggleFavorite: function(){
       if(this.props.queue.favorite){
           QueueViewActions.removeFromFavorites(AuthStore.getAuthId(),this.props.queue.id,this.props.queue.favorite);
       } else {
           QueueViewActions.addToFavorites(AuthStore.getAuthId(),this.props.queue.id);
       }
    },
    _toggleRightNav: function(){
        this.refs.container.toggleRightNav();
    },
    _goToEdit: function(){
      this.transitionTo("users.user.queues.queue.edit",this.getParams())
    },
    render: function() {
        console.log("RENDER QUEUE DETAIL",this.props.queue);
        if(this.props.queue === undefined || this.props.queue === "unset" ){
            return this._getLoadingQueue();
        } else {
            return this._getQueue()
        }
    },

    _getLoadingQueue: function(){
        return (
            <span>Loading...</span>
        )
    },
    _getQueue: function(){
        var queue = this.props.queue;
        var auth = AuthStore.getAuth();
        var fab;
        if(auth === undefined){
            fab = undefined;
        } else if(queue.favorite){
            fab = <mui.FloatingActionButton icon="action-favorite" onClick={this._toggleFavorite} />;
        } else {
            fab = <mui.FloatingActionButton icon="action-favorite-outline" onClick={this._toggleFavorite} />;
        }

        var rightButton =  <mui.IconButton icon="navigation-more-vert" onClick={this._toggleRightNav} />;
        var rightNav = this.renderRightNav();
        var header = (
            <div className="layout vertical center center-justified">
            </div>
        );

        var type = queue.object.splash === undefined ? "normal" : "splash";

        return (
            <PageContainer
                ref="container"
                type={type}
                title={queue.object.name}
                background={TmdbImg.getURL(queue.object.splash)}
                header={header}
                fab={fab}
                rightButton={rightButton}
                rightNav={rightNav}
            >
                {this.renderContent()}
            </PageContainer>
        )
    },
    renderRightNav: function(){

        function handleClick(x,y,item){
            item.action();
        }
        var items =  [
            { payload: '1', text: 'Edit', icon: 'editor-mode-edit', action: this._goToEdit}
        ];
        return (
            <div style={{marginTop: 90}}>
                <mui.Menu menuItems={items} onItemClick={handleClick}/>
            </div>
        )
    },
    renderContent: function(){
        var auth = AuthStore.getAuth();
        var queue = this.props.queue;
        var content;
        if(auth.id === queue.user && Array.isArray(queue.items) && queue.items.length === 0){
            content = this.renderQueueBootstrap();
        }
        else if(Array.isArray(queue.items)){
            var items = queue.items.map(function(contentItem){
                var content = ContentStore.getItem(contentItem.content);

                return <Poster key={contentItem.item.id} contentid={content.id} src={content.object.posterPath} />
            });

            content = <div className="layout horizontal wrap">{items}</div>
        } else {
            content = <span>Loading content...</span>
        }

        var message;
        if(auth.id === queue.user && Array.isArray(queue.items) && queue.items.length > 0 && queue.object.splash === undefined ){
            message = (
                <div className="layout vertical center">
                    <div className="panel panel-large">
                        <mui.Paper>
                            <div className="card card__pink card--body">
                                <div>Now that there are items in this queue, you can change the splash image to customize the look of your queue.</div>
                                <div>From the sub menu  <mui.Icon icon="navigation-more-vert" />, choose "Edit" and select a new splash image.</div>
                            </div>
                        </mui.Paper>
                    </div>
                </div>
            )
        }

        return (
            <div className="full-width layout horizontal wrap">

                {this.renderQueueTop()}
                <div className="layout vertical center">
                    {message}
                    <div className="panel panel-large">
                    { content }
                    </div>
                </div>
            </div>
        )
    },
    renderQueueTop: function(){

       return (
           <div className="panel panel-small">
               <mui.Paper className="card card__rounded card__shadow card__biggertext">
                   <div className="card--body">
                       <div className="layout horizontal">
                           <Avatar user={UserStore.getUser(this.props.queue.user)} type="badge"/>
                           <span className="flex"/>
                           <div> {this.renderAccess()}</div>
                       </div>
                       <div> {this.props.queue.object.description}  </div>
                   </div>
               </mui.Paper>
           </div>
       )
    },
    renderQueueBootstrap: function(){
        return (
            <div className="full-width layout vertical center">
                <div className="panel panel-medium">
                    <div className="card card__rounded card__shadow card__biggertext">
                        <div className="card--title"></div>
                        <div className="card--body ">
                            <div>Looks like there is nothing in this queue yet... let's fix that!</div>
                            <br/>
                            <div>
                            From the main menu <mui.Icon icon="navigation-menu" />, choose Search.
                            Click the item you want to add to your queue to view the content details.
                            From the details page, click "Add to Queues".
                            </div>
                            <br/>
                            <div>
                                After you have added items to your queue, you can change the splash image from the Settings page of the queue.
                                From the sub menu <mui.Icon icon="navigation-more-vert" />, choose "Edit" and select a new splash image.
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        )
    },
    renderAccess: function(){
        return <div className="layout horizontal center">
            <span className="flex"></span>
            <div className="layout vertical center">
                <mui.Icon icon="action-lock"/>
                <span>{this.props.queue.object.access}</span>
            </div>
        </div>
    }
});


