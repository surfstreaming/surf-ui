var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;

// Flux
var StoreUtils = require('../../../utils/StoreUtils'),
    AppStore = require('../../../stores/AppStore'),
    QueueStore = require('../../../stores/QueueStore'),
    UserStore = require('../../../stores/UserStore'),
    QueueViewActions = require('../../../actions/view/QueueViewActions'),
    StoreDataMixin = require('../../../mixins/StoreDataMixin'),
    ActionTypes = require('../../../constants/ActionTypes')
    ;
// API
var QueueAPI = require('../../../utils/api/QueueAPI');
// Components
var TmdbImg = require('../../common/TmdbImg')
;

module.exports = React.createClass({
    mixins: [ Router.Navigation, Router.State, StoreDataMixin ],
    statics: {
        resolve: {
            queue: function(params,query,data, token){
                return data.user.then(function(user){
                    return QueueAPI.getQueue(user.result,params.queueid,["user","items","favorite","favorite_count","likes"], token);
                })
            }
        },
        meta: function(path,params,query,data){
            console.log("Getting meta");

            if(typeof data !== 'undefined' && typeof data.queue !== 'undefined'){
                var queue = data.queue.entities.queues[data.queue.result];
                return [
                    {property: "og:site_name", content: "Surf"},
                    {property: "og:type", content: "article"},
                    {property: "og:url", content: "http://surfbeta.com"+path},
                    {property: "og:title", content: queue.object.name },
                    {property: "og:description", content: queue.object.description },
                    {property: "og:image", content: TmdbImg.getURL(queue.object.splash,780)},
                    {property: "og:image:type", content: "image/jpeg"},
                    {property: "og:image:width", content: 780},
                    {property: "og:image:height", content: 439},

                    // Twitter
                    {property: "twitter:card", content: "summary_large_image"}
                ];
            } else {
                return {};
            }
        }
    },
    getDataNeeded: function(props){
        return {
            listeners: [AppStore],
            requests: {
                queue: {
                    async: true,
                    retriever: QueueStore.getQueue,
                    retrieverParameters: [props.params.queueid],
                    fields: this.getPartsNeeded(),
                    action: QueueViewActions.requestQueue,
                    actionParameters: [props.user.id,props.params.queueid,this.getPartsNeeded()],
                    actionPrecheck: function(){
                        return props.user.id !== undefined && props.params.queueid !== undefined;
                    }
                }
            },
            events: {}
        }
    },
    getPartsNeeded: function(){
        if(this.isActive("users.user.queues.queue.detail")){
            return ["user","items","favorite","favorite_count","likes"];
        } else if(this.isActive("users.user.queues.queue.edit")){
            return ["user","favorite","favorite_count","splash_images"];
        } else {
            return ["user"];
        }
    },
    render: function(){
        return (
            <Router.RouteHandler  params={this.props.params} query={this.props.query} queue={this.state.queue} />
        )
    }
});


