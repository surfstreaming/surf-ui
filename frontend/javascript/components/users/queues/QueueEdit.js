var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;
// Flux
var AppStore = require('../../../stores/AppStore'),
    AuthStore = require('../../../stores/AuthStore'),
    QueueStore = require('../../../stores/QueueStore'),
    UserStore = require('../../../stores/UserStore'),
    ContentStore = require('../../../stores/ContentStore'),
    QueueViewActions = require('../../../actions/view/QueueViewActions')
;
//Components
var Avatar = require('../../common/Avatar'),
    Collapsible = require('../../common/Collapsible'),
    PageContainer = require('../../common/PageContainer'),
    ImgSelector = require('../../common/ImgSelector'),
    TmdbImg = require('../../common/TmdbImg')
;
module.exports = React.createClass({
    mixins: [ Router.Navigation, Router.State ],
    statics: {
        getTitle: function(){
            return "Edit Queue";
        }
    },
    getInitialState: function(){
        return {queue: this.props.queue, confirmDelete: false };
    },
    componentWillReceiveProps: function(props){
        console.log("NEW PROPS",props,this.props);
        this.setState({ queue: props.queue  });

    },
    render: function() {
        console.log("RENDER QUEUE EDIT",this.props);
        if(this.props.queue === undefined || this.props.queue.object === undefined){
            return this._getLoadingQueue();
        } else {
            return this._getQueue()
        }
    },
    _save: function(){
        var queue = React.addons.update(this.state.queue,{
           object: {
               name: {$set: this.refs.queueName.getValue()},
               description: {$set: this.refs.description.getDOMNode().value}
           }
        });
        console.log("SAVE",queue);
        QueueViewActions.updateQueue(this.props.params.username,queue.user,queue);
    },
    _getLoadingQueue: function(){
        return (
            <span>Loading...</span>
        )
    },
    _accessClicked: function(e,index,item){
        this.setState({
           queue: React.addons.update(this.state.queue,{
                object: {
                    access: {$set: item.payload }
                }
           })
        });
    },
    _accessToIdx: function(access){
        if(access === "private") return 0;
        else if(access === "friends") return 1;
        else if(access === "public") return 2;
    },
    _splashSelected: function(e){
        console.log("SELECTED",e);
        this.setState({
            queue: React.addons.update(this.state.queue,{
                object: {
                    splash: {$set: e.src }
                }
            })
        });
    },
    _resetDelete: function(){
        this.setState({deletePreConfirm: false});
    },
    _deleteQueue: function(){
        if(this.state.deletePreConfirm){
            QueueViewActions.deleteQueue(AuthStore.getAuthId(),this.state.queue.id);
        } else {
            this.setState({deletePreConfirm: true})
        }
    },
    _renderSplashImages: function(){
        var splashImages = [];
        if(Array.isArray(this.state.queue.splash_images)){
            splashImages = this.state.queue.splash_images.map(function(img){
                return {id: img.id, type: "tmdb", src: img.object.filePath, size: 184}
            });
        } else {
            return <span>Loading...</span>
        }

        return (
            <Collapsible title="Splash Image" show={true}>
                {Array.isArray(splashImages) ?
                    <ImgSelector images={splashImages} rows={3} columns={2} onSelect={this._splashSelected} /> : <span>Loading...</span>
                }
            </Collapsible>
        )
    },
    _renderDelete: function(){
        var preConfirmDelete = <div id="preConfirmDelete">
            <div>
            Are you sure you want to delete this queue&#63; This cannot be undone!
            </div>
            <div className="layout horizontal end-justified">
                <mui.FlatButton onClick={this._resetDelete} className="button-transparent" label="Cancel"/>
                <mui.FlatButton onClick={this._deleteQueue} className="button-alert" label="Delete"/>
            </div>
        </div>;

        var confirmDelete = <div>
            <div>
            Ok - last chance! Confirm again to delete this queue.
            </div>
            <div className="layout horizontal end-justified">
                <mui.FlatButton onClick={this._resetDelete} className="button-transparent" label="Don't Delete"/>
                <mui.FlatButton onClick={this._deleteQueue} className="button-alert" label="Yes, Delete"/>
            </div>
        </div>;

        return (
            <Collapsible title="Delete Queue" show={this.state.deletePreConfirm}>
                {this.state.deletePreConfirm ? confirmDelete : preConfirmDelete}
            </Collapsible>
        )
    },
    _renderBasics: function(){
        var accessItems = [
            { payload: 'private', text: 'Private',data: "Only you can access", icon: "action-lock" },
            { payload: 'friends', text: 'Friends',data: "Only you and your friends", icon: "action-lock-outline" },
            { payload: 'public', text: 'Public',data: "Anyone can access", icon: "action-lock-open" }
        ];
        var accessIdx = this._accessToIdx(this.state.queue.object.access);

        return <mui.Paper className="panel panel-medium card">
            <div className="panel">
                <h5 className="pink">What is the name of the queue&#63;</h5>
                <mui.Input ref="queueName" defaultValue={this.state.queue.object.name} type="text" name="queueName"/>
            </div>
            <div className="panel">
                <h5 className="pink">Who can access the queue&#63;</h5>
                <mui.Menu autoWidth={false} zDepth={0} selectedIndex={accessIdx} menuItems={accessItems} onItemClick={this._accessClicked} />

            </div>
            <div className="panel">
                <h5 className="pink">Queue description</h5>
                <textarea ref="description" name="Text1" cols="40" rows="5" defaultValue={this.state.queue.object.description}></textarea>
            </div>
        </mui.Paper>
    },
    _getQueue: function(){
        var queue = this.state.queue;

        var fab = <mui.FloatingActionButton icon="content-save" onClick={this._save} />;
        var leftButton = <mui.IconButton icon="navigation-arrow-back" onClick={this.goBack} />;
        var rightButton;

        return (
            <PageContainer
                title={"Editing: "+queue.object.name}
                type="normal"
                leftButton={leftButton}
                rightButton={rightButton}
                fab={fab}
            >
                <div className="layout horizontal wrap center-justified layoutContainer">
                    {this._renderBasics()}

                    <div className="panel panel-medium">
                        {this._renderSplashImages()}
                        {this._renderDelete()}
                    </div>
                </div>


            </PageContainer>
        )
    }
});


