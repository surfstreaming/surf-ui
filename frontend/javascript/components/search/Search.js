var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;
// Flux
var SearchViewActions = require('../../actions/view/SearchViewActions'),
    SearchStore = require('../../stores/SearchStore')
;
// Components
var
    Poster = require('../common/Poster'),
    PageContainer = require('../common/PageContainer'),
    LeftNav = require('../nav/LeftNav')
;

module.exports = React.createClass({
    mixins: [ Router.State, Router.Navigation ],
    statics: {
        getTitle: function(){
            return "Search";
        }
    },
    componentDidMount: function() {
        SearchStore.addChangeListener(this._searchStoreChange);
    },
    componentWillUnmount: function(){
        SearchStore.removeChangeListener(this._searchStoreChange);
    },
    _searchStoreChange: function(){
        var results = SearchStore.getResults();
        this.setState({ results: results })
    },
    getInitialState: function() {
        var lastSearch = SearchStore.getSearch();
        console.log("LAST SEARCH WAS: ",lastSearch);

        var defaultName = {title: undefined};

        return React.addons.update(lastSearch,{
            name: {$set: lastSearch.name || defaultName },
            results: {$set: SearchStore.getResults()}
        });
    },

    _getResults: function(){
        var results = this.state.results.map(this._renderResult);
        return <div className="layout horizontal wrap">{results}</div>;
    },
    _renderResult: function(result){
        return <div className="poster">
            <Poster key={result.fields.vertexId[0]} src={result.fields.poster_path} contentid={result.fields.vertexId[0]} size={92} />
        </div>;
    },
    _getLoading: function(){
        return <span>Loading...</span>
    },
    _getWaiting: function(){
        return <span>Enter search query</span>
    },
    _doSearch: function(){
        var pageSize = 30;
        var fromResult = 0;
        SearchViewActions.search({
            name: {
                title: this.refs.title.getValue(),
            },
            pagination: {
                pageSize: pageSize,
                from: fromResult
            }
        });
    },
    _toggleLeftNav: function(){
        this.refs.container.toggleLeftNav();
    },
    _toggleRightNav: function(){
        this.refs.container.toggleRightNav();
    },
    render: function() {
        var results;
        if(this.state.loading) {
            results = this._getLoading();
        } else if (Array.isArray(this.state.results)){
            results = this._getResults();
        } else {
            results = this._getWaiting();
        }

        var leftNav = <LeftNav />;
        var rightNav = this.renderRightNav();
        var leftButton = <mui.IconButton className="PageContainer-icon" icon="navigation-menu" onClick={this._toggleLeftNav} />;
        var rightButton = <mui.IconButton className="PageContainer-icon" icon="action-settings" onClick={this._toggleRightNav} />;
        return (
            <PageContainer
                ref="container"
                title="Search"
                type="normal"
                defaultBackground="/images/material3.jpg"
                leftNav={leftNav}
                leftButton={leftButton}
                rightNav={rightNav}
                rightButton={rightButton}
                contentClass="container--main--content__center"
            >
                <div className="panel panel-small">
                    <div className="layout horizontal">
                        <div>
                            <mui.Input ref="title" type="text" name="title" placeholder="Enter Search Term" description="Enter Search" defaultValue={this.state.name.title}/>
                        </div>
                    </div>
                    <div className="layout vertical">
                        <mui.FlatButton className="self-end button-confirm" label="Search" onClick={this._doSearch}/>
                    </div>
                </div>

                <div className="panel panel-large">
                    {results}
                </div>
            </PageContainer>
        );
    },
    renderRightNav: function(){
        var filterMenuItems = [
            { payload: '1', text: 'Text Opt-In', toggle: true},
            { payload: '2', text: 'Text Opt-Out', toggle: true},
            { payload: '3', text: 'Voice Opt-Out', toggle: true}
        ];

        return (
            <div style={{marginTop: 90}}>
                <mui.Menu menuItems={filterMenuItems} />
            </div>
        )
    }
});


