// node modules
var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;
// Flux
var  AppStore = require('../../../stores/AppStore'),
    AuthStore = require('../../../stores/AuthStore'),
    ContentStore = require('../../../stores/ContentStore'),
    ContentViewActions = require('../../../actions/view/ContentViewActions'),
    QueueStore = require('../../../stores/QueueStore'),
    QueueViewActions = require('../../../actions/view/QueueViewActions'),
    StoreUtils = require('../../../utils/StoreUtils'),
    UserStore = require('../../../stores/UserStore')
;
// Components
var PageContainer = require('../../common/PageContainer'),
    Poster = require('../../common/Poster'),
    QueueCardList = require('../../common/QueueCardList'),
    TmdbImg = require('../../common/TmdbImg'),
    Collapsible = require('../../common/Collapsible'),
    ManageQueuesDialog = require('../../queues/ManageQueuesDialog')
;

module.exports = React.createClass({
    mixins: [ Router.Navigation, Router.State ],
    statics: {
        getTitle: function(){
            return "Movies";
        }
    },
    _componentsNeeded: function(){
        console.log("Components needed:",AuthStore.getAuthId(),AuthStore.getAuthId() !== undefined);
        return AuthStore.getAuthId() !== undefined ?
            ["genres","keywords","cast","crew","public_queues","private_queues","friend_queues"]
            : ["genres","keywords","cast","crew","public_queues"];
    },
    _requestIfNeeded: function(contentId){
        var components = this._componentsNeeded();
        if(!StoreUtils.isInBag(ContentStore.getAllContent(),contentId,components)){
            console.log("REQUESTING CONTENT",contentId,components);
            ContentViewActions.requestContent(contentId,components);
            return true;
        } else {
            return false;
        }
    },
    componentDidMount: function() {
        AppStore.addChangeListener(this._storeChange);
        var contentId = this.getParams().contentid;
        if(!this._requestIfNeeded(contentId)){
            this.setState({content : ContentStore.getItem(contentId)});
        }
    },
    componentWillUnmount: function(){
        AppStore.removeChangeListener(this._storeChange);
    },
    componentWillReceiveProps: function(props){
        console.log("(Movie) new props",props);
        //this._requestIfNeeded();
    },
    _storeChange: function(){
        var id = this.getParams().contentid;
        console.log("(Movie) state change",id);
        this.setState({content : ContentStore.getItem(id)});
    },
    getInitialState: function() {
        var contentId = this.getParams().contentid;
        return { content : ContentStore.getItem(contentId)  };
    },
    _renderPrivateQueues: function(){
        var user = AuthStore.getUserObject();
        // Don't render if user is not available yet
        if( user === undefined || user.object === undefined){
            console.log("user not available yet - don't render private queues");
            return <span></span>
        }

        var queues = this.state.content.private_queues.map(function(queueAndItem){
            var qid = queueAndItem.queue;
            var queue = QueueStore.getQueue(qid);

            return { queue: queue, user: user,  pending: queueAndItem.item._pending };
        });

        return <QueueCardList
            queues={queues}
            renderIfEmpty={<span>This does not appear in any of your queues.</span>}
            label="In Your Queues "
            showAvatar={false}
        />
    },
    _renderFriendQueues: function(){
        console.log("FRIEND QUEUES:",this.state.content.friend_queues);

        var queues = this.state.content.friend_queues.map(function(qid){
            var queue = QueueStore.getQueue(qid);
            var user = UserStore.getUser(queue.user);

            return { queue: queue, user: user };
        });

        return <QueueCardList
            queues={queues}
            renderIfEmpty={<span>This does not appear in any of your friend's queues.</span>}
            label="In Your Friend's Queues "
        />
    },
    _renderPublicQueues: function(){
        console.log("PUBLIC QUEUES:",this.state.content.public_queues);
        return <QueueCardList
            queues={this.state.content.public_queues}
            renderIfEmpty={<span>This does not appear in any public queues.</span>}
            label={"Public Queues "}
        />
    },
    _renderCast: function(){
        console.log("START CAST",this.state.content);
        var count = 0;
        var content = <span>No cast available</span>;
        if(Array.isArray(this.state.content.cast)){
            count = this.state.content.cast.length;
            var castMarkup = this.state.content.cast.map(function(cast){
                return (
                    <div key={cast.id} className="layout vertical center">
                        <Poster src={cast.object.profilePath} noLink={true}>
                            <div className="layout vertical">
                                <strong>{cast.object.character}</strong>
                                <small>{cast.object.name}</small>
                            </div>
                        </Poster>
                    </div>
                )
            });

            content = <div className="layout horizontal wrap">{castMarkup}</div>
        }
        console.log("DONE CAST");

        return (
            <Collapsible title={"Cast ("+count+")"}>
                {content}
            </Collapsible>
        )
    },
    _manageQueues: function(){
        this.refs.manageQueuesDialog.show();
    },
    _toggleRightNav: function(){
        this.refs.container.toggleRightNav();
    },
    render: function(){
        console.log("RENDER CONTENT",this.state.content);
        if(this.state.content === undefined || this.state.content.object === undefined) {
            return this._renderLoading();
        } else {
            return this._renderContent();
        }

    },
    _renderLoading: function(){
        return (
            <span>Loading...</span>
        )
    },
    _renderContent: function(){
        var content = this.state.content;
        var factMenu = [
            { key: 1, text: 'Released: ', number: content.object.releaseDate.substring(0,4) },
            { key: 2, text: 'Runtime: ', number: content.object.runtime + "m" }
        ];

        var facts = factMenu.map(function(fact){
            return (
                <div key={fact.key} className="layout horizontal">
                    <strong>{fact.text}</strong>
                    <span className="flex"></span>
                    <span className="self-end">{fact.number}</span>
                </div>
            )
        });

        var manageQueues;
        console.log("AUTH IS:",AuthStore.getAuthId());
        if(AuthStore.getAuthId() !== undefined){
            var dialogActions = [
                { text: 'CANCEL' },
                { text: 'SUBMIT', onClick: this._onDialogSubmit }
            ];
            var buttonLabel ;
            if(Array.isArray(this.state.content.private_queues)){
                buttonLabel = this.state.content.private_queues.length > 0 ? "Add/Remove Queues" : "Add To Queues";
                manageQueues = <div>
                    <mui.FlatButton className="button-confirm" label={buttonLabel} onClick={this._toggleRightNav}/>
                </div>;

            }


        }

        var details;
        if(!content.loading && content.cast !== undefined){
            details = (
                <div>
                { AuthStore.getUserObject() ? this._renderPrivateQueues() : undefined }
                { AuthStore.getUserObject() ? this._renderFriendQueues() : undefined }
                { this._renderPublicQueues()  }
                { this._renderCast() }
                </div>
            )
        } else {
            details = <span>Loading...</span>
        }

        var rightNav, rightButton;
        if(AuthStore.getUserObject() !== undefined){
            rightButton = <mui.IconButton icon="navigation-more-vert" onClick={this._toggleRightNav}/>;
            rightNav = this.renderRightNav();
        }

        return (
            <PageContainer
                ref="container"
                type="splash"
                title={content.object.title}
                background={TmdbImg.getURL(content.object.backdropPath)}
                defaultBackground="/images/material3.jpg"
                rightButton={rightButton}
                rightNav={rightNav}
            >
                <div className="Movie-poster-container">
                    <div className="Movie-poster">
                        <div className="Movie-poster-img">
                            <Poster size="large" src={content.object.posterPath} noLink />
                        </div>
                        {manageQueues}
                    </div>
                    <div className="panel panel-large">
                        <p className="Movie-description">{content.object.overview}</p>
                    </div>
                </div>
                <div className="panel panel-large">
                    <div>
                        <div>
                            <div className="layout horizontal">
                                <div>
                                {facts}
                                </div>
                            </div>
                        </div>
                    </div>

                {details}
                </div>
            </PageContainer>
        )

    },
    renderRightNav: function(){
        if(Array.isArray(this.state.content.private_queues)){
            return (
                <div style={{marginTop: 45}}>
                    <ManageQueuesDialog
                        content={this.state.content}
                        inQueues={this.state.content.private_queues}
                        onClose={this._toggleRightNav}
                    />
                </div>
            )
        } else {
            return <span>Loading...</span>;
        }

    }
});