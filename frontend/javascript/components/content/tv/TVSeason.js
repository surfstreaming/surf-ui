// node modules
var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
    ;
// Flux
var AppStore = require('../../../stores/AppStore'),
    ContentStore = require('../../../stores/ContentStore'),
    StoreDataMixin = require('../../../mixins/StoreDataMixin'),
    ContentAPI = require('../../../utils/api/ContentAPI'),
    ContentViewActions = require('../../../actions/view/ContentViewActions')
    ;
// Components
var PageContainer = require('../../common/PageContainer'),
    TmdbImg = require('../../common/TmdbImg'),
    ErrorPage = require('../../common/ErrorPage')
    ;

var TVSeason = React.createClass({
    mixins: [ StoreDataMixin, Router.Navigation, Router.State ],
    statics: {
        getTitle: function(){
            return "TV Series";
        },
        resolve: {
            series: function(params,query,data, token){
                var components = [];
                console.log("DO IT NOW");
                return ContentAPI.getTVSeason(params.seriesid, params.seasonid, components, token);
            }
        },
        meta: function(path,params,query,data){
            console.log("Getting meta",data);

            if(typeof data !== 'undefined' && typeof data.series !== 'undefined'){
                console.log("OVER HERE",data);
                var series = data.season.entities.season[data.season.result];
                return [
                    {property: "og:site_name", content: "Surf"},
                    {property: "og:type", content: "video.tv_show"},
                    {property: "og:url", content: "http://surfbeta.com"+path},
                    {property: "og:title", content: series.object.name },
                    {property: "og:description", content: series.object.description },
                    {property: "og:image", content: TmdbImg.getURL(series.object.backdrop_path,780)},
                    {property: "og:image:type", content: "image/jpeg"},
                    {property: "og:image:width", content: 780},
                    {property: "og:image:height", content: 439},

                    // Twitter
                    {property: "twitter:card", content: "summary_large_image"}
                ];
            } else {
                return {};
            }
        }
    },
    getDataNeeded: function(props){
        return {
            listeners: [AppStore],
            requests: {
                series: {
                    retriever: ContentStore.getSeason,
                    retrieverParameters: [props.params.seasonid],
                    action: ContentViewActions.requestSeason,
                    actionParameters: [props.params.seriesid, props.params.seasonid]
                }
            },
            events: {
                TVSEASON_ERROR : this.apiError
            }
        }
    },
    apiError: function(payload){
        this.setState({
            isError: true,
            error: payload.action.error
        });
    },
    render: function(){
        if(this.state.isError){
            return <ErrorPage error={this.state.error} />
        }
        return (
            <div>
                <h1>sup</h1>
            </div>
        )

    }
});

module.exports = TVSeason;