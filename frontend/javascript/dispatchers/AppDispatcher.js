require('es6-shim');

var Dispatcher = require('flux').Dispatcher,
    PayloadSources = require('../constants/PayloadSources');

var AppDispatcher = Object.assign(new Dispatcher(), {
    handleServerAction: function(action) {
        //console.log('server action', action);

        if (!action.type) {
            throw new Error('Empty action.type: you likely mistyped the action.');
        }

        this.dispatch({
            source: PayloadSources.SERVER_ACTION,
            action: action
        });
    },

    handleViewAction: function(action) {
        //console.log('view action', action);

        if (!action.type) {
            throw new Error('Empty action.type: you likely mistyped the action.');
        }

        console.log("(Dispatcher) BEGIN");
        this.dispatch({
            source: PayloadSources.VIEW_ACTION,
            action: action
        });
        console.log("(Dispatcher) COMPLETE");
        //console.log('complete view action', action);
    }
});

module.exports = AppDispatcher;