var React = require('react'),
    Router = require('react-router'),
    routes = require('./routes'),
    RouterStore = require('./stores/RouterStore'),
    AuthViewActions = require('./actions/view/AuthViewActions');

var injectTapEventPlugin = require("react-tap-event-plugin");

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

AuthViewActions.getInitialAuth();
/*
var AppRouter = React.renderComponent(routes, document.getElementById('surf'));
RouterStore.set(AppRouter);
*/
var el = document.getElementById('surf');
var router = Router.create({
    routes: routes,
    location: Router.HistoryLocation,
    onError: function(err){
        console.error("got an error",err);
    }
});
router.run(function (Handler,state) {
    RouterStore.set(router);
    console.log("RUN:",state);
    var title = state.routes.filter(function (match) {
        // gather up the handlers that have a static `getTitle` method
        return match.handler.getTitle;
    }).reduce(function (previous, match) {
        return match.handler.getTitle(previous);
    },"");

    React.render(<Handler params={state.params} query={state.query} title={title}/>, el);
});