
var _ = require('lodash'),
    merge = require('react/lib/merge'),
    EventEmitter = require('events').EventEmitter,
    shallowEqual = require('react/lib/shallowEqual'),
    CHANGE_EVENT = 'change';

var StoreUtils = {
    createStore: function(spec) {
        var store = merge(EventEmitter.prototype, merge(spec, {
            emitChange: function(context) {
                this.emit(CHANGE_EVENT,context);
            },

            addChangeListener: function(callback) {
                this.on(CHANGE_EVENT, callback);
            },

            removeChangeListener: function(callback) {
                this.removeListener(CHANGE_EVENT, callback);
            }
        }));

        _.each(store, function (val, key) {
            if (_.isFunction(val)) {
                store[key] = store[key].bind(store);
            }
        });

        store.setMaxListeners(0);
        return store;
    },
    process: function(storeName,handlers){
        function handleIf(source,action,payload,f){
            if(payload.source === source && payload.action.type === action){
                console.log("("+ storeName +") Handling " + source + ":"+action,payload.action);
                return f(payload.action);
            } else {
                //console.log("(AppStore) Ignoring " + source + ":"+action);
                return false;
            }
        }

        return handlers.reduce(function(acc,cur){
            var res = handleIf(cur.source,cur.action,cur.payload,cur.handler);
            return acc === true || typeof res === 'undefined' ? true : res;
        },false);
    },

    isInBag: function(bag, id, fields) {
        var item = bag[id];
        if (!bag[id]) {
            return false;
        }

        if (fields) {
            return fields.every(function(field){ return item.hasOwnProperty(field) });
        } else {
            return true;
        }
    },
    hasFields: function(item,fields){
        if(item === undefined) return false;

        return fields.every(function(field){ return item.hasOwnProperty(field) });
    },

    mergeIntoBag: function(bag, entities, transform) {
        if (!transform) {
            transform = function(x){return x };
        }

        for (var key in entities) {
            if (!entities.hasOwnProperty(key)) {
                continue;
            }

            if (!bag.hasOwnProperty(key)) {
                bag[key] = transform(entities[key]);
            } else if (!shallowEqual(bag[key], entities[key])) {
                bag[key] = transform(Object.assign(bag[key], entities[key]));
            }
        }
    },
    requestIfNeeded: function(request){
        var fields = request.fields || [];
        var object = request.retriever.call(this,request.id);
        return this._requestIfNeeded(request.id,object, fields,request.action,request.async);
    },
    _requestIfNeeded: function(id,object,fields,action,async){
        if(! this.hasFields(object,fields) && (object === undefined || object.loading !== true)){
            if(async){
                setTimeout(action.bind(this,id,fields),0);
            } else {
                action.call(this,id,fields);
            }

        }
    },
    loadEntities: function(action,bag,entity,storeName){
    if(typeof action.normalized !== 'undefined' && typeof action.normalized.entities[entity] !== 'undefined'){
        console.log("("+storeName+") Loading Entities",action.type,action.normalized.entities);
        StoreUtils.mergeIntoBag(bag,action.normalized.entities[entity],function(q){q.loading = false; return q;});
        return true;
    } else {
        return false;
    }
}
};

module.exports = StoreUtils;