var normalizr = require('normalizr'),
    arrayOf = normalizr.arrayOf
;


var users = new normalizr.Schema('users');
var content = new normalizr.Schema('content');
var queues = new normalizr.Schema('queues');

users.define({
    friends: arrayOf(users),
    queues: arrayOf(queues),
    pending_friends: arrayOf({
        connection: users
    }),
    friends_requesting: arrayOf({
        connection: users
    })
});

content.define({
    private_queues: arrayOf({
        queue: queues
    }),
    friend_queues: arrayOf(queues)
});


queues.define({
    user: users,
    first_ten: arrayOf({
        content: content
    }),
    items: arrayOf({
        content: content
    })
});


module.exports = {
    users: users,
    queues: queues,
    content: content
};