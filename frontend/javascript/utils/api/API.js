var superagent = require('superagent'),
    normalizr = require('normalizr'),
    normalize = normalizr.normalize
    ;

var API_ROOT = 'http://api.surfbeta.com';

var API = {
  token: undefined,
  promise: function(req){
      var handleSuccess = this.handleSuccess,
          handleError = this.handleError;
      return new Promise(function(resolve,reject){
          req.end(function(err,res){
              if(typeof err === 'undefined' || err === null){
                  handleSuccess(res,resolve,reject);
              } else {
                  handleError(err,resolve,reject);
              }
          });
      });
  },
  handleSuccess: function(res,resolve,reject){
      if(res.status == 200){
          resolve(res.body);
      } else {
          reject({
              status: res.status,
              body: res.body
          });
      }
  },
  handleError: function(err,resolve,reject){
      console.log("ERR:",err);
      reject(err);
  },
  componentStr: function(arr,defaults){

      if(arr instanceof Array && arr.length > 0){
          return "?components="+arr.join(",");
      } else if (defaults instanceof Array && defaults.length > 0) {
          return "?components="+defaults.join(",");
      } else {
          return "";
      }
  },
  base: function(request){
      request.data = request.data === undefined ? {} : request.data;
      if(request.url === undefined){
          request.url = API_ROOT + request.resource + this.componentStr(request.components,request.defaultComponents);
      }
      var req = superagent(request.method,request.url).accept('application/json');
      console.log("Full url: ",request.url);

      var token = request.token || this.token;
      if(typeof token !== 'undefined'){
          req = req.set('Authorization',token)
      }
      if(request.data !== undefined){
          req.send(request.data);
      }

      return this.promise(req).then(function(results){
          if(request.flatten){
              if(Array.isArray(results)){
                  results.forEach(function(x){
                      Object.keys(x.components).forEach(function(k){
                          x[k] = x.components[k];
                      });
                      delete x.components;
                  })
              } else {
                  Object.keys(results.components).forEach(function(k){
                      results[k] = results.components[k];
                  });
                  delete results.components;
              }

          }
          if(request.transform !== undefined){
              results = request.transform(results);
          }
          if(request.schema !== undefined){
              return normalize(results,request.schema);
          } else {
              return results;
          }
      });

  },
  get: function(request){
      console.log("GET: ",request);
      request.method = "GET";
      return this.base(request);
  },
  post: function(request){
      request.method = "POST";
      return this.base(request);
  },
  del: function(request){
      request.method = "DELETE";
      return this.base(request);
  },
  put: function(request){
      request.method = "PUT";
      return this.base(request);
  }
};

module.exports = API;