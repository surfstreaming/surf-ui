var API = require('./API'),
    QueueServerActions = require('../../actions/server/QueueServerActions'),
    normalizr = require('normalizr'),
    normalize = normalizr.normalize,
    arrayOf = normalizr.arrayOf,
    Schema = require('./Schema'),
    AuthStore = require('../../stores/AuthStore'),
    RouterStore = require('../../stores/RouterStore')
;

module.exports = {
    getFavorites: function(userId,components){
        console.log("GET FAVES");

        return API.get({
                resource: "/users/"+ ( userId || 0 ) +"/queues/favorites",
                components: components,
                flatten: true,
                defaultComponents: ['first_ten','user'],
                schema: arrayOf(Schema.queues)
            });
    },
    getFeatured: function(components){
        return API.get({
            resource: "/featured/queues",
            flatten: true,
            components: components,
            defaultComponents: ['first_ten','user'],
            schema: arrayOf(Schema.queues)
        });
    },
    getQueues: function(userId, components){
        return API.get({
            resource: "/users/" + userId + "/queues",
            components: components,
            flatten: true,
            defaultComponents: ['first_ten','user'],
            schema: arrayOf(Schema.queues)
        });
    },
    getQueue: function(userId, queueId, components, token){
        return API.get({
            token: token,
            resource: "/users/"+ userId +"/queues/"+queueId,
            flatten: true,
            components: components,
            defaultComponents: ['first_ten','user'],
            schema: Schema.queues
        })
    },
    createQueue: function(queue){
        var auth = AuthStore.getAuth();
        return API.post({
            resource: "/users/"+ auth.id +"/queues",
            data: queue.object,
            schema: Schema.queues
        }).then(QueueServerActions.receiveNewQueue.bind(this,auth.id));
    },
    updateQueue: function(userId,queue){
        return API.put({
            resource: "/users/"+ userId +"/queues/"+queue.id,
            data: queue.object,
            transform: function(result){
                result.user = { id: userId };
                return result;
            }
        });
    },
    deleteQueue: function(userId,queueId){
        return API.del({
            resource: "/users/"+ userId +"/queues/"+queueId,
            data: {}
        }).then(function(){
            var merge = {
                id: queueId,
                deleted: "deleted"
            };
            var normalized = normalize(merge,Schema.queues);
            QueueServerActions.receiveDeleteQueue(userId,normalized);
            RouterStore.get().transitionTo("queues");
        });
    },
    addFavorite: function(userId, queueId){
        return API.post({
            resource: "/users/"+ userId +"/queues/favorites",
            data: { queueId:  queueId }
        }).then(QueueServerActions.receiveFavoriteAdd.bind(this,userId,queueId))
    },
    deleteFavorite: function(userId, queueId, favoriteId){
        return API.del({
            resource: "/users/"+ userId +"/queues/favorites/"+favoriteId,
            data: {  }
        }).then(QueueServerActions.receiveFavoriteDelete.bind(this,userId,queueId))
    },
    addQueueItem: function(userId,queueId,contentId,notes){
        console.log("addQueueItem");
        var data = {
            item: { notes: notes === undefined ? "" : notes },  contentId : contentId
        };
        return API.post({
            resource: "/users/"+ userId +"/queues/"+queueId + "/items",
            data: data
        });
    },
    removeQueueItem: function(userId,queueId,itemId){
        console.log("addQueueItem");
        return API.del({
            resource: "/users/"+ userId +"/queues/"+ queueId + "/items/"+itemId
        })
    },
    manageQueueItems: function(requests){
        Promise.all(requests.map(function(request){
            if(request.addRemove === "add"){
                return this.addQueueItem(request.userId,request.queueId,request.contentId);
            } else {
                return this.removeQueueItem(request.userId,request.queueId,request.itemId,request.notes);
            }
        },this)).then(function(results){
            console.log("EVERYTHING RESOLVED",results);
            QueueServerActions.manageQueueItemResults(requests,results);
        });
    }
};