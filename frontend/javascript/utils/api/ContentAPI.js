var API = require('./API'),
    ContentServerActions = require('../../actions/server/ContentServerActions'),
    AuthStore = require('../../stores/AuthStore'),
    arrayOf = require('normalizr').arrayOf,
    Schema = require('./Schema')
;

module.exports = {
    getFeaturedContent: function(){
        return API.get({
            resource: "/featured/content",
            schema: arrayOf(Schema.content),
            transform: function(results){
                return results.map(function(x){ return x.connection});
            }
        });
    },
    getContent: function(contentId,components){
        var defaultComponents = ["genres","keywords","cast","crew","public_queues"];
        return API.get({
            resource: "/movies/"+contentId,
            flatten: true,
            components: components,
            defaultComponents: defaultComponents,
            schema: Schema.content,
            transform: function(results){
                // TODO alter API to fit the schema
                if(Array.isArray(results.private_queues)) {
                    var authId = AuthStore.getState().auth.id;
                    var authUsername = AuthStore.getState().auth.username;
                    results.private_queues = results.private_queues.map(function (q) {
                        var queue = q.queue;
                        queue.user = { id: authId, username: authUsername };
                        return { item: q.item, queue: queue };
                    });
                }
                if(Array.isArray(results.friend_queues)) {
                    results.friend_queues = results.friend_queues.map(function(queueAndUser){
                        var rtn = queueAndUser.queue;
                        rtn.user = queueAndUser.user;
                        console.log(rtn);
                        return rtn;
                    });
                }
                return results;
            }
        }).then(ContentServerActions.receiveContent);
    },
    getTVSeries: function(seriesId, components,token){
        var defaultComponents = [];
        return API.get({
            token: token, // used for server side rendering
            resource: "/content/tv/"+seriesId,
            flatten: true,
            components: components,
            defaultComponents: defaultComponents,
            schema: Schema.content
        }).then(ContentServerActions.tvSeriesResponse,ContentServerActions.tvSeriesError);
    },
    getTVSeason: function(seriesId,seasonId, components,token){
        var defaultComponents = [];
        return API.get({
            token: token, // used for server side rendering
            resource: "/content/tv/"+seriesId+"/season/"+seasonId,
            flatten: true,
            components: components,
            defaultComponents: defaultComponents,
            schema: Schema.content
        }).then(ContentServerActions.tvSeasonResponse,ContentServerActions.tvSeasonError);
    },
    getTVEpisode: function(seriesId,seasonId,episodeId, components,token){
        var defaultComponents = [];
        return API.get({
            token: token, // used for server side rendering
            resource: "/content/tv/"+seriesId+"/season/"+seasonId+"/episode/"+episodeId,
            flatten: true,
            components: components,
            defaultComponents: defaultComponents,
            schema: Schema.content
        }).then(ContentServerActions.tvEpisodeResponse,ContentServerActions.tvEpisodeError);
    }
};