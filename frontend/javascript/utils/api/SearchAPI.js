var API = require('./API'),
    ActionTypes = require('../../constants/ActionTypes'),
    SearchServerActions = require('../../actions/server/SearchServerActions')
;

module.exports = {
    search: function(request){
        return API.post({
            resource: "/search/query",
            data: request
        }).then(SearchServerActions.searchResponse)
    }
};