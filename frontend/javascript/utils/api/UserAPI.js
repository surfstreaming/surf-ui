var API = require('./API'),
    ActionTypes = require('../../constants/ActionTypes'),
    Schema = require('./Schema')
;

module.exports = {
    getUserByUsername: function(username,components){
        return API.get({
            resource:"/users/username/"+username,
            components: components,
            flatten: true,
            schema: Schema.users,
            defaultComponents: ["relationship"]
        })
    },
    getUser: function(userId,components){
        return API.get({
            resource:"/users/"+userId,
            components: components,
            flatten: true,
            schema: Schema.users
        })
    },
    getCurrentUser: function(components){
        return API.get({
            resource: "/auth/current",
            schema: Schema.users,
            components: components,
            flatten: true,
            defaultComponents: ["friends","queues","pending_friends","friends_requesting"]
        });
    },
    addFriend: function(userId,friendId){
        return API.post({
            resource: "/users/" + userId + "/friends",
            data: {
                friendUserId: friendId
            }
        })

    },
    removeFriend: function(userId, friendshipId){
        return API.del({
            resource: "/users/" + userId + "/friends/" + friendshipId,
            data: {}
        })
    },
    confirmFriend: function(userId, friendshipID){
        return API.put({
            resource: "/users/" + userId + "/friends/" + friendshipID,
            data: {}
        });
    },
    update: function(userId,user){
        return API.put({
            resource: "/users/" + userId,
            data: user,
            schema: Schema.users
        });
    },
    changePassword: function(current,password,confirm){
        return API.post({
            resource: "/auth/changePassword",
            data: {
                current: current,
                password: password,
                confirm: confirm
            }
        })
    }
};