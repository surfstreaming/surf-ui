var API = require('./API'),
    AuthServerActions = require('../../actions/server/AuthServerActions'),
    React = require('react'),
    RouterStore = require('../../stores/RouterStore')
;

module.exports = {
    getInitialAuth: function(){
        var auth = "anonymous";
        if (typeof localStorage === 'undefined') {
            // get the 'token' url query parameter
            auth = undefined;
        } else {
            var authStr = localStorage["surf-auth"];
            try {
                if(authStr !== undefined){
                    auth = JSON.parse(authStr);
                }
            } catch(err) {
                console.log("AUTH PARSE ERROR",err,authStr);
                AuthServerActions.invalidToken(err,authStr);
            }
        }
        AuthServerActions.receiveInitialAuth(auth);
    },
    login: function(request){
        return API.post({
            resource: "/auth/login",
            data: request
        });
    },
    logout: function(){
        var success = function(response){
            AuthServerActions.logoutSuccess(response);
            RouterStore.get().transitionTo("/",undefined)
        };

        return API.post({
            resource: "/auth/logout",
            data: {token: API.token, refreshToken: "x"}
        }).then(success, AuthServerActions.logoutError);
    },
    usernameAvailable: function(username){
        return API.get({
            resource: "/auth/usernameAvailable/"+username
        }).then(AuthServerActions.usernameAvailable.bind(this,username),AuthServerActions.usernameUnavailable.bind(this,username))
    },
    emailAvailable: function(email){
        return API.get({
            resource: "/auth/emailAvailable/"+email
        }).then(AuthServerActions.emailAvailable.bind(this,email),AuthServerActions.emailUnavailable.bind(this,email))
    },
    quickRegister: function(email){
        return API.post({
            resource: "/auth/register",
            data: { email: email }
        }).then(AuthServerActions.quickRegisterSuccess.bind(this,email),AuthServerActions.quickRegisterError.bind(this,email))
    },
    signup: function(request) {
        var defaults = {
            displayNameType: "firstL",
            avatarType: "defaults",
            avatarId: "/images/avatars/default1.png",
            displayName: request.firstName + " " + request.lastName.substring(0, 1) + ".",
            access: "NormalUser",
            onboarded: false
        };

        var fullRequest = React.addons.update(defaults, {$merge: request});
        return API.post({
            resource: "/auth/signup",
            data: fullRequest
        }).then(
            // SUCCESS
            function(response){
                AuthServerActions.registerSuccess(fullRequest,response);
                RouterStore.get().transitionTo('signup',null,{success: true});
            }
            // ERROR
            ,AuthServerActions.registerError.bind(this,fullRequest)
        )

    },
    validate: function(token){
        return API.post({
            resource: "/auth/validate",
            data: { token: token }
        }).then(AuthServerActions.validateSuccess.bind(this,token),AuthServerActions.validateError.bind(this,token))
    }

};