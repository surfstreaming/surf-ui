var StoreUtils = require('./StoreUtils')
;

module.exports = {
    genericRequestIfNeeded: function(id,retriever,fieldsFunc,action,async){
        var object = retriever.call(this,id);
        var fields = fieldsFunc.call(this);
        if(! StoreUtils.hasFields(object,fields) && (object === undefined || object.loading !== true)){
            if(async){
                setTimeout(action.bind(this,id,fields),0);
            } else {
                action.call(this,id,fields);
            }

        }
    }
}