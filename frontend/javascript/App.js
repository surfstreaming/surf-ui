var React = require('react'),
    Router = require('react-router'),
    mui = require('material-ui')
;
// Flux
var AuthStore = require('./stores/AuthStore'),
    UserViewActions = require('./actions/view/UserViewActions')
;
// Components
var Icon = mui.Icon,
    AppCanvas = mui.AppCanvas,
    AppBar = mui.AppBar
;

module.exports = React.createClass({
    mixins: [ Router.State, Router.Navigation ],
    getInitialState: function(){
        return {
            hideMessage: false
        }
    },
    componentDidMount: function() {
        AuthStore.addChangeListener(this._authChange);
        this._authChange();
    },
    componentWillUnmount: function() {
        AuthStore.addChangeListener(this._authChange);
    },
    _authChange: function(payload){
        console.log("(App) AUTH CHANGE",payload,AuthStore.getAuthId());
        var id = AuthStore.getAuthId();

        if(id !== undefined && AuthStore.getUserObject() === undefined){
            console.log("REQUEST CURRENT USER");
            setTimeout(UserViewActions.requestCurrentUser,0);
        }
    },
    _toggleMessage: function(){
        this.setState({ hideMessage: !this.state.hideMessage });
    },
    render: function() {

        return (
            <AppCanvas predefinedLayout={1}>
                <Router.RouteHandler params={this.props.params} query={this.props.query} />
            </AppCanvas>
        );

    }
});


