
var MediaQueryMixin = {
    _mediaQueries: {
        tabletsAndDesktop: "(min-width: 768px)",
        phonesLandscape: "(max-width: 767px)",
        phones: "(max-width: 767px) and (orientation: portrait)"
    },
    componentWillMount: function() {
        if(window === undefined){
            // could be rendering on server
            this.setState({media: "server"});
        } else {
            this._getMediaQuery();

        }

    },
    componentWillUnmount: function() {
        var q = this.state.mql;
        if (q !== undefined) q.removeListener(this._getMediaQuery);
    },
    _getMediaQuery: function(){
        var res = this.mediaQuery();
        if(res !== undefined){
            if(this.state.mql !== undefined){
                this.state.mql.removeListener(this._getMediaQuery);
            }
            res.query.addListener(this._getMediaQuery);
            this.setState({media: res.media, mql: res.query});
        }
    },
    mediaQuery: function() {
        var queries = this._mediaQueries;
        return Object.keys(this._mediaQueries).reduce(function (acc, key) {
            if (acc !== undefined) return acc;

            var query = window.matchMedia(queries[key]);
            if (query.matches) {
                return {
                    media: key,
                    query: query
                };
            }
        },undefined);
    }
};

module.exports = MediaQueryMixin;