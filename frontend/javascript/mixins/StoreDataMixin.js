var StoreUtils = require('../utils/StoreUtils')
;


function _getDataFromStores(data){

    var change = {};
    if(data.requests !== undefined) {
        Object.keys(data.requests).forEach(function (key) {
            var request = data.requests[key];
            var retrieverParams = request.retrieverParameters || [request.id] || [];
            change[key] = request.retriever.apply(this, retrieverParams);
        });
    }

    if(data.transform){
        change = data.transform.call(this,change);
        if(change === undefined) console.warn("Change is undefined - are you sure a value is being returned?");
    }

    return change;
}

var StoreDataMixin = {
    getInitialState: function(){
        var requests = this.getDataNeeded(this.props);
        return _getDataFromStores(requests);
    },
    componentWillMount: function() {
        var data = this.getDataNeeded(this.props);
        var listener = this._updateState;
        data.listeners.forEach(function(store){
            store.addChangeListener(listener)
        });

        if(data.requests !== undefined){
            Object.keys(data.requests).forEach(function(key){
                if(data.requests[key].action){
                    this.requestIfNeeded(data.requests[key]);
                }
            },this);
        }
    },
    componentWillUnmount: function(){
        var data = this.getDataNeeded(this.props);
        var listener = this._updateState;
        data.listeners.forEach(function(store){
            store.removeChangeListener(listener)
        });
    },
    componentWillReceiveProps: function(props){
        var data = this.getDataNeeded(props);
        if(data.requests !== undefined){
            Object.keys(data.requests).forEach(function(key){
                if(data.requests[key].action){
                    this.requestIfNeeded(data.requests[key]);
                }
            },this);
        }
    },
    _updateState: function(event){
        var data = this.getDataNeeded(this.props);

        if(data.events !== undefined){
            Object.keys(data.events).forEach(function(key){
                // Check if we should respond to this event
                if(event.action.type === key){
                    data.events[key](event);
                }
            });
        }

        if(data.requests !== undefined){
            var newData = _getDataFromStores(data);
            this.setState(newData);
        }

    },
    requestIfNeeded: function(request){
        var fields = request.fields || [];

        var retrieverParams = request.retrieverParameters || [request.id] || [];
        var object = request.retriever.apply(this,retrieverParams);

        var preCheckFunc = request.actionPrecheck || function(){return true};
        var preCheck = preCheckFunc();

        if(preCheck && ! StoreUtils.hasFields(object,fields) && (object === undefined || object.loading !== true)){
            var actionParameters = request.actionParameters || [request.id,request.fields];
            if(request.async){
                setTimeout(function(){
                    request.action.apply(this,actionParameters)
                },0);
            } else {
                request.action.apply(this,actionParameters);
            }
        }
    }

};

module.exports = StoreDataMixin;