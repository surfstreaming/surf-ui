
var gulp = require('gulp');
var merge = require('merge-stream');
var gutil = require('gulp-util');
var less = require('gulp-less');
var react = require('gulp-react');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
var minifycss = require('gulp-minify-css');
var nodemon = require('gulp-nodemon');
var mocha = require('gulp-mocha');


gulp.task('clean', function() {
    return gulp.src(['build/*'], {read: false}).pipe(clean());
});

// Parse and compress JS and JSX files

gulp.task('javascript', function() {
    return gulp.src('frontend/javascript/**/*.js')
        .pipe(react())
        .pipe(gulp.dest('build/javascript/'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('build/javascript/'));
});

gulp.task('javascript_nomin', function() {
    return gulp.src('frontend/javascript/**/*.js')
        .pipe(react())
        .pipe(gulp.dest('build/javascript/'));
});

gulp.task('html', function() {
    var html = gulp.src('frontend/**/*.html')
        .pipe(gulp.dest('build/'));

    var favicon = gulp.src('frontend/favicon.ico')
        .pipe(gulp.dest('build/'));

    return merge(html, favicon);
});


// Browserify the source tree into a client-side library

function browserifyTask() {

    return gulp.src('build/javascript/client.js')
        .pipe(browserify({
            transform: ['envify']
        }))
        .pipe(rename('bundle.js'))
        .pipe(gulp.dest('build/javascript/'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('build/javascript/'));
}

gulp.task('browserify', ['html','javascript'], browserifyTask);
gulp.task('browserify_nodep', ['html','javascript'], browserifyTask);

gulp.task('browserify_nomin', ['html','javascript_nomin'],function(){
    return gulp.src('build/javascript/client.js')
        .pipe(browserify({
            transform: ['envify']
        }))
        .pipe(rename('bundle.js'))
        .pipe(gulp.dest('build/javascript/'));
});
// Compile and minify less

gulp.task('styles', function() {
    return gulp.src('frontend/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('build/'))
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('build/'));
    //.pipe(livereload());
});

// Images

gulp.task('images', function() {
    return gulp.src('frontend/images/**/*')
        .pipe(gulp.dest('build/images/'));
});

// Testing

gulp.task('javascript_test', function() {
    return gulp.src('frontend/tests/**/*.js')
        .pipe(gulp.dest('build/tests/'));
});

gulp.task('test', ['html','javascript', 'javascript_test'], function() {
    return gulp.src('build/tests/**/*.js').pipe(mocha());
});

// Local development (live reloading)

gulp.task('watch', ['clean'], function() {
    var watching = false;
    gulp.start('browserify', 'styles', 'images', function() {
        // Protect against this function being called twice
        if (!watching) {
            watching = true;
            gulp.watch('frontend/**/*.js', ['javascript']);
            gulp.watch('frontend/*.html', ['html']);
            gulp.watch('frontend/favicon*', ['html']);
            gulp.watch('frontend/*.html', ['html']);
            gulp.watch('frontend/images/**/*', ['images']);
            gulp.watch('build/javascript/client.js', ['browserify']);
            gulp.watch('frontend/**/*.less', ['styles']);
            nodemon({
                script: 'server.js',
                watch: 'build'
            });
        }
    });
});

gulp.task('dist-html', ['html'],function(){
    return gulp.src(['build/index.html','build/server.html','build/favicon.ico']).pipe(gulp.dest("dist/"));
});
gulp.task('dist-javascript', ['javascript','browserify'],function(){
    return gulp.src('build/javascript/bundle*.js').pipe(gulp.dest("dist/javascript/"));
});
gulp.task('dist-stylesheets', ['styles'],function(){
    return gulp.src('build/stylesheets/app.min.css').pipe(gulp.dest("dist/stylesheets/"));
});
gulp.task('dist-images', ['images'],function(){
    return gulp.src("build/images/**/*").pipe(gulp.dest("dist/images/"));
});

gulp.task('dist',['dist-html','dist-javascript','dist-stylesheets','dist-images']);

gulp.task('default', ['clean'], function() {
    return gulp.start('browserify', 'styles', 'images');
});