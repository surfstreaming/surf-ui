var express = require('express');
var cookieParser = require('cookie-parser');

var server = express()
    .use(cookieParser());

var React = require('react'),
    Router = require('react-router'),
    addons = require('react/addons'),
    Head = require('./build/javascript/Head')
;

console.log("STARTING",server.get('env'));
var folder = process.argv[2] == undefined ? "./build" : process.argv[2];

require("node-jsx").install({ extension: ".jsx", harmony: true });

var routes = require('./build/javascript/routes.js');

process.env.PWD = process.cwd();

// process command line parameters
console.log("Operating on folder " + folder);

var srvRoot = process.env.PWD + "/" + folder;

server.use("/javascript", express.static(srvRoot + "/javascript"));
server.use("/stylesheets", express.static(srvRoot + "/stylesheets"));
server.use("/images", express.static(srvRoot + "/images"));
server.use("/favicon.ico", function(req, res) {
    res.sendFile(srvRoot + "/favicon.ico");
});

server.all("/*", function(req, res, next) {

    var router = Router.create({
        routes: routes,
        location: req.path,
        onError: function(err){
            console.error("got an error",err);
        }
    });

    if (req.originalUrl == "/favicon.ico")
        return next(); //pass up to other express middleware and routes

    if(typeof req.query.noserver !== 'undefined'){
        console.log("Don't render on server");
        res.sendFile(srvRoot+"/index.html");
    } else {
        var token = req.cookies.token;
        router.run(function (Handler,state) {

            var promises = state.routes.filter(function (match) {
                // gather up the handlers that have a static `getTitle` method
                return match.handler.resolve;
            }).reduce(function (acc, match) {
                Object.keys(match.handler.resolve).forEach(function(key){
                    acc[key] = match.handler.resolve[key](state.params,state.query,acc,token);
                });
                return acc;
            },{});

            var promiseArray = [];
            var promiseKeys = [];
            Object.keys(promises).forEach(function(key){
                promiseArray.push(promises[key]);
                promiseKeys.push(key);
            });

            Promise.all(promiseArray).catch(function(error){
                console.log("GOT AN ERROR",error);
                return [];
            }).then(function(resolved){
                var data = {};
                resolved.forEach(function(x,idx){
                    var key = promiseKeys[idx];
                    data[key] = x;
                });

                var meta = state.routes.filter(function (match) {
                    return match.handler.meta;
                }).reduce(function (acc, match) {
                    var meta = match.handler.meta(state.path,state.params,state.query,data);
                    return acc.concat(meta);
                },[]);

                var title = "Surf | See What's Next";

                var head = React.renderToStaticMarkup(React.createElement(Head, {title: title, meta: meta}));
                //var body = React.renderToString(React.createElement(Handler, {params:state.params ,query: state.query}));
                //console.log("ready to write",res);
                var body = "";
                res.write('<html>');
                res.write(head);
                res.write('<body>');
                res.write('<div id="surf">' + body + '</div>');
                res.write('</body>');
                //res.write('<script>' + res.locals.state + '</script>');
                res.write("<script> "+
                "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ "+
                "    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), "+
                "    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) "+
                "})(window,document,'script','//www.google-analytics.com/analytics.js','ga'); "+
                "ga('create', 'UA-57242250-1', 'auto'); "+
                "ga('send', 'pageview'); "+
                '</script>');
                res.write('<script src="/javascript/bundle.js"></script>');
                res.write('</html>');
                res.end();
                console.log("done writing");

            });

        });
    }
});


var port = Number(process.env.PORT || 5000);
server.listen(port, function() {
    console.log("Listening on " + port);
});